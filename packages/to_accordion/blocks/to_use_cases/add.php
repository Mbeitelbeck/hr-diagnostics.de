<?php
defined('C5_EXECUTE') or die("Access Denied.");
$form = Loader::helper('form');
?>
<div class="title" style="margin-bottom: 25px;">
    <p>
        Bitte fügen Sie hier den Titel des Use-Case ein:
    </p>

    <input
        type="text"
        name="title"
        required
        style="width: 50%;"
        placeholder="<?php echo t('Bitte einen Titel eintragen') ?>"
        value=""
        >
</div>
<ul class="ccm-dialog-tabs tabs" id="ccm-block-tabs">
    <li class="ccm-nav-active">
        <a href="javascript:void(0)" id="ccm-block-content-one">Rahmenbedinungen</a>
    </li>
    <li>
        <a href="javascript:void(0)" id="ccm-block-content-two">Technische Lösungen</a>
    </li>
    <li>
        <a href="javascript:void(0)" id="ccm-block-content-three">Diagnostische Lösungen</a>
    </li>
</ul>
<div id="toUseCases" style="padding: 10px;">
    <div id="ccm-block-content-one-tab">
        <div class="clearfix"></div>
        <div class="editor" style="margin-top: 10px;">
            <?php  echo $form->textarea("content_one", "", array('class'=>"ccm-advanced-editor")); ?>
        </div>
    </div>
    <div id="ccm-block-content-two-tab" style="display: none;">
        <div class="clearfix"></div>

        <div class="editor" style="margin-top: 10px;">
            <?php  echo $form->textarea("content_two", "", array('class'=>"ccm-advanced-editor")); ?>
        </div>
    </div>
    <div id="ccm-block-content-three-tab" style="display: none;">
        <div class="clearfix"></div>
        <div class="editor" style="margin-top: 10px;">
            <?php  echo $form->textarea("content_three", "", array('class'=>"ccm-advanced-editor")); ?>
        </div>
    </div>
</div>