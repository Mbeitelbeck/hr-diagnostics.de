<?php
defined('C5_EXECUTE') or die("Access Denied.");
$title = $controller->getTitle();
$content_one = $controller->getContent_one();
$content_two = $controller->getContent_two();
$content_three = $controller->getContent_three();
?>


<div class="useCaseLayer">
    <h3><?php echo $controller->getTitle(); ?></h3>
    <div class="content">
        <?php
        if($content_one != "") {
        ?>
            <div class="useCase">
                <h2 class="rahmenbedinungen">Rahmenbedingungen</h2>
                <?php echo $content_one; ?>
            </div>
        <?php
        }
        if($content_two != "") {
            ?>
            <div class="useCase">
                <h2 class="technisch">Technische Lösung</h2>
                <?php echo $content_two; ?>
            </div>
        <?php
        }
        if($content_three != "") {
            ?>
            <div class="useCase">
                <h2 class="diagnostische">Diagnostische Lösung</h2>
                <?php echo $content_three; ?>
            </div>
        <?php
        }
        ?>
    </div>
</div>
