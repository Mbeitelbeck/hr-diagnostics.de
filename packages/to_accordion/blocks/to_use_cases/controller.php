<?php
defined('C5_EXECUTE') or die(_('Access Denied.'));
class ToUseCasesBlockController extends BlockController {
    protected $btTable = "toUsesCases";
    protected $btDescription = "Use-Case";
    protected $btInterfaceWidth = "750";
    protected $btInterfaceHeight = "400";
    /**
     * @return string
     */
    public function getBlockTypeDescription()
    {
        return $this->btDescription;
    }

    /**
     * @return string
     */
    public function getBlockTypeName()
    {
        return t('to:HRD Use-Cases');
    }

    public function save($data)
    {
        parent::save($data);
    }

    public function add()
    {

    }

    public function edit()
    {


    }

    public function view()
    {

    }




    function getContent_one() {
        $content = $this->content_one;
        return $content;
    }

    function getContent_two() {
        $content = $this->content_two;
        return $content;
    }

    function getContent_three() {
        $content = $this->content_three;
        return $content;
    }

    function getTitle() {
        $title = $this->title;
        return $title;
    }
}