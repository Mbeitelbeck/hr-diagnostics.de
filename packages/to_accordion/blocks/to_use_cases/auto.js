$(function() {
    tinyMCE.init({
        mode : "textareas",
        width: "100%",
        height: "300px",
        inlinepopups_skin : "concreteMCE",
        theme_concrete_buttons2_add : "spellchecker",
        relative_urls : false,
        convert_urls: false,
        plugins: "paste,inlinepopups,spellchecker,safari",
        theme : "advanced",
        theme_advanced_toolbar_location  : "top",
        editor_selector : "ccm-advanced-editor",
        theme_advanced_blockformats : "p,address,pre,h1,h2,h3,div,blockquote,cite",
        theme_advanced_buttons1 : "bold,italic,underline,|,bullist,|,undo,redo,|,pasteword,formatselect",
        theme_advanced_buttons2 :"",
        theme_advanced_buttons3 :"",
        theme_advanced_toolbar_align : "left",
        theme_advanced_blockformats : "p,h2,h3",
        spellchecker_languages : "+German=gm"

        //,styleselect,fontselect,fontsizeselect,strikethrough,justifyfull,justifyleft,justifycenter,justifyright,|
    });

    var ccm_ugActiveTab = "ccm-block-content-one";

    $("#ccm-block-tabs a").click(function() {
        $("li.ccm-nav-active").removeClass('ccm-nav-active');
        $("#" + ccm_ugActiveTab + "-tab").hide();
        ccm_ugActiveTab = $(this).attr('id');
        $(this).parent().addClass("ccm-nav-active");
        $("#" + ccm_ugActiveTab + "-tab").show();
    });
})
