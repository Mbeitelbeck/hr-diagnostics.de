<?php
class ToAccordionPackage extends Package {
    protected $pkgHandle = 'to_accordion';
    protected $appVersionRequired = '5.5';
    protected $pkgVersion = '1.0';

    public function getPackageDescription() {
        return t("Installiert den toAccordion - HRD Use-Cases Block und fügt ein Stack-Template hinzu");
    }

    public function getPackageName() {
        return t("to:Accordion Package");
    }

    public function install() {
        $pkg = parent::install();
        // install block
        BlockType::installBlockTypeFromPackage('to_use_cases', $pkg);
    }
}