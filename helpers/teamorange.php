<?php
/**
 * @package Helpers
 * @category Concrete
 * @author Teamorange
 */

defined('C5_EXECUTE') or die("Access Denied.");
class TeamorangeHelper
{

    public $lang = LANGUAGE;
    public $locale = ACTIVE_LOCALE;
    public $homePath = DIR_REL;
    public $homeID = 0;

    public function __construct()
    {
        if (Package::getByHandle('multilingual')) {
            $ms = MultilingualSection::getCurrentSection();
            if (is_object($ms)) {
                $this->homeID = $ms->cID;
                $this->homePath = Loader::helper('navigation')->getLinkToCollection($ms, true);
                $this->lang = $ms->getLanguage();
                $this->locale = $lang . "_" . $ms->getIcon();
            }
        }

        if (file_exists('firelogger.php')) {
            include_once('firelogger.php');
        }
    }

    public function et($de, $en)
    {
        echo t($de, $en);
    }

    public function t($de, $en)
    {
        switch ($this->lang) {
            case 'en':
                $t = $en;
                break;
            case 'de':
            default:
                $t = $de;
        }
        if (!preg_match("/<.*>.*<\/.*>/", $t)) {
            $t = htmlentities($t);
        }
        return $t;
    }

    public function trace()
    {
        if (file_exists('firelogger.php')) {
            try {
                throw new Exception('trace');
            } catch (Exception $e) {
                flog($e);
            }
        }
    }

    /**
     * Erstellt Thumbnails. Crasht nicht bei zu großen Bildern.
     * Returns an object with the following properties: src, width, height, error, errorMsg
     * @param mixed $obj
     * @param int $maxWidth
     * @param int $maxHeight
     * @param bool $crop
     *
     * @return \stdClass
     */
    public function getThumbnailSafely($obj, $maxWidth, $maxHeight, $crop = false)
    {

        $errorThumb = new stdClass;
        $errorThumb->error = true;
        $errorThumb->src = '';
        $errorThumb->width = 0;
        $errorThumb->height = 0;

        if ($obj instanceof File) {
            $f = File::getByID($obj->fID);
            if ($f->error) {
                $errorThumb->errorMsg = '[ERROR] Invalid file: ' . $obj->fID;
                Log::addEntry($errorThumb->errorMsg);
                return $errorThumb;
            }
            $path = $f->getPath();
        } else {
            $path = $obj;
        }

        if ($path == '') {
            $errorThumb->errorMsg = '[ERROR] Invalid path.';
            Log::addEntry($errorThumb->errorMsg);
            return $errorThumb;
        }
        $size = @getimagesize($path);
        if (empty($size)) {
            $errorThumb->errorMsg = "[ERROR] Can't get size: " . $path;
            Log::addEntry($errorThumb->errorMsg);
            return $errorThumb;
        }
        if (($size[0] > 5000) || ($size[1] > 5000)) {
            $errorThumb->errorMsg = '[ERROR] Image too big: ' . $obj->fID;
            Log::addEntry($errorThumb->errorMsg);
            return $errorThumb;
        }
        $imgHelper = Loader::helper('image');
        $thumb = $imgHelper->getThumbnail($path, $maxWidth, $maxHeight, $crop);
        $thumb->error = false;
        $thumb->errormsg = 'Everything went better than expected.';
        return $thumb;
    }
}