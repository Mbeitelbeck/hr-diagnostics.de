<?php
defined('C5_EXECUTE') or die("Access Denied.");
$form = Loader::helper('form');
$captcha = Loader::helper('validation/captcha');
?>
<div class="diagonal-box">
    <div class="content">
	<?php if (isset($response)) { ?>
	<p><b>Eine E-Mail mit einem Bestätigungslink wurde an Ihre E-Mail Adresse geschickt.</b> Sobald Sie Ihre Abmeldung bestätigt haben, werden Sie aus unserer Empfängerliste gelöscht.</p>
	<?php }elseif (isset($error)){ ?>
	<p>Leider ist der Service vorrübergehend nicht erreichbar.</p><p>Bitte versuchen Sie es zu einem späteren Zeitpunkt noch einmal.</p>
	<?php }else{
        if (isset($errorArray)){
            foreach($errorArray as $e) {
                echo '<p style="color:crimson;">'.$e.'</p>';
            }
            echo '<hr><br><br>';
        }
    ?>
        <form method="post" class="miniSurveyView" action="<?php echo $this->action('unsubscribe') ?>" enctype="multipart/form-data">
            <table class="formBlockSurveyTable">
                <tbody>
                <tr>
                    <td valign="top" class="question required">Eingetragene E-Mail&nbsp;<span>*</span></td>
                    <td valign="top"><input type="text" name="email" value="" class="text"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="submit" value="Abmelden"/></td>
                </tr>
                </tbody>
            </table>
        </form>
			<?php } ?>
    </div>
</div>
