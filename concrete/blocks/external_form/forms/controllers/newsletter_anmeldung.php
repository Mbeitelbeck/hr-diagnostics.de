<?php 
defined('C5_EXECUTE') or die("Access Denied.");
class NewsLetterAnmeldungExternalFormBlockController extends BlockController {

	private $postUrl = "http://tools.emailsys.net/104/1073/xbg0ii/subscribe/form.html";
	
	public function action_subscribe() {
		$captcha = Loader::helper('validation/captcha');
        /** @var FormHelper $val */
		$val = Loader::helper('validation/form');
		$mh = Loader::helper('mail');
		
		// check captcha
		if (!$captcha->check()) {
				$errors['captcha'] = t("Incorrect captcha code");
				$_REQUEST['ccmCaptchaCode'] = '';
		}
		
		// Validate input
		$val->setData($this->post());
		$val->addRequiredEmail('email', 'Bitte geben Sie eine g&uuml;ltige E-Mail Adresse ein');
		
		//$val->addRequired('gender', $errorMsg = 'Bitte geben Sie Ihre Anrede ein.', $validate = ValidationFormHelper::VALID_NOT_EMPTY);
		//$val->addRequired('firstname', $errorMsg = 'Bitte geben Sie Ihren Vornamen ein.', $validate = ValidationFormHelper::VALID_NOT_EMPTY);
		//$val->addRequired('lastname', $errorMsg = 'Bitte geben Sie Ihren Nachnamen ein.', $validate = ValidationFormHelper::VALID_NOT_EMPTY);
		//$val->addRequired('extra1', $errorMsg = 'Bei welchem Unternehmen sind Sie beschäftigt', $validate = ValidationFormHelper::VALID_NOT_EMPTY);
		$val->addRequired('datenschutz', $errorMsg = 'Bitte nehmen Sie die Datenschutzerklärung zur Kenntnis', $validate = ValidationFormHelper::VALID_NOT_EMPTY);


        /* ************************************************************* *
         * Validation setup/errors and functions for non required fields *
         * ************************************************************* */


        // Setup for fields to be validated
        $postData = $this->post();
        $name = $postData['firstname'];
        $lastname = $postData['lastname'];
        $phone = $postData['extra2'];
        $zip = $postData['zip'];
        $city = $postData['extra4'];
        $cError = array();

        // Store form Values in $_SESSION[] so they do not get lost on error
        $_SESSION['postDataStore'] = $postData;

        // Validation functions
        function regIsInt($input)
        {
            return preg_match("/^[0-9]+$/", $input);
        }

        function regIsChar($input)
        {
            return preg_match("/^[a-zA-öäüZÖÄÜß -]+$/", $input);
        }

        function regIsPhone($input)
        {
            return preg_match("/^([0-9 -])+$/", $input);
        }


        // Error highlighting functions
        function replaceInt($input)
        {
            return preg_replace("/[0-9]/", "", $input);
        }

        function replaceChar($input)
        {
            return preg_replace("/[a-zA-ZöäüÖÄÜß -]/", "", $input);
        }

        function replacePhone($input)
        {
            return preg_replace("/([0-9 -])/", "", $input);
        }


        // Non required fields validation
        if ( $name != '' && !(regIsChar($name)) )
        {
            $cError[] = 'Unerlaubte(s) Zeichen im Vornamen: "'.replaceChar($name).'"';
        }

        if ( $lastname != '' && !(regIsChar($lastname)) )
        {
            $cError[] = 'Unerlaubte(s) Zeichen im Nachnamen: "'.replaceChar($name).'"';
        }

        if ( $phone != '' && !(regIsPhone($phone)) )
        {
            $cError[] = 'Unerlaubte(s) Zeichen in der Telefonnummer: "'.replacePhone($phone).'"';
        }

        if ( $zip != '' && !(regIsInt($zip)) )
        {
            $cError[] = 'Unerlaubte(s) Zeichen in der PLZ: "'.replaceInt($zip).'"';
        }

        if ( $city != '' && !(regIsChar($city)) )
        {
            $cError[] = 'Unerlaubte(s) Zeichen im Ortsnamen: "'.replaceChar($city).'"';
        }

        // END of validation functions and error array setup.


        // Check for any validation errors
		if ($val->test() && !isset($errors['captcha']) && count($cError) == 0) {

			// build up post variables
			$vars = '';
			foreach ($this->post() as $key => $value) {
				if($key != 'datenschutz' && $key != 'ccmCaptchaCode' && $key != 'submit' && !empty($value)){
					if(!empty($vars)){
						$vars.='&';
					}
					$vars.= $key.'='.$value;
				}
			}

			// try to post data to rapidmail
			if ($this->postToRapidmail($vars)) {
				$this->set('response', 'Erfolgreich angemeldet');

			} else {
				// received error from rapidmail
				$this->set('error', 'Service nicht erreichbar');
			}

            // Clear form data stored in $_SESSION[]
            unset($_SESSION['postDataStore']);

		} else {
			// input errors
			$errorArray = $val->getError()->getList();

            // get all non required field validation errors
            foreach ($cError as $e) {
                $errorArray[] = $e;
            }

            // check for captcha error
			if (isset($errors['captcha'])) {
				$errorArray[] = $errors['captcha'];
			}

            // set errors to be displayed
			$this->set('errorArray', $errorArray);
		}
	}
	
	private function postToRapidmail($vars) {
		$ch = curl_init($this->postUrl);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,0);
		curl_setopt($ch, CURLOPT_HEADER,0);
		$result = curl_exec($ch);
		$httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close ($ch);
		/*if($httpStatus != 200){
			return false;
		}*/
		return true;
	}
}