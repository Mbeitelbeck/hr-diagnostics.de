<?php 
defined('C5_EXECUTE') or die("Access Denied.");
class NewsLetterAbmeldungExternalFormBlockController extends BlockController {

	private $postUrl = "http://tools.emailsys.net/104/1073/xbg0ii/unsubscribe/form.html";
	
	public function action_unsubscribe() {
		$val = Loader::helper('validation/form');
		$mh = Loader::helper('mail');
		
		// Validate input
		$val->setData($this->post());
		$val->addRequiredEmail('email', 'Bitte geben Sie eine E-Mail Adresse ein');
		
		if ($val->test()) {
			// build up post variables
			$vars = 'email='.$this->post('email');
			// try to post data to rapidmail
			if($this->postToRapidmail($vars)){
				$this->set('response', 'Erfolgreich abgemeldet');
			}else{
				// received error from rapidmail
				$this->set('error', 'Service nicht erreichbar');
			}
		}else{
			// input errors
			$errorArray = $val->getError()->getList(); 
			$this->set('errorArray', $errorArray);
		}
	}
	
	private function postToRapidmail($vars){
		$ch = curl_init($this->postUrl);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,0);
		curl_setopt($ch, CURLOPT_HEADER,0);
		$result = curl_exec($ch);
		$httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close ($ch);
		//if($httpStatus != 200){
		//	return false;
		//}
		return true;
	}
}