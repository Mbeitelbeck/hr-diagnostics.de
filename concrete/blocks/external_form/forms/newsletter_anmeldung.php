<?php
defined('C5_EXECUTE') or die("Access Denied.");
$form = Loader::helper('form');
$captcha = Loader::helper('validation/captcha');
?>
<style>
    label {
        cursor: pointer;
    }
</style>
<div class="diagonal-box">
    <div class="content">
	<?php if (isset($response)) { ?>
	<p><b>Vielen Dank für Ihre Anmeldung!</b></p>
	<p>Eine E-Mail mit einem Aktivierungslink wurde an Ihre E-Mail-Adresse geschickt! </p>
	<?php }elseif (isset($error)){ ?>
	<p>Leider ist der Service vorrübergehend nicht erreichbar.</p><p>Bitte versuchen Sie es zu einem späteren Zeitpunkt noch einmal.</p>
	<?php }else{
            if (isset($errorArray)){
                foreach($errorArray as $e) {
                    echo '<p style="color:crimson;">'.$e.'</p>';
                }
                echo '<hr><br><br>';

                // Restore Values from $_SESSION[]
                $firstname = $lastname = $extra1 = $email = $extra2 = $extra3 = $zip = $extra4 = $extra2 = $female = $male = '';

                $postSave = $_SESSION['postDataStore'];
                foreach ($postSave as $k => $v)
                {
                    switch ($k)
                    {
                        case 'gender':
                            $gender = $v;
                            switch ($gender)
                            {
                                case 'female':  $female = ' checked';   break;
                                case 'male':    $male   = ' checked';   break;
                            }
                            break;
                        case 'datenschutz':
                            if (!empty($v))
                            {
                                $privacy = ' checked';
                            }
                            break;
                        case 'firstname':   $firstname  = $v;   break;
                        case 'lastname':    $lastname   = $v;   break;
                        case 'extra1':      $extra1     = $v;   break;
                        case 'email':       $email      = $v;   break;
                        case 'extra2':      $extra2     = $v;   break;
                        case 'extra3':      $extra3     = $v;   break;
                        case 'zip':         $zip        = $v;   break;
                        case 'extra4':      $extra4     = $v;   break;
                    }
                }
            }
    ?>
        <form method="post" class="miniSurveyView" action="<?php echo $this->action('subscribe')?>" enctype="multipart/form-data">
            <table class="formBlockSurveyTable">
                <tbody>
                    <tr>
                        <td class="question">
                            Anrede
                        </td>
                        <td>
                            <input type="radio" id="gender_female" name="gender" value="female"<?php echo $female ?>/> <label for="gender_female">Frau</label>
                            <input type="radio" id="gender_male" name="gender" value="male"<?php echo $male ?>/> <label for="gender_male">Herr</label>
                        </td>
                    </tr>
                    <tr>
                        <td class="question">
                            <label for="firstname">Vorname</label>
                        </td>
                        <td>
                            <input id="firstname" type="text" name="firstname" value="<?php echo $firstname ?>" class="text">
                        </td>
                    </tr>
                    <tr>
                        <td class="question">
                            <label for="lastname"></label>Nachname
                        </td>
                        <td>
                            <input id="lastname" type="text" name="lastname" value="<?php echo $lastname ?>" class="text">
                        </td>
                    </tr>
                    <tr>
                        <td class="question">
                            <label for="unternehmen">Unternehmen / Behörde</label>
                        </td>
                        <td>
                            <input id="unternehmen" type="text" name="extra1" value="<?php echo $extra1 ?>" class="text">
                        </td>
                    </tr>
                    <tr>
                        <td class="question required">
                            <label for="mail">Geschäftliche E-Mail&nbsp;<span>*</span></label>
                        </td>
                        <td>
                            <input id="mail" type="text" name="email" value="<?php echo $email ?>" class="text">
                        </td>
                    </tr>
                    <tr>
                        <td class="question">
                            <label for="tel">Telefon</label>
                        </td>
                        <td>
                            <input id="tel" type="text" name="extra2" value="<?php echo $extra2 ?>" class="text">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="question">
                            <label for="str">Straße / Hausnummer</label>
                        </td>
                        <td valign="top">
                            <input id="str" type="text" name="extra3" value="<?php echo $extra3 ?>" class="text">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="question">
                            <label for="plz">PLZ</label>
                        </td>
                        <td valign="top">
                            <input id="plz" type="text" name="zip" value="<?php echo $zip ?>" class="text">
                        </td>
                    </tr>
                    <tr>
                        <td class="question">
                            <label for="ort">Ort</label>
                        </td>
                        <td>
                            <input id="ort" type="text" name="extra4" value="<?php echo $extra4 ?>" class="text">
                        </td>
                    </tr>
                    <tr>
                        <td class="question required">Datenschutz&nbsp;<span>*</span></td>
                        <td>
                            <div class="checkboxList">
                                <div class="checkboxPair">
                                    <input id="datenschutz" type="checkbox" name="datenschutz" value="Mit dem Ausfüllen der Anmeldung stimmen Sie dem Erhalt des Newsletters der HR Diagnostics AG zu. Der Newsletter wird per E-Mail verschickt und enthält u.a. Informationen über neue Produkte und Angebote. Wir werden Ihre personenbezogenen Daten, die wir für den Versand des Newsletters verarbeiten, nicht Dritten zur Verfügung stellen. Sie können den Erhalt des Newsletters jederzeit mit Wirkung für die Zukunft abbestellen, per Klick auf einen Link im Newsletter, per E-Mail an newsletter[at]hr-diagnostics.de, per Telefon unter 0711 / 48602010 oder postalisch unter HR Diagnostics AG, Königstr. 20, 70173 Stuttgart. Falls wir Sie nicht in dem Empfängerkreis unseres Newsletter ausnehmen, werden Sie hierüber im Nachgang zu Ihrer Anmeldung informiert"<?php echo $privacy ?>>
                                    <label for="datenschutz">
                                        Mit dem Ausfüllen der Anmeldung stimmen Sie dem Erhalt des Newsletters der HR
                                        Diagnostics AG zu. Der Newsletter wird per E-Mail verschickt und enthält u.a.
                                        Informationen über neue Produkte und Angebote. Wir werden Ihre personenbezogenen
                                        Daten, die wir für den Versand des Newsletters verarbeiten, nicht Dritten zur
                                        Verfügung stellen. Sie können den Erhalt des Newsletters jederzeit mit Wirkung für
                                        die Zukunft abbestellen, per Klick auf einen Link im Newsletter, per E-Mail an
                                        newsletter[at]hr-diagnostics.de, per Telefon unter 0711 / 48602010 oder postalisch
                                        unter HR Diagnostics AG, Königstr. 20, 70173 Stuttgart. Falls wir Sie nicht in dem
                                        Empfängerkreis unseres Newsletter aufnehmen, werden Sie hierüber im Nachgang zu
                                        Ihrer Anmeldung informiert
                                    </label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Bitte geben Sie die Zeichen und Nummern ein.</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><?php $captcha->display(); ?><br/> <?php $captcha->showInput(); ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="hidden" name="extra7" value="website"/>
                            <input type="submit" name="submit" value="Anmelden"/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
			<?php } ?>
    </div>
</div>
