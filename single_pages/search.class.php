<?php
class Search
{
	public function setQuery($param)
	{
		$this->query = $param;
	}
	
	public function getTestbatterie()
	{
		$db = Loader::db();
		$this->test = $db->GetAll("SELECT DISTINCT * FROM `btTOApp` WHERE `testtyp` = 0 and `dimensionen` IS NOT NULL");
		
		foreach($this->test as $array => $innerArray)
		{
			foreach($innerArray as $key)
			{
				if(strpos($innerArray['dimensionen'], $this->query)!==false)
				{
					$this->testResult[] = $innerArray;
				}
			}
		}
		return $this->testResult;
	}
	
	
	
	public function getEinzelTest()
	{
		$db = Loader::db();
		$this->einzel = $db->GetAll("SELECT DISTINCT * FROM `btTOApp` WHERE `testtyp` = 1 and `dimensionen` IS NOT NULL");
		
		foreach($this->test as $array => $innerArray)
		{
			foreach($innerArray as $key)
			{
				if(strpos($innerArray['dimensionen'], $this->query)!==false)
				{
					$this->einzelResult[] = $innerArray;
				}
			}
		}
		return $this->einzelResult;
	}
	
}
?>