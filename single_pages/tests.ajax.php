<?php

// hauptsächlich wegen der Zugangsdaten zur Datenbank
require_once('../config/site.php');


if (!isset($_GET['q'])) {
    $_GET['q'] = 0;
}
if (!isset($_GET['a'])) {
    $_GET['a'] = 0;
}
if (!isset($_GET['e'])) {
    $_GET['e'] = 0;
}
if (!isset($_GET['u'])) {
    $_GET['u'] = 0;
}
if (!isset($_GET['c'])) {
    $_GET['c'] = 0;
}

$SQL = "SELECT bID, ((
(kuerzel LIKE '%" . $_GET['q'] . "%' and kuerzel is not null)
OR (name LIKE '%" . $_GET['q'] . "%' and name is not null)
OR (kurzbeschreibung LIKE '%" . $_GET['q'] . "%' and kurzbeschreibung is not null)
OR (beschreibung LIKE '%" . $_GET['q'] . "%' and beschreibung is not null)
OR (besonderheit LIKE '%" . $_GET['q'] . "%' and besonderheit is not null)
)";
if ($_GET['a'] == 1) {
    $SQL = $SQL . " AND isPersonalauswahl = 1";
}
if ($_GET['e'] == 1) {
    $SQL = $SQL . " AND isPersonalentwicklung = 1";
}
if ($_GET['u'] == 1) {
    $SQL = $SQL . " AND isUngBearb = 1";
}
$SQL = $SQL . ") AS visible
FROM btHRtest
ORDER BY bID";

$dbhandle = mysql_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD) or die(mysql_error());
$db = mysql_select_db(DB_DATABASE, $dbhandle);

$result = mysql_query($SQL);

if ($_GET['c'] > 0 and is_numeric($_GET['c'])) {
    $SQL2    = "SELECT
		bID, 
		if((concat(empfohlen,' ') REGEXP 'c" . $_GET['c'] . "[^0-9]'),'e',
			if((concat(bedEmpfohlen,' ') REGEXP 'c" . $_GET['c'] . "[^0-9]'),'b',
				if((concat(jewEntw,' ') REGEXP 'c" . $_GET['c'] . "[^0-9]'),'j','0')
			)
		) AS ok 
	FROM btHRtest
	ORDER BY bID";
    $result2 = mysql_query($SQL2);

    $counter = 0;
    while ($row2 = mysql_fetch_object($result2)) {
        $tmprow[$row2->bID] = $row2->ok;
        $counter++;
    }
}


$json = '{"RES":{"COLUMNS":["BID","VISIBLE"';
if (isset($tmprow)) {
    $json = $json . ',"STATUS"';
}
$json = $json . '],"DATA":[';
while ($row = mysql_fetch_object($result)) {
    if (isset($tmprow)) {
        $json = $json . '[' . $row->bID . ',';
        if (is_numeric($tmprow[$row->bID])) {
            $json = $json . '0,' . $tmprow[$row->bID];
        }
        else {
            if ($row->visible == "") {
                $rVisible = 0;
            }
            else {
                $rVisible = $row->visible;
            }
            $json = $json . '' . $rVisible . ',"' . $tmprow[$row->bID] . '"';
        }
    }
    else {
        if ($row->visible == "") {
            $rVisible = 0;
        }
        else {
            $rVisible = $row->visible;
        }
        $json = $json . '[' . $row->bID . ',' . $rVisible;
    }
    $json = $json . '],';
}
$json = substr($json, 0, strlen($json) - 1);
$json = $json . ']}}';

echo $json;
