<?php 
	defined('C5_EXECUTE') or die(_("Access Denied."));
	$aBlocks = $controller->generateNav();
	global $c;
	echo("<ul id=\"topnavi\">");
	$first = true;
	
	$nh = Loader::helper('navigation');
	
	foreach($aBlocks as $ni) {
		$_c = $ni->getCollectionObject();
		if ($_c->getCollectionAttributeValue('top_nav')) {
			
			$pageLink = $ni->getURL();
			
			if ($first) {
				echo('<li class="first"><a href="' . $pageLink . '">' . $ni->getName() . '</a></li>
');
			} else {
				echo('<li><a href="' . $pageLink . '">' . $ni->getName() . '</a></li>
');
			}
			
			$first = false;
		
		}
	}
/*	if ($_SESSION["lang"] == "de") {
		echo('<li><a href="/en/">English</a></li>');
	} else {
		echo('<li><a href="/de/">deutsch</a></li>');
	};*/
	echo("</ul>");
?>