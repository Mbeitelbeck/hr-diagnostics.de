<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

$aBlocks = $controller->generateNav();
$first   = true;
$nh      = Loader::helper('navigation');
global $c;

echo("<ul id=\"footernavi\">");
foreach ($aBlocks as $ni) {
    $_c = $ni->getCollectionObject();
    if ($_c->getCollectionAttributeValue('shop_footer_nav')) {

        $pageLink = $ni->getURL();

        if ($first) {
            echo('<li><a style="border-left:none;" href="' . $pageLink . '">' . $ni->getName() . '</a></li>');
        }
        else {
            echo('<li><a href="' . $pageLink . '">' . $ni->getName() . '</a></li>');
        }

        $first = false;
    }
}
echo("<li><a href='mailto:testverlag@hr-diagnostics.de?subject=Kontakt Testverlag'>Kontakt</a></li>");

echo("</ul>");
