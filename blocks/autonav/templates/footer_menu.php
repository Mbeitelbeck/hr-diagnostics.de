<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

$aBlocks = $controller->generateNav();
$first   = true;
$nh      = Loader::helper('navigation');
global $c;

echo("<ul id=\"footernavi\">");
echo('<li class="first"><a href="/login/">Login</a></li>');
foreach ($aBlocks as $ni) {
    $_c = $ni->getCollectionObject();
    if ($_c->getCollectionAttributeValue('footer_nav')) {

        $pageLink = $ni->getURL();

        if ($first) {
            echo('<li><a href="' . $pageLink . '">' . $ni->getName() . '</a></li>');
        }
        else {
            echo('<li><a href="' . $pageLink . '">' . $ni->getName() . '</a></li>');
        }

        $first = false;
    }
}
echo("</ul>");
