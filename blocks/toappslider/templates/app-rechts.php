<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$c = Page::getByID($_REQUEST['cID'], $_REQUEST['cvID']);

$p = Page::getByID($link);
$pagePath = $p->cPath;


$file = File::getByID($fID);
$path = $file->getPath();
$size = getimagesize($path);

$imagePath = File::getRelativePathFromID($fID);


?>

<div class="app-box right">
	<h3><?php echo $title; ?></h3>
	<p class="desc"><?php echo $kurzbeschreibung; ?></p>
	<div class="info">
		<div class="icon">
			<?php		
		if ($fID != 0 && $fID != "") {  		
			print '<img'.$class.' src="'.$imagePath.'" alt="'.$title.'" width="" height="" />';
		}		
		?>
		</div>
		<div class="list"> <?php print $content; ?>
			<?php if ($pagePath != "") { ?>
			<a title="<?php print $title; ?>" href="<?php print $pagePath; ?>" class="info-link">Mehr Information</a>
			<?php } ?>
			<div class="action">
				<p class="price"><?php print $preis; ?> &euro; <a class="buy" href="#"><span>bestellen</span></a></p>
			</div>
			<div class="footer"></div>
		</div>
		<div class="footer"></div>
	</div>
</div>
