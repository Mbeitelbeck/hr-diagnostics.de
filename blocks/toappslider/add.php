<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true;
$assetLibraryPassThru = array(
	'type' => 'image'
);
$pageselector = Loader::helper('form/page_selector');
$al = Loader::helper('concrete/asset_library');

?>

<h2><strong>HRD TEST-Slider anlegen</strong></h2>
<input type="hidden" name="datePublic" value="<?php echo date( 'Y-m-d H:i:s'); ?>" />
<div class="ccm-block-field-group">
	<h3><?php echo t('&Uuml;berschirft')?></h3>
	<?php echo  $form->text('title', $title, array('style' => 'width: 500px')); ?> </div>
<div class="hr">
	<hr />
</div>


<div class="ccm-block-field-group">
	<h3><?php echo t('Kurzbeschreibung')?></h3>
	<?php echo  $form->text('kurzbeschreibung', $kurzbeschreibung, array('style' => 'width: 500px')); ?> </div>


<div class="hr">
	<hr />
</div>
<div class="ccm-block-field-group">
	<h3><?php echo t('Teaser-Grafik')?></h3>
	<?php echo $al->image('fID', 'fID', t('Choose File'), $sf);?> </div>
<div class="hr">
	<hr />
</div>

<div class="hr">
	<hr />
</div>
<div id="ccm-block-field-group">
	<h3><?php echo t('Link zur Detailseite')?></h3>
	<?php print $pageselector->selectPage('link', $link);?> </div>
	

