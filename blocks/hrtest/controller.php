<?php
	Loader::block('library_file');
	defined('C5_EXECUTE') or die(_("Access Denied."));	
	global $initHRtest;
	$initHRtest=true;
	class HrtestBlockController extends BlockController {

		protected $btInterfaceWidth = "850";
		protected $btInterfaceHeight = "600";
		protected $btTable = 'btHRtest';

		/** 
		 * Used for localization. If we want to localize the name/description we have to include this
		 */
		public function getBlockTypeDescription() {
			return t("HRtestelement.");
		}
		
		public function getBlockTypeName() {
			return t("HRtestBox");
		}		
		
		/*public function getJavaScriptStrings() {
			return array(
				'image-required' => t('You must select an image.')
			);
		}*/
		
		function getID() {return $this->bID;}
		function getFileID() {return $this->fID;}
		function getKuerzel() {return $this->kuerzel;}
		function getName() {return $this->name;}
		function getDescription() {return $this->description;}
		function getDefinition() {return $this->definition;}
		function getVerfahren() {return $this->verfahren;}
		function getAnwendungsbereich() {return $this->anwendungsbereich;}
		function getLangversion() {return $this->langversion;}
		function getKurzversion() {return $this->kurzversion;}
		function isPersonalauswahl() {return $this->isPersonalauswahl;}
		function isPersonalentwicklung() {return $this->isPersonalentwicklung;}
		function getUngBearb() {return $this->ungBearb;}
		function getBesonderheit() {return $this->besonderheit;}
		function getGuetekriterien() {return $this->guetekriterien;}
		
		public function save($args) {		
			$args['fOnstateID'] = ($args['fOnstateID'] != '') ? $args['fOnstateID'] : 0;
			$args['fID'] = ($args['fID'] != '') ? $args['fID'] : 0;
			$args['kuerzel'] = ($args['kuerzel'] != '') ? $args['kuerzel'] : 0;
			$args['name'] = ($args['name'] != '') ? $args['name'] : "";
			$args['kurzbeschreibung'] = ($args['kurzbeschreibung'] != '') ? $args['kurzbeschreibung'] : "";
			$args['beschreibung'] = ($args['beschreibung'] != '') ? $args['beschreibung'] : "";
			
			$args['langItems'] = ($args['langItems'] != '') ? $args['langItems'] : 0;
			$args['langDimensionen'] = ($args['langDimensionen'] != '') ? $args['langDimensionen'] : 0;
			$args['langDauer'] = ($args['langDauer'] != '') ? $args['langDauer'] : 0;
			$args['langIsUngBearb'] = ($args['langIsUngBearb'] != '') ? $args['langIsUngBearb'] : 0;
			
			$args['kurzItems'] = ($args['kurzItems'] != '') ? $args['kurzItems'] : 0;
			$args['kurzDimensionen'] = ($args['kurzDimensionen'] != '') ? $args['kurzDimensionen'] : 0;
			$args['kurzDauer'] = ($args['kurzDauer'] != '') ? $args['kurzDauer'] : 0;
			$args['kurzIsUngBearb'] = ($args['kurzIsUngBearb'] != '') ? $args['kurzIsUngBearb'] : 0;
			
			$args['isPersonalauswahl'] = ($args['isPersonalauswahl'] != '') ? $args['isPersonalauswahl'] : 0;
			$args['isPersonalentwicklung'] = ($args['isPersonalentwicklung'] != '') ? $args['isPersonalentwicklung'] : 0;
			
			$args['besonderheit'] = ($args['besonderheit'] != '') ? $args['besonderheit'] : "";
			$args['guetekriterien'] = ($args['guetekriterien'] != '') ? $args['guetekriterien'] : "";
			$args['bedEmpfohlen'] = ($args['bedEmpfohlen'] != '') ? $args['bedEmpfohlen'] : "";
			$args['jewEntw'] = ($args['jewEntw'] != '') ? $args['jewEntw'] : "";
			$args['empfohlen'] = ($args['empfohlen'] != '') ? $args['empfohlen'] : "";
			parent::save($args);
		}

		function getContentAndGenerate($align = false, $style = false, $id = null) {
			$db = Loader::db();
			global $c;
			$bID = $this->bID;
			
			
			/*$r = $db->query("select choiceS, choiceM, choiceXL from btHRtest where bID =".$bID);
			while ($row = $r->fetchRow()) {
				$html = "<div class='".$row['choiceS']."'>S</div><div class='".$row['choiceM']."'>M</div><div class='".$row['choiceXL']."'>XL</div>";
			}*/
			
			var_dump($db->query("select * from btHRtest where bID =".$bID));

			return $html;
		}
		
		function getAllocation() {
			$db = Loader::db();
			global $c;
			$bID = $this->bID;
			
// TEMP	!!! Aus alten Tabelle Zuordnung laden		
	/*$r = $db->query("select * from grouptests WHERE testid = ".$bID);
	while ($row = $r->fetchRow()) {
		if ($row["value"]==1){
			$tmpGeeignet = trim($tmpGeeignet)." c".$row["tgid"];
		} else if ($row["value"]==2) {
			$tmpbedGeeignet = trim($tmpbedGeeignet)." c".$row["tgid"];
		} else if ($row["value"]==4) {
			$tmpJewEnt = trim($tmpJewEnt)." c".$row["tgid"];
		}
	}
	$r = $db->query("UPDATE btHRtest
					SET empfohlen = '".$tmpGeeignet."',
					bedEmpfohlen = '".$tmpbedGeeignet."',
					jewEntw = '".$tmpJewEnt."'
					WHERE bID = ".$bID);*/
// TEMP	

// TEMP	!!! Tabelleninhalt neu strukturieren		
	/*$r = $db->query("select * from btHRtest");
	while ($row = $r->fetchRow()) {
		$q = $db->query("UPDATE btHRtest
					SET beschreibung = '<h4>Definition:</h4>".$row["definition"]."<h4>Verfahren:</h4>".$row["verfahren"]."<h4>Anwendungsbereich:</h4>".$row["anwendungsbereich"]."'
					WHERE bID = ".$row["bID"]);
	}*/
// TEMP	

// TEMP	!!! Langversion und Kurzversion neu speichern
	/*$r = $db->query("select * from btHRtest");
	while ($row = $r->fetchRow()) {
		$arrayLangversion = preg_split("/\//", $row["langversion"] );
		$arrayKurzversion = preg_split("/\//", $row["kurzversion"] );
		$q = $db->query("UPDATE btHRtest
						SET kurzItems = '".$arrayKurzversion[0]."',kurzDimensionen = '".$arrayKurzversion[1]."',kurzDauer = '".$arrayKurzversion[2]."',
							langItems = '".$arrayLangversion[0]."',langDimensionen = '".$arrayLangversion[1]."',langDauer = '".$arrayLangversion[2]."'
						WHERE bID = ".$row["bID"]);
	}*/
// TEMP	

		
			$allocation = array("empfohlen" => array(),"bedEmpfohlen" => array(),"nichtEmpfohlen" => array(),"jewEntw" => array());
			
			$r = $db->query("select * from targetgroups");
			while ($row = $r->fetchRow()) {
				$targetgroups["c".$row["id"]] = array("id" => "c".$row["id"], "category" => $row["category"], "title" => $row["title"]);
				$allocation["nichtEmpfohlen"]["c".$row["id"]] = $targetgroups["c".$row["id"]];
			}
			
			$r = $db->query("select jewEntw, bedEmpfohlen, empfohlen from btHRtest where bID =".$bID);
			while ($row = $r->fetchRow()) {
				$jewEntw = explode(" ", $row["jewEntw"]);
				foreach($jewEntw as $item) {
					if($item!=""){$allocation["jewEntw"][$item] = $targetgroups[$item];};
					unset($allocation["nichtEmpfohlen"][$item]);
				}
				$bedEmpfohlen = explode(" ", $row["bedEmpfohlen"]);
				foreach($bedEmpfohlen as $item) {
					if($item!=""){$allocation["bedEmpfohlen"][$item] = $targetgroups[$item];};
					unset($allocation["nichtEmpfohlen"][$item]);
				}
				$empfohlen = explode(" ", $row["empfohlen"]);
				foreach($empfohlen as $item) {
					if($item!=""){$allocation["empfohlen"][$item] = $targetgroups[$item];};
					unset($allocation["nichtEmpfohlen"][$item]);
				}
			}
			return $allocation;
		}
		
		function getFileObject($id) {
			return File::getByID($id);
		}
		
		function getImages() {
			$iID1 = $this->iID1;
			$iID2 = $this->iID2;
			$iID3 = $this->iID3;
			$iID4 = $this->iID4;
			$iID5 = $this->iID5;
			
			$anz = 0;
			if ($iID1 > 0) $anz++;
			if ($iID2 > 0) $anz++;
			if ($iID3 > 0) $anz++;
			if ($iID4 > 0) $anz++;
			if ($iID5 > 0) $anz++;
			
			$html = "";
			$counter = 0;
			if ($iID1 > 0) {
				$counter++;
				$image = $this->getImagePath($iID1);
				$html = "<a href='".$image."' class='fancy' title='Screenshot [".$counter."/".$anz."]' rel='g".$this->bID."'><span><img src='".$image."' data-src='".$image."' alt='' /></span></a>";
			}
			if ($iID2 > 0) {
				$counter++;
				$image = $this->getImagePath($iID2);
				if($html!=""){ $hidden = " hidden";}else{$hidden = "";};
				$html = $html."<a href='".$image."' class='fancy".$hidden."' title='Screenshot [".$counter."/".$anz."]' rel='g".$this->bID."'><img src='".$image."' data-src='".$image."' alt='' /></a>";
			}
			if ($iID3 > 0) {
				$counter++;
				$image = $this->getImagePath($iID3);
				if($html!=""){ $hidden = " hidden";}else{$hidden = "";};
				$html = $html."<a href='".$image."' class='fancy".$hidden."' title='Screenshot [".$counter."/".$anz."]' rel='g".$this->bID."'><img src='".$image."' data-src='".$image."' alt='' /></a>";
			}
			if ($iID4 > 0) {
				$counter++;
				$image = $this->getImagePath($iID4);
				if($html!=""){ $hidden = " hidden";}else{$hidden = "";};
				$html = $html."<a href='".$image."' class='fancy".$hidden."' title='Screenshot [".$counter."/".$anz."]' rel='g".$this->bID."'><img src='".$image."' data-src='".$image."' alt='' /></a>";
			}
			if ($iID5 > 0) {
				$counter++;
				$image = $this->getImagePath($iID5);
				if($html!=""){ $hidden = " hidden";}else{$hidden = "";};
				$html = $html."<a href='".$image."' class='fancy".$hidden."' title='Screenshot [".$counter."/".$anz."]' rel='g".$this->bID."'><img src='".$image."' data-src='".$image."' alt='' /></a>";
			}
			
			/*if ($counter > 1) {
				$html = $html."<br /><span>Bild 1 von ".$counter."</span>";
			}*/
			return $html;
		}
		
		function getResult() {
			$iRID = $this->iRID;
			
			if ($iRID > 0) {
				$image = $this->getImagePath($iRID);
				$html = "<a href='".$image."' class='fancy' title='Beispielergebnis' rel='g".$this->bID."'><span><img src='".$image."' data-src='".$image."' alt='' /></span></a>";
			}
			return $html;
		}
		
		function getImagePath($id) {
			$f = $this->getFileObject($id);
			$relPath = $f->getRelativePath();
			return $relPath;
		}		
	}

?>