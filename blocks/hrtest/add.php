﻿<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true;
$assetLibraryPassThru = array(
	'type' => 'image'
);
$al = Loader::helper('concrete/asset_library');
?>
<!--<script type="text/javascript" src="/themes/hr-d/_scripts/jquery-ui-1.7.2.custom.min.js"></script>-->
<script type="text/javascript">
$(function() {
	$("#nichtEmpfohlen,#bedEmpfohlen,#empfohlen,#jewEntw").sortable({
		connectWith: '.connectedList',
		placeholder: 'highlight',
		opacity: 0.7,
		update: function(event, ui) {
			var jewEntw = "";
			var bedEmpfohlen = "";
			var empfohlen = "";
			
			$("#jewEntw li").each(function(){ jewEntw = jewEntw+" "+$(this).attr("class"); });
			$("#bedEmpfohlen li").each(function(){ bedEmpfohlen = bedEmpfohlen+" "+$(this).attr("class"); });
			$("#empfohlen li").each(function(){ empfohlen = empfohlen+" "+$(this).attr("class"); });
			
			$("#f-jewEntw").val(jewEntw.replace(/^\s+/,""));
			$("#f-bedEmpfohlen").val(bedEmpfohlen.replace(/^\s+/,""));
			$("#f-empfohlen").val(empfohlen.replace(/^\s+/,""));
		}
	}).disableSelection();
});
</script>
<style type="text/css">
.list		 						{ float:left; width:200px;}
.list ul		 					{ padding:5px 10px 5px 0;}
.list li		 					{ background:  url(/concrete/images/bg_dialog_t.png) 0 -1px;
									border:1px solid #dedede;
									cursor:pointer;
									padding:2px 5px;
									list-style:none;}
.list li.highlight 					{ background:  url(/concrete/images/bg_header_active.png) 0 -1px;
									height:13px;}
.footer 							{ clear:both;}
</style>
<h2><strong>Allgemein</strong></h2>
<div class="ccm-block-field-group">
<h3><?php echo t('K&uuml;rzel')?></h3>
<?php echo  $form->text('kuerzel', array('style' => 'width: 250px')); ?>
</div>

<div class="ccm-block-field-group">
<h3><?php echo t('Name')?></h3>
<?php echo  $form->text('name', array('style' => 'width: 250px')); ?>
</div>

<div id="ccm-block-field-group">
<h3><?php echo t('Kurzbeschreibung')?></h3>
<textarea style="color: #444444; font-size:12px; height:60px; width:800px;" name="kurzbeschreibung"><?php echo $kurzbeschreibung ?></textarea>
</div>

<div class="ccm-block-field-group">
<h3><?php echo t('Zuordnung')?></h3>
<div class="list">	
	<h4>nicht empfohlen (3)</h4>
	<ul id="nichtEmpfohlen" class="connectedList">
		<?php
		$db = Loader::db();
		$r = $db->query("select * from targetgroups");
		while ($row = $r->fetchRow()) {
			echo '<li class="c'.$row["id"].'">'.$row["title"].'</li>';
		}
		?>
	</ul>
</div>
<div class="list">
	<h4>bedingt empfohlen (2)</h4>
	<ul id="bedEmpfohlen" class="connectedList">
	</ul>
</div>
<div class="list">
	<h4>empfohlen (1)</h4>
	<ul id="empfohlen" class="connectedList">
	</ul>
</div>
<div class="list">	
	<h4>jeweils entwickelt (4)</h4>
	<ul id="jewEntw" class="connectedList">
	</ul>
</div>
<div class="footer"></div>
<input type="hidden" id="f-bedEmpfohlen" name="bedEmpfohlen" value="<?php echo $bedEmpfohlen ?>" />
<input type="hidden" id="f-empfohlen" name="empfohlen" value="<?php echo $empfohlen ?>" />
<input type="hidden" id="f-jewEntw" name="jewEntw" value="<?php echo $jewEntw ?>" />
</div>

<ul id="ccm-autonav-tabs" class="ccm-dialog-tabs" style="padding-top:20px; margin-top:10px;">
	<li class="ccm-nav-active"><a id="ccm-autonav-tab-details" rel="details" href="javascript:void(0);"><?php echo t('Auf einen Blick')?></a></li>
	<li class=""><a id="ccm-autonav-tab-beschreibung" rel="beschreibung" href="javascript:void(0);"><?php echo t('Beschreibung')?></a></li>
	<li class=""><a id="ccm-autonav-tab-guetekriterien" rel="guetekriterien" href="javascript:void(0);"><?php echo t('G&uuml;tekriterien')?></a></li>
</ul>
<div style="padding: 10px">
	<div class="ccm-autonavPane" id="ccm-autonavPane-details">
		<div style="float:left; width:230px; ">
			<h2><?php echo t('Langversion')?></h2>
			<div id="ccm-block-field-group">
			<h3><?php echo t('Anzahl Items')?></h3>
			<?php echo  $form->text('langItems', $langItems, array('style' => 'width: 120px')); ?>
			</div>
			<div id="ccm-block-field-group">
			<h3><?php echo t('Anzahl Dimensionen')?></h3>
			<?php echo  $form->text('langDimensionen', $langDimensionen, array('style' => 'width: 120px')); ?>
			</div>
			<div id="ccm-block-field-group">
			<h3><?php echo t('Durchf&uuml;hrungsdauer')?></h3>
			<?php echo  $form->text('langDauer', $langDauer, array('style' => 'width: 120px')); ?>
			</div>
		</div>
		<div style="float:left; width:230px; ">
			<h2><?php echo t('Kurzversion')?></h2>
			<div id="ccm-block-field-group">
			<h3><?php echo t('Anzahl Items')?></h3>
			<?php echo  $form->text('kurzItems', $kurzItems, array('style' => 'width: 120px')); ?>
			</div>
			<div id="ccm-block-field-group">
			<h3><?php echo t('Anzahl Dimensionen')?></h3>
			<?php echo  $form->text('kurzDimensionen', $kurzDimensionen, array('style' => 'width: 120px')); ?>
			</div>
			<div id="ccm-block-field-group">
			<h3><?php echo t('Durchf&uuml;hrungsdauer')?></h3>
			<?php echo  $form->text('kurzDauer', $kurzDauer, array('style' => 'width: 120px')); ?>
			</div>
		</div>
		<div style="float:left; width:230px; ">		
			<h2><?php echo t('Allgemein')?></h2>
			<div id="ccm-block-field-group">
			<h3><?php echo t('Personalauswahl')?></h3>
			<?php echo  $form->checkbox('isPersonalauswahl','1',$isPersonalauswahl); ?> geeignet
			</div>
			<div id="ccm-block-field-group">
			<h3><?php echo t('Personalentwicklung')?></h3>
			<?php echo  $form->checkbox('isPersonalentwicklung','1',$isPersonalentwicklung); ?> geeignet
			</div>
			<div id="ccm-block-field-group">
			<h3><?php echo t('ungesch&uuml;tzte Bearbeitung')?></h3>
			<?php echo  $form->checkbox('isUngBearb','1',$isUngBearb); ?> m&ouml;glich
			</div>
		</div>
		<div style="clear:both"></div>
		<h3><?php echo t('Besonderheit')?></h3>
		<textarea id="ccm-content-3" class="advancedEditor" name="besonderheit"><?php echo $besonderheit ?></textarea>		
	</div>
	<div class="ccm-autonavPane" id="ccm-autonavPane-beschreibung" style="display: none">
		<div id="ccm-block-field-group">
		<h3><?php echo t('Beschreibung')?></h3>
		<textarea id="ccm-content-2" class="advancedEditor" name="beschreibung"><?php echo $beschreibung ?></textarea>
		</div>
	</div>
	<div class="ccm-autonavPane" id="ccm-autonavPane-guetekriterien" style="display: none">
		<div id="ccm-block-field-group">
		<h3><?php echo t('G&uuml;tekriterien')?></h3>
		<textarea id="ccm-content-4" class="advancedEditor" name="guetekriterien"><?php echo $guetekriterien ?></textarea>
		</div>
	</div>
</div>
	
<h2 style="padding-top:20px; margin-top:10px; border-top:1px dotted #aaaaaa;"><strong>Bilder</strong></h2>
<div class="ccm-block-field-group">
<h3><?php echo t('Screenshots')?></h3>
<?php
	$iO1 = null;
	$iO2 = null;
	$iO3 = null;
	$iO4 = null;
	$iO5 = null;
	echo $al->image('image1', 'iID1', t('Choose Image'), $iO1);
	echo $al->image('image2', 'iID2', t('Choose Image'), $iO2);
	echo $al->image('image3', 'iID3', t('Choose Image'), $iO3);
	echo $al->image('image4', 'iID4', t('Choose Image'), $iO4);
	echo $al->image('image5', 'iID5', t('Choose Image'), $iO5);
?>
</div>
<div class="ccm-block-field-group">
<h3><?php echo t('Beispielergebnisse')?></h3>
<?php
	$iRO = null;
	echo $al->image('imageR', 'iRID', t('Choose Image'), $iRO);
?>
</div>
