<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
//global $initHRtest;
$c = Page::getByID($_REQUEST['cID'], $_REQUEST['cvID']);
/*if($initHRtest==true && !$c->isEditMode()) {
	?><script type="text/javascript" src="/blocks/hrtest/hrtest.js"></script><?php
}*/
//$initHRtest = false;
$id = $controller->getID();
?>
<li id="test<?php echo($id); ?>" class="test">
	<h3>
		<span class="arrow">&nbsp;</span>
		<span class="label"><?php echo($controller->getKuerzel()); ?><em> | <?php echo($controller->getName()); ?> </em></span>
		<span class="referral">&nbsp;</span>
		<span class="properties">
			<?php
			if ($_SESSION["lang"] == "de") { 
				if ($isPersonalauswahl != 1) { $personalauswahl = "zur Personalauswahl ungeeignet"; } else { $personalauswahl = "zur Personalauswahl geeignet";}
				if ($isPersonalentwicklung != 1) { $personalentwicklung  = "zur Personalentwicklung ungeeignet"; } else { $personalentwicklung = "zur Personalentwicklung geeignet";}
				if ($isUngBearb != 1) { $ungBearb = "keine ungesch&uuml;tzte Bearbeitung m&ouml;glich"; } else { $ungBearb = "ungesch&uuml;tzte Bearbeitung m&ouml;glich";}
			} else {
				if ($isPersonalauswahl != 1) { $personalauswahl = "not qualified for personnel selection"; } else { $personalauswahl = "qualified for personnel selection";}
				if ($isPersonalentwicklung != 1) { $personalentwicklung  = "not qualified for personnel development"; } else { $personalentwicklung = "qualified for personnel development";}
				if ($isUngBearb != 1) { $ungBearb = "unprotected editing not possible"; } else { $ungBearb = "unprotected editing possible";}
			}
			?>
			<img src="/themes/hr-d/_images/test/<?php if ($isPersonalauswahl == 1) { echo "check"; } else { echo "cross"; } ?>.png" alt="" title="<?php echo $personalauswahl; ?>" />
			<img src="/themes/hr-d/_images/test/<?php if ($isPersonalentwicklung == 1) { echo "check"; } else { echo "cross"; } ?>.png" alt="" title="<?php echo $personalentwicklung; ?>" />
			<img src="/themes/hr-d/_images/test/<?php if ($isUngBearb == 1) { echo "check"; } else { echo "cross"; } ?>.png" alt="" title="<?php echo $ungBearb; ?>" />
		</span>
		<span class="footer"></span>
	</h3>
<?php if (!$c->isEditMode()) { ?>
	<div class="details">
		<div id="kurzbeschreibung">
			<p><?php echo $kurzbeschreibung; ?></p>
			<div class="footer"></div>
		</div>
		<div id="tabs<?php echo($id); ?>" class="flora">
			<ul class="ui-tabs-nav">
				<li class="ui-tabs-selected"><a href="#fragment-<?php echo($id); ?>-1"><span><?php if ($_SESSION["lang"] == "de") { echo "Auf einen Blick"; } else { echo "At a glance"; } ?></span></a></li>
				<?php /*?><li><a href="#fragment-<?php echo($id); ?>-2"><span>Beschreibung</span></a></li><?php */?>
				<li><a href="#fragment-<?php echo($id); ?>-3"><span><?php if ($_SESSION["lang"] == "de") { echo "G&uuml;tekriterien"; } else { echo "Psychometric properties"; } ?></span></a></li>
			</ul>
			
			<div id="fragment-<?php echo($id); ?>-1">
				<div class="leftcol">
					<div class="images"><?php echo $controller->getImages(); ?></div>
					<div class="images"><?php echo $controller->getResult(); ?></div>
				</div>
				<div class="rightcol">
					<table>
						<thead>
							<tr>
								<td></td>
								<?php if($kurzItems!="0") { ?><td class="headline"><?php if ($_SESSION["lang"] == "de") { echo "Kurzversion"; } else { echo "Short version"; } ?></td><?php } ?>
								<td class="headline"><?php if ($_SESSION["lang"] == "de") { echo "Langversion"; } else { echo "Long version"; } ?></td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th><?php if ($_SESSION["lang"] == "de") { echo "Anzahl Items:"; } else { echo "# of items:"; } ?></th>
								<?php if($kurzItems!="0") { echo "<td>".$kurzItems."</td>"; }?>
								<td><?php echo $langItems ?></td>
							</tr>
							<tr>				
								<th><?php if ($_SESSION["lang"] == "de") { echo "Anzahl Dimensionen:"; } else { echo "# of dimensions"; } ?></th>
								<?php if($kurzItems!="0") { echo "<td>".$kurzDimensionen."</td>"; }?>
								<td><?php echo $langDimensionen ?></td>
							</tr>
							<tr>
								<th><?php if ($_SESSION["lang"] == "de") { echo "Durchf&uuml;hrungsdauer:"; } else { echo "Duration:"; } ?></th>
								<?php if($kurzItems!="0") { echo "<td>".$kurzDauer." min</td>"; }?>
								<td><?php echo $langDauer ?> min</td>
							</tr>
						</tbody>
					</table>
					<div class="hr"><hr /></div>
					<div class="besonderheiten">
						<?php echo $besonderheit; ?>
					</div>
				</div>
				<div class="footer"></div>
			</div>
			<?php /*?><div id="fragment-<?php echo($id); ?>-2">
				<?php echo $beschreibung; ?>
			</div><?php */?>
			<div id="fragment-<?php echo($id); ?>-3" class="guetekriterien">
				<?php echo $guetekriterien; ?>
			</div>
		</div>
	</div>
<?php } ?>
</li>
<?php /*?>
<div class="hrtest">
		<?php echo($controller->getContentAndGenerate()); ?>
</div>

<h3><span class="arrow"></span><span class="label">LOGIC<em> | Logi(sti)ktest</em></span><span class="referral">bed. empf.</span></h3><?php */?>