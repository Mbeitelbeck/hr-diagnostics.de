<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true;
$assetLibraryPassThru = array(
	'type' => 'image'
);
$al = Loader::helper('concrete/asset_library');
?>
<h2><strong>Inhalt</strong></h2>
<div class="ccm-block-field-group">
<h3><?php echo t('&Uuml;berschrift')?></h3>
<?php echo  $form->text('headline', $headline, array('style' => 'width: 250px')); ?>
</div>
<div id="ccm-block-field-group">
<h3><?php echo t('Text')?></h3>
<textarea id="ccm-content-<?php echo $a->getAreaID()?>" class="ccm-advanced-editor" name="content"></textarea>
</div>

<h2 style="padding-top:20px; margin-top:10px; border-top:1px dotted #aaaaaa;"><strong>Bild</strong></h2>
<div class="ccm-block-field-group">
<?php echo $al->image('fID', 'fID', t('Choose Image'), $fID);?>
</div>
<div class="ccm-block-field-group">
<h3><?php echo t('Style')?></h3>
<?php echo  $form->text('style', $style, array('style' => 'width: 250px')); ?>
</div>


<h2 style="padding-top:20px; margin-top:10px; border-top:1px dotted #aaaaaa;"><strong>Varianten</strong></h2>

<div class="ccm-block-field-group">
<h3><?php echo t('externer Link')?></h3>
<?php echo  $form->text('extLink', $extLink, array('style' => 'width: 250px')); ?>
</div>
<div class="ccm-block-field-group">
<h3><?php echo t('gro&szlig;es Bild')?></h3>
<?php echo $al->image('fBigID', 'fBigID', t('Choose Image'), $fBigID);?>
</div>

<div class="ccm-block-field-group">
<h3><?php echo t('interner Link')?></h3>
<?php  $form = Loader::helper('form/page_selector');
print $form->selectPage('intLink', $intLink);?>
</div>


