<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$c = Page::getByID($_REQUEST['cID'], $_REQUEST['cvID']);

$p = Page::getByID($link);
$pagePath = $p->cPath;

$i = $controller->getFileObject($fID);
$imagePath = $i->getRelativePath();
?>
<div class="diagonal-box">
	<div class="content">
		<div class="textcol">
			<?php echo $content; ?>
		</div>
		<div class="imagecol" style="<?php echo $style ?>">
			<img src="<?php echo $imagePath ?>" alt="<?php echo $fID; ?>" />
		</div>
		<div class="footer"></div>
	</div>
</div>