<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$c = Page::getByID($_REQUEST['cID'], $_REQUEST['cvID']);

$i = $controller->getFileObject($fID);
$imagePath = $i->getRelativePath();
?>
<div class='diagonal-box zitat'>
	<img src="<?php echo $imagePath ?>" alt="<?php echo $fID; ?>" />
	<div class='content'>
		<?php echo $content; ?>
	</div>
	<div class="footer"></div>
</div>
<div class="footer"></div>