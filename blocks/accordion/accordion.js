$(document).ready(function(){
	$(".accordion .content").hide();
	$(".accordion .top").click(function(){
		if($(this).hasClass("open")){
			$(this).parent().find(".content").slideUp(200);
			$(this).removeClass("open");
		} else {
//			$(".accordion .open").parent().find(".content").slideUp();
//			$(".accordion .open").removeClass("open");
			$(this).parent().find(".content").slideDown(200);
			$(this).addClass("open");
		}
	});
});
