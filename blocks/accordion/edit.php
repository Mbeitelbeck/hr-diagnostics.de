﻿<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true; 
$assetLibraryPassThru = array(
	'type' => 'image'
);
$al = Loader::helper('concrete/asset_library');
echo $choiceS = $controller->getChoice("S");
echo $choiceM = $controller->getChoice("M");
echo $choiceXL = $controller->getChoice("XL");

?>
<h2><strong>Allgemein</strong></h2>
<div class="ccm-block-field-group">
<h2><?php echo t('Titel')?></h2>
<?php echo  $form->text('title', $title, array('style' => 'width: 250px')); ?>
</div>

<h2><strong>Inhalt</strong></h2>
<div id="ccm-block-field-group">
<h2><?php echo t('Text')?></h2>
<textarea id="ccm-content-<?php echo $b->getBlockID()?>-<?php echo $a->getAreaID()?>" class="ccm-advanced-editor" name="content"><?php echo $controller->getContentEditMode()?></textarea>
</div>

<h2 style="padding-top:20px; margin-top:10px; border-top:1px dotted #aaaaaa;"><strong><?php echo t('S')?></strong></h2>
<div class="ccm-block-field-group">
<input<?php if ($choiceS=="verfuegbar") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceS" id="choiceS4" value="verfuegbar" type="radio">verf&uuml;gbar/inklusive<br>
<input<?php if ($choiceS=="optional") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceS" id="choiceS5" value="optional" type="radio">optional<br>
<input<?php if ($choiceS=="nicht-verfuegbar") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceS" id="choiceS6" value="nicht-verfuegbar" type="radio">nicht verf&uuml;gbar
</div>

<h2><strong><?php echo t('M')?></strong></h2>
<div class="ccm-block-field-group">
<input<?php if ($choiceM=="verfuegbar") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceM" id="choiceM4" value="verfuegbar" type="radio">verf&uuml;gbar/inklusive<br>
<input<?php if ($choiceM=="optional") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceM" id="choiceM5" value="optional" type="radio">optional<br>
<input<?php if ($choiceM=="nicht-verfuegbar") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceM" id="choiceM6" value="nicht-verfuegbar" type="radio">nicht verf&uuml;gbar
</div>

<h2><strong><?php echo t('XL')?></strong></h2>
<div class="ccm-block-field-group">
<input<?php if ($choiceXL=="verfuegbar") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceXL" id="choiceXL4" value="verfuegbar" type="radio">verf&uuml;gbar/inklusive<br>
<input<?php if ($choiceXL=="optional") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceXL" id="choiceXL5" value="optional" type="radio">optional<br>
<input<?php if ($choiceXL=="nicht-verfuegbar") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceXL" id="choiceXL6" value="nicht-verfuegbar" type="radio">nicht verf&uuml;gbar
</div>