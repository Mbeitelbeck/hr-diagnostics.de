<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

$imagePath = File::getRelativePathFromID($fID);
$fileBigPath = File::getRelativePathFromID($fOnstateID);
?>

<?php
$img = "<img class=\"ccm-image-block\" alt=\"{$altText}\" src=\"{$imagePath}\" />";

if($fileBigPath != ""){
	$img = "<a href=\"{$fileBigPath}\" rev=\"{$altText}\" class='fancy' rel='group'>" . $img ."</a>";
}

echo $img;
?>