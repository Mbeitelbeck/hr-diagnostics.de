<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$includeAssetLibrary = true;
$al = Loader::helper('concrete/asset_library');
?>
<h2><strong>Alias-Bereich einfügen</strong></h2>
<h3>Aus folgendem Bereich Inhalt einfügen:</h3>
<div class="ccm-block-field-group">
	<?php  echo $form->radio('area','Slider', 'Slider');?> <?php   echo t('Slider');?> 
	<?php  echo $form->radio('area','RechteSpalte', 'RechteSpalte');?> <?php   echo t('Rechte Spalte');?> 
	<?php  echo $form->radio('area','Inhalt', 'Inhalt');?> <?php   echo t('Inhalt');?> 
</div>

<h3>Aus dieser Seite den Inhalt des Bereichs anzeigen</h3>
<div class="ccm-block-field-group">
	<?php  $form = Loader::helper('form/page_selector');
	print $form->selectPage('alias', $alias);?>
</div>
