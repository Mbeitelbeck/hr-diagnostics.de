<?php 
	defined('C5_EXECUTE') or die(_("Access Denied."));
	require_once(DIR_FILES_BLOCK_TYPES_CORE . '/library_file/controller.php');
	
	class ToaliasBlockController extends BlockController {

		protected $btInterfaceWidth = 320;
		protected $btInterfaceHeight = 350;
		protected $btTable = 'btTOAlias';

		/** 
		 * Used for localization. If we want to localize the name/description we have to include this
		 */
		public function getBlockTypeDescription() {
			return t("F&uuml;gt den Inhalt eines bestimmten Bereichs einer bestimmten Seite hinzu");
		}
				
		public function getBlockTypeName() {
			return t("t:o | Alias-Bereich");
		}

		public function getJavaScriptStrings() {
			return array('file-required' => t('You must select a file.'));	
		}
		
		public function save($args) {		
			$args['alias'] = ($args['alias'] != '') ? $args['alias'] : "";
			$args['area'] = ($args['area'] != '') ? $args['area'] : "";
			parent::save($args);
		}
		
		/*public function getCurrentArea() {
			$c = Page::getCurrentPage();
			$b = Block::getByID($this->bID, $c, "Teaser");
			$a = $b->getBlockAreaObject();
			return $a->getAreaHandle();
		}*/
	}
?>
