<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true;
$assetLibraryPassThru = array(
	'type' => 'image'
);
$al = Loader::helper('concrete/asset_library');
?>
<h2><strong>Allgemein</strong></h2>
<div class="ccm-block-field-group">
<h3><?php echo t('Unter&uuml;berschirft')?></h3>
<?php echo  $form->text('subtitle', $subtitle, array('style' => 'width: 250px')); ?>
</div>
<div class="ccm-block-field-group">
<h3><?php echo t('&Uuml;berschirft')?></h3>
<?php echo  $form->text('title', $title, array('style' => 'width: 500px')); ?>
</div>
<div id="ccm-block-field-group">
<h3><?php echo t('Text')?></h3>
<textarea id="ccm-content-<?php echo $a->getAreaID()?>" class="ccm-advanced-editor" name="content"><?php echo $content ?></textarea>
</div>
<div id="ccm-block-field-group">
<h3><?php echo t('Link')?></h3>
<?php echo  $form->text('link', $link, array('style' => 'width: 250px')); ?>
</div>