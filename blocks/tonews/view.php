<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$c = Page::getByID($_REQUEST['cID'], $_REQUEST['cvID']);

$p = Page::getByID($link);
$pagePath = $p->cPath;

?>
<div class="hr"><hr /></div>
<div class="news">
	<div class="top">
		<h5><?php echo $subtitle; ?></h5>
		<h4><?php echo $title; ?></h4>
	</div>
	<div class="content">
		<?php echo $content; ?>
	</div>
	<?php if($link!="") { ?>
	<div class="bottom">
		<p>
			<a title="<?php echo $title ?>" href="<?php echo $link ?>">&Ouml;ffnen</a>
		</p>
	</div>
	<?php } ?>
</div>