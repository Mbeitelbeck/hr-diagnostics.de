<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true;
$assetLibraryPassThru = array(
	'type' => 'image'
);
$al = Loader::helper('concrete/asset_library');
if ($fID > 0) { $sf = $controller->getFileObject(); }
?>
<h2><strong>Allgemein</strong></h2>
<div class="ccm-block-field-group">
<h3><?php echo t('Datum')?></h3>
<?php echo  $form->text('datum', $datum, array('style' => 'width: 250px')); ?>
</div>
<div class="ccm-block-field-group">
<h3><?php echo t('Medium')?></h3>
<?php echo  $form->text('medium', $medium, array('style' => 'width: 250px')); ?>
</div>
<div class="ccm-block-field-group">
<h3><?php echo t('&Uuml;berschirft')?></h3>
<?php echo  $form->text('title', $title, array('style' => 'width: 500px')); ?>
</div>
<div id="ccm-block-field-group">
<h3><?php echo t('Text')?></h3>
<textarea id="ccm-content-<?php echo $a->getAreaID()?>" class="ccm-advanced-editor" name="content"><?php echo $content; ?></textarea>
</div>

<h2 style="padding-top:20px; margin-top:10px; border-top:1px dotted #aaaaaa;"><strong>Bericht</strong></h2>
<div class="ccm-block-field-group">
<h3><?php echo t('Datei')?></h3>
<?php echo $al->file('ccm-b-file', 'fID', t('Choose File'), $sf);?>
</div>
<div id="ccm-block-field-group">
<h3><?php echo t('oder Link')?></h3>
<?php echo  $form->text('link', $link, array('style' => 'width: 250px')); ?>
</div>

<h2 style="padding-top:20px; margin-top:10px; border-top:1px dotted #aaaaaa;"><strong>Dateiinformationen</strong></h2>
<div id="ccm-block-field-group">
<h3><?php echo t('Dateiinformation')?></h3>
<?php echo  $form->text('info', $info, array('style' => 'width: 250px')); ?>
</div>
<div id="ccm-block-field-group">
<h3><?php echo t('Dateityp')?></h3>
<?php echo  $form->text('typ', $typ, array('style' => 'width: 250px')); ?>
</div>
<div id="ccm-block-field-group">
<h3><?php echo t('Dateigr&ouml;&szlig;e')?></h3>
<?php echo  $form->text('groesse', $groesse, array('style' => 'width: 250px')); ?>
</div>
