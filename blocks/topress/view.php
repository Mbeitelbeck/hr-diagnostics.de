<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$c = Page::getByID($_REQUEST['cID'], $_REQUEST['cvID']);

$p = Page::getByID($link);
$pagePath = $p->cPath;

$filePath = File::getRelativePathFromID($fID);
?>
<div class="hr"><hr /></div>
<div class="presse">
	<div class="top">
		<h5><?php echo $datum; ?> | <?php echo $medium; ?></h5>
		<h4><?php echo $title; ?></h4>
	</div>
	<div class="content">
		<?php echo $content; ?>
	</div>
	<div class="bottom">
		<p>
			<?php if ($fID != 0) { ?>
				<a onclick="_gaq.push(['_trackEvent', 'PDF', '<?php echo $filePath ?>'])" title="<?php echo $title ?>" href="<?php echo $filePath ?>" target="_blank";>Download</a>&nbsp;<?php if ($info != "") { echo "(".$info.")"; } ?>&nbsp;|&nbsp;
			<?php } else { ?>
				<a onclick="_gaq.push(['_trackEvent', 'HTML', '<?php echo $link ?>'])" title="<?php echo $title ?>" href="<?php echo $link ?>" target="_blank";>&Ouml;ffnen</a>&nbsp;<?php if ($info != "") { echo "(".$info.")"; } ?>&nbsp;|&nbsp;
			<?php } 
			echo $typ; 
			if($groesse != "") { echo "&nbsp;|&nbsp;".$groesse; } ?>
		</p>
	</div>
</div>