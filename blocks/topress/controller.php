<?php
	Loader::block('library_file');
	defined('C5_EXECUTE') or die(_("Access Denied."));	
	global $initAccordion;
	$initAccordion=true;
	class TopressBlockController extends BlockController {

		protected $btInterfaceWidth = "615";
		protected $btInterfaceHeight = "465";
		protected $btTable = 'btTOPress';

		/** 
		 * Used for localization. If we want to localize the name/description we have to include this
		 */
		public function getBlockTypeDescription() {
			return t("Erstellt einen Pressebericht.");
		}
		
		public function getBlockTypeName() {
			return t("Pressebericht");
		}		
		
		public function getJavaScriptStrings() {
			return array(
				'image-required' => t('You must select an image.')
			);
		}
		
		// HTML-EDITOR
		
		function br2nl($str) {
			$str = str_replace("\r\n", "\n", $str);
			$str = str_replace("<br />\n", "\n", $str);
			return $str;
		}
		
		function getContentEditMode() {
			$content = $this->translateFrom($this->content);
			return $content;				
		}
		function translateFrom($text) {
			// old stuff. Can remove in a later version.
			$text = str_replace('href="{[CCM:BASE_URL]}', 'href="' . BASE_URL . DIR_REL, $text);
			$text = str_replace('src="{[CCM:REL_DIR_FILES_UPLOADED]}', 'src="' . BASE_URL . REL_DIR_FILES_UPLOADED, $text);

			// we have the second one below with the backslash due to a screwup in the
			// 5.1 release. Can remove in a later version.

			$text = preg_replace(
				array(
					'/{\[CCM:BASE_URL\]}/i',
					'/{CCM:BASE_URL}/i'),
				array(
					BASE_URL . DIR_REL,
					BASE_URL . DIR_REL)
				, $text);
				
			/*// now we add in support for the links
			
			$text = preg_replace_callback(
				'/{CCM:CID_([0-9]+)}/i',
				array('ContentBlockController', 'replaceCollectionID'),				
				$text);

			// now we add in support for the files
			
			$text = preg_replace_callback(
				'/{CCM:FID_([0-9]+)}/i',
				array('ContentBlockController', 'replaceFileID'),				
				$text);*/
			

			return $text;
		}
		private function replaceFileID($match) {
			$fID = $match[1];
			if ($fID > 0) {
				$path = File::getRelativePathFromID($fID);
				return $path;
			}
		}
		
		private function replaceCollectionID($match) {
			$cID = $match[1];
			if ($cID > 0) {
				$path = Page::getCollectionPathFromID($cID);
				if (URL_REWRITING == true) {
					$path = DIR_REL . $path;
				} else {
					$path = DIR_REL . '/' . DISPATCHER_FILENAME . $path;
				}
				return $path;
			}
		}
		
		function translateTo($text) {
			// keep links valid
			$url1 = str_replace('/', '\/', BASE_URL . DIR_REL . '/' . DISPATCHER_FILENAME);
			$url2 = str_replace('/', '\/', BASE_URL . DIR_REL);
			$url3 = View::url('/download_file', 'view_inline');
			$url3 = str_replace('/', '\/', $url3);
			$url3 = str_replace('-', '\-', $url3);
			
			$text = preg_replace(
				array(
					'/' . $url1 . '\?cID=([0-9]+)/i', 
					'/' . $url3 . '\/([0-9]+)/i', 
					'/' . $url2 . '/i'),
				array(
					'{CCM:CID_\\1}',
					'{CCM:FID_\\1}',
					'{CCM:BASE_URL}')
				, $text);
			return $text;
		}
		// ENDE
		
		function getContent() {
			$content = $this->translateFrom($this->content);
			return $content;				
		}
	
	
		function getFileID() {return $this->fID;}
		function getFileOnstateID() {return $this->fOnstateID;}
		function getFileOnstateObject() {
			if ($this->fOnstateID > 0) {
				return File::getByID($this->fOnstateID);
			}
		}
		function getFileObject() {
			return File::getByID($this->fID);
		}		
		function getAltText() {return $this->altText;}
		function getExternalLink() {return $this->externalLink;}
		
		public function save($args) {		
			$args['datum'] = ($args['datum'] != '') ? $args['datum'] : "";
			$args['content'] = ($args['content'] != '') ? $args['content'] : "";			
			$args['title'] = ($args['title'] != '') ? $args['title'] : "";
			$args['fID'] = ($args['fID'] != '') ? $args['fID'] : 0;
			$args['link'] = ($args['link'] != '') ? $args['link'] : "";
			$args['info'] = ($args['info'] != '') ? $args['info'] : "";
			$args['medium'] = ($args['medium'] != '') ? $args['medium'] : "";
			$args['typ'] = ($args['typ'] != '') ? $args['typ'] : "";
			$args['groesse'] = ($args['groesse'] != '') ? $args['groesse'] : "";			
			parent::save($args);
		}

		function getContentAndGenerate($align = false, $style = false, $id = null) {
/*			$db = Loader::db();
			global $c;
			$bID = $this->bID;
			
			$f = $this->getFileObject();
			$fullPath = $f->getPath();
			$relPath = $f->getRelativePath();
			
			$fancyLinkImg = $relPath;
			$fancyTitle = $this->altText;
			if($this->getFileOnstateObject() != ""){
				$fancyBigImg = $this->getFileOnstateObject()->getRelativePath();
			}
			$fancyContentURL = $this->externalLink;
			
			if($fancyContentURL != ""){
				$img = "<a class=\"fancybox iframe\" href=\"$fancyContentURL\"><img alt=\"$fancyTitle\" src=\"$fancyLinkImg\" width=\"180\" /></a>";
			}  elseif($fancyBigImg != "") {
				 $img = "<a class=\"fancybox\" href=\"$fancyBigImg\"><img alt=\"$fancyTitle\" src=\"$fancyLinkImg\" width=\"180\" /></a>"; 
			}  else {
				$img = "<a class=\"fancybox\" href=\"http://www.google.de\"><img alt=\"$fancyTitle\" src=\"$fancyLinkImg\" width=\"180\" /></a>";
			}; 

			return $img;
*/		}

	}

?>