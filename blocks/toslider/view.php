<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
global $initSlider;
$c = Page::getByID($_REQUEST['cID'], $_REQUEST['cvID']);
if($initSlider==true && !$c->isEditMode()) {
	?><script type="text/javascript" src="/blocks/toslider/slider.js"></script><?php
}
$initSlider = false;
?>
<div class="slider">
	<div class="top">
		<h4><?php echo $title; ?></h4>
	</div>
	<div class="content">
		<?php echo $content; ?>
		<div class="footer"></div>
	</div>
</div>