<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true; 
$assetLibraryPassThru = array(
	'type' => 'image'
);
$al = Loader::helper('concrete/asset_library');
?>
<h2><strong>Allgemein</strong></h2>
<div class="ccm-block-field-group">
<h2><?php echo t('Titel')?></h2>
<?php echo  $form->text('title', $title, array('style' => 'width: 250px')); ?>
</div>

<h2><strong>Inhalt</strong></h2>
<div id="ccm-block-field-group">
<h2><?php echo t('Text')?></h2>
<textarea id="ccm-content-<?php echo $a->getAreaID()?>" class="advancedEditor ccm-advanced-editor" name="content"><?php echo $controller->getContentEditMode()?></textarea>

</div>