$(document).ready(function(){
	$(".slider .content").hide();
	$(".slider .top").click(function(){
		if($(this).hasClass("open")){
			$(this).parent().find(".content").slideUp();
			$(this).removeClass("open");
		} else {
			$(".slider .open").parent().find(".content").slideUp("fast");
			$(".slider .open").removeClass("open");
			
			$(this).parent().find(".content").slideDown("fast");
			$(this).addClass("open");
		}
	});
});