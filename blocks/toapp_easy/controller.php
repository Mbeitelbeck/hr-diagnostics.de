<?php
	Loader::block('library_file');
	defined('C5_EXECUTE') or die(_("Access Denied."));	
	global $initAccordion;
	$initAccordion=true;
	class ToappenBlockController extends BlockController
	{
		protected $btInterfaceWidth = "900";
		protected $btInterfaceHeight = "700";
		protected $btTable = 'btTOAppEnVersion';
	
		/** 
		 * Used for localization. If we want to localize the name/description we have to include this
		 */
		public function getBlockTypeDescription() {
			return t("Creates an App-Block (EN).");
		}
		
		public function getBlockTypeName() {
			return t("t:o | App-Block-English");
		}		
		
		public function getJavaScriptStrings() {
			return array(
				'image-required' => t('You must select an image.')
			);
		}
		
		// HTML-EDITOR
		
		function br2nl($str) {
			$str = str_replace("\r\n", "\n", $str);
			$str = str_replace("<br />\n", "\n", $str);
			return $str;
		}
		
		function getContentEditMode() {
			$content = $this->translateFrom($this->content);
			return $content;				
		}
		function translateFrom($text) {
			// old stuff. Can remove in a later version.
			$text = str_replace('href="{[CCM:BASE_URL]}', 'href="' . BASE_URL . DIR_REL, $text);
			$text = str_replace('src="{[CCM:REL_DIR_FILES_UPLOADED]}', 'src="' . BASE_URL . REL_DIR_FILES_UPLOADED, $text);
	
			// we have the second one below with the backslash due to a screwup in the
			// 5.1 release. Can remove in a later version.
	
			$text = preg_replace(
				array(
					'/{\[CCM:BASE_URL\]}/i',
					'/{CCM:BASE_URL}/i'),
				array(
					BASE_URL . DIR_REL,
					BASE_URL . DIR_REL)
				, $text);
				
			/*// now we add in support for the links
			
			$text = preg_replace_callback(
				'/{CCM:CID_([0-9]+)}/i',
				array('ContentBlockController', 'replaceCollectionID'),				
				$text);
	
			// now we add in support for the files
			
			$text = preg_replace_callback(
				'/{CCM:FID_([0-9]+)}/i',
				array('ContentBlockController', 'replaceFileID'),				
				$text);*/
			
	
			return $text;
		}
		private function replaceFileID($match) {
			$fID = $match[1];
			if ($fID > 0) {
				$path = File::getRelativePathFromID($fID);
				return $path;
			}
		}
		
		private function replaceCollectionID($match) {
			$cID = $match[1];
			if ($cID > 0) {
				$path = Page::getCollectionPathFromID($cID);
				if (URL_REWRITING == true) {
					$path = DIR_REL . $path;
				} else {
					$path = DIR_REL . '/' . DISPATCHER_FILENAME . $path;
				}
				return $path;
			}
		}
		
		function translateTo($text) {
			// keep links valid
			$url1 = str_replace('/', '\/', BASE_URL . DIR_REL . '/' . DISPATCHER_FILENAME);
			$url2 = str_replace('/', '\/', BASE_URL . DIR_REL);
			$url3 = View::url('/download_file', 'view_inline');
			$url3 = str_replace('/', '\/', $url3);
			$url3 = str_replace('-', '\-', $url3);
			
			$text = preg_replace(
				array(
					'/' . $url1 . '\?cID=([0-9]+)/i', 
					'/' . $url3 . '\/([0-9]+)/i', 
					'/' . $url2 . '/i'),
				array(
					'{CCM:CID_\\1}',
					'{CCM:FID_\\1}',
					'{CCM:BASE_URL}')
				, $text);
			return $text;
		}
		// ENDE
		
		function getContent() {
			$content = $this->translateFrom($this->content);
			return $content;				
		}
	
	
		function getFileID() {return $this->fID;}
		function getFileOnstateID() {return $this->fOnstateID;}
		function getFileOnstateObject() {
			if ($this->fOnstateID > 0) {
				return File::getByID($this->fOnstateID);
			}
		}
		function getFileObject($id) {
			return File::getByID($id);
		}		
		function getAltText() {return $this->altText;}
		function getExternalLink() {return $this->externalLink;}
		
		
		public function save($args)
		{
			$db = Loader::db();
			$args['link'] = ($args['link'] != '') ? $args['link'] : "";
			$args['bestellen'] = ($args['bestellen'] != '') ? $args['bestellen'] : "";
			$args['content'] = ($args['content'] != '') ? $args['content'] : "";
			$args['kurzbeschreibung'] = ($args['kurzbeschreibung'] != '') ? $args['kurzbeschreibung'] : "";
			$args['title'] = ($args['title'] != '') ? $args['title'] : "";
			$args['preis'] = ($args['preis'] != '') ? $args['preis'] : "";
			$args['testtyp'] = ($args['testtyp'] != '') ? $args['testtyp'] : "";
			$anz = count($args['dimensionen']);
			
			if(!empty($args['dimensionen']) || isset($args['dimensionen']))
			{
				if($anz >= 2)
				{
					$args['dimensionen'] = implode(",", $args['dimensionen']);
				}
				else
				{
					if($anz == 1)
					{
						foreach($args['dimensionen'] as $key => $value)
						{
							$args['dimensionen'] = $value;
						}
					}
				}
			}
			else
			{
				$args['dimensionen'] = '';
			}
			
			$anz = count($args['features']);
			if(!empty($args['features']) || isset($args['features']))
			{
				if($anz >= 2)
				{
					$args['features'] = implode(",", $args['features']);
				}
				else
				{
					if($anz == 1)
					{
						foreach($args['features'] as $key => $value)
						{
							$args['features'] = $value;
						}
					}
				}
			}
			else
			{
				$args['features'] = '';
			}
			parent::save($args);
		}
		
		
		public function getDimensionen($typ)
		{
			$db = Loader::db();
			$rs = $db->GetAll("SELECT akID FROM AttributeKeys WHERE akHandle = ?",array($typ));
			$result = $db->GetAll("SELECT akID,value FROM atSelectOptions WHERE akID = ?",array($rs[0]['akID']));
	
			foreach($result as $begriff)
			{
				foreach($begriff as $name => $typ)
				{
					if($name == 'akID')  continue;
	
					$typs[] = $typ;
				}
			}
			return $typs;
		}
		
		
		public function getFeatures()
		{
			$db = Loader::db();
			$rs = $db->GetAll("SELECT akID FROM AttributeKeys WHERE akHandle = ?",array('features'));
			$result = $db->GetAll("SELECT akID,value FROM atSelectOptions WHERE akID = ?",array($rs[0]['akID']));
	
			foreach($result as $begriff)
			{
				foreach($begriff as $name => $typ)
				{
					if($name == 'akID')  continue;
	
					$typs[] = $typ;
				}
			}
			return $typs;
		} 
		
		
		public function setArray($array)
		{
			$this->dimensionen = $array;
			return $this->dimensionen;
		}
		
		public function setFeatures($array)
		{
			$this->cleanFeatures = $array;
			return $this->cleanFeatures;
		}
		
		
		

		
		public function orderArray()
		{
			foreach($this->dimensionen  as $key => $value)
			{
				foreach($value as $field)
				{
					$this->cleanDimensionen[] = $field;
				}
			}
			return $this->cleanDimensionen; 
		}
	}
?>