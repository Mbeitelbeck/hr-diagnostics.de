<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$c = Page::getByID($_REQUEST['cID'], $_REQUEST['cvID']);

$p = Page::getByID($link);
$pagePath = $detailpage;


$file = File::getByID($fID);
$path = $file->getPath();
$size = getimagesize($path);

$imagePath = File::getRelativePathFromID($fID);
// Testbatterie
if($testtyp == 1)
{
?>

<div class="app-box-batterie">
  <h3><a href="<?php echo $pagePath ?>"><?php echo $title; ?></a></h3>
  <p class="desc"><a href="<?php echo $pagePath ?>"><?php echo $kurzbeschreibung; ?></a></p>
  <div class="info">
    <div class="icon">
      <?php		
		if ($fID != 0 && $fID != "") {  		
			print '<a href="'.$pagePath.'"><img'.$class.' src="'.$imagePath.'" alt="'.$title.'" /></a>';
		}		
		?>
    </div>
    <div class="list"> <?php print $content; ?>
      <div class="footer"></div>
      <?php if ($pagePath != "") { ?>
      <a title="<?php print $title; ?>" href="<?php print $pagePath; ?>" class="info-link">Mehr Information</a>
      <?php }
			?>
      <div class="footer"></div>
      <div class="action">
        <p class="price"><?php print $preis; ?> &euro; <a class="buy" href="/de/authentifizierung"><span>bestellen</span></a></p>
      </div>
      <div class="footer"></div>
    </div>
    <div class="dimension">
      <p style="font-size:11px;"><strong>Dimensionen:</strong></p>
      <a title="" href="">Interessen </a> </div>
    <div class="footer"></div>
  </div>
</div>
<?php
}
// Einzeltest
else
{
?>
<div class="app-box">
  <h3><a href="<?php echo $pagePath ?>"><?php echo $title; ?></a></h3>
  <p class="desc"><a href="<?php echo $pagePath ?>"><?php echo $kurzbeschreibung; ?></a></p>
  <div class="info">
    <div class="icon">
      <?php		
		if ($fID != 0 && $fID != "") {  		
			print '<a href="'.$pagePath.'"><img'.$class.' src="'.$imagePath.'" alt="'.$title.'" /></a>';
		}		
		?>
    </div>
    <div class="list"> <?php print $content; ?>
      <?php if ($pagePath != "") { ?>
      <a title="<?php print $title; ?>" href="<?php print $pagePath; ?>" class="info-link">Mehr Information</a>
      <?php }
			?>
      <div class="action">
        <p class="price"><?php print $preis; ?> &euro; <a class="buy" href="/de/authentifizierung"><span>bestellen</span></a></p>
      </div>
      <div class="footer"></div>
    </div>
    <div class="footer"></div>
  </div>
</div>
<?php
}
?>
