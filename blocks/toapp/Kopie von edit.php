<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true;
$assetLibraryPassThru = array(
	'type' => 'image'
);
$pageselector = Loader::helper('form/page_selector');
$al = Loader::helper('concrete/asset_library');
if ($fID > 0) { $sf = $controller->getFileObject($fID); }
?>



<h2><strong>HRD TEST-App bearbeiten</strong></h2>
<input type="hidden" name="datePublic" value="<?php echo $datePublic; ?>" />
<div class="ccm-block-field-group">
	<h3><?php echo t('&Uuml;berschirft')?></h3>
	<?php echo  $form->text('title', $title, array('style' => 'width: 500px')); ?> </div>
<div class="hr">
	<hr />
</div>
<div id="ccm-block-field-group">
	<h3><?php echo t('Um was handelt es sich')?></h3>
    <?php
	if($testtyp == 0)
	{
		echo 'Einzel Test: '.$form->radio('testtyp', '0', array('style' => 'width: 200px', 'checked' => 'checked'));
		echo 'Testbatterie: '.$form->radio('testtyp', '1', array('style' => 'width: 200px'));
	}
	elseif($testtyp == 1)
	{
		echo 'Einzel Test: '.$form->radio('testtyp', '0', array('style' => 'width: 200px'));
		echo 'Testbatterie: '.$form->radio('testtyp', '1', array('style' => 'width: 200px', 'checked' => 'checked'));
	}
    ?>
</div>
<div class="hr">
	<hr />
</div>
<div id="ccm-block-field-group">
	<h3><?php echo t('Kurzbeschreibung')?></h3>
	<textarea style="color: #444444; font-size:12px; height:60px; width:500px;" name="kurzbeschreibung"><?php echo $kurzbeschreibung ?></textarea>
</div>
<div class="hr">
	<hr />
</div>
<div class="ccm-block-field-group">
	<h3><?php echo t('Test ICON')?></h3>
	<?php echo $al->image('fID', 'fID', t('Choose File'), $sf);?> </div>
<div class="hr">
	<hr />
</div>
<div id="ccm-block-field-group">
	<h3><?php echo t('Listenpunkte')?></h3>
	<textarea id="ccm-content-<?php echo $a->getAreaID()?>1" class="ccm-advanced-editor" name="content"><?php echo $content ?></textarea>
</div>
<div class="hr">
	<hr />
</div>
<!--<div id="ccm-block-field-group">
	<h3><?php /*echo t('Link zur Detailseite')*/?></h3>
	<?php /*print $pageselector->selectPage('link', $link);*/?> </div>
<div class="hr">
	<hr />
</div>-->

<div class="hr">
	<hr />
</div>
<div class="ccm-block-field-group">
	<h3><?php echo t('Preis')?></h3>
	<?php echo  $form->text('preis', $preis, array('style' => 'width: 200px')); ?> Euro </div>

	

<div class="ccm-block-field-group">
	<h3><?php echo t('Bestell-Link')?></h3>
	<?php echo  $form->text('bestellen', $bestellen, array('style' => 'width: 500px')); ?> </div>
