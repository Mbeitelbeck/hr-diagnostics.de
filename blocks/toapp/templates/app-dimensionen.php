﻿<?php
$page = Page::getCurrentPage();

if (strpos($dimensionen, ",") !== FALSE) {
    $dimensionen = explode(',', $dimensionen);
}
else {
    $dimensionen = array($dimensionen);
}
$arrayLen = count($dimensionen);
$d = array('', 'eins', 'zwei', 'drei', 'vier', 'fünf', 'sechs', 'sieben', 'acht', 'neun');
?>

<h2>Ergebnisrückmeldung</h2>
<div style="float:left; margin:10px 0 10px 12px;"> <img width="154" height="107" border="0" src="/files/7213/2466/1384/flip-pdf.jpg" alt=""> </div>
<div style="float:right; width:480px;">
  <p>Die Ergebnisrückmeldung umfasst
    <?php
        if ($arrayLen < 2) {
            echo ' folgende Dimension:';
        }
        else {
            echo ' folgende ' . $d[$arrayLen] . ' Dimensionen:';
        }
        ?>
  </p>
  <?php
    $i = 1;
    foreach ($dimensionen as $array => $key) {
        if ($i % 2 != 0) {
            $left[] = '<li><a title="' . $key . '" href="/de/test-nach-dimensionen-finden/?search=' . $key . '">' . $key . '</a></li>';
        }
        else {
            $right[] = '<li><a title="' . $key . '" href="/de/test-nach-dimensionen-finden/?search=' . $key . '">' . $key . '</a></li>';
        }
        $i++;
    }
    if (is_array($left)) {
        echo '<ul style="float:left;">';
        foreach ($left as $link) {
            echo $link;
        }
        echo '</ul>';
    }
    if (is_array($right)) {
        echo '<ul style="float:left; margin-left: 50px;">';
        foreach ($right as $link) {
            echo $link;
        }
        echo '</ul>';
    }
    ?>
</div>
<div class="footer"></div>
<p> <?php echo $originalContent ?> </p>
<p> Hier können Sie eine exemplarische <a class="bericht action"
        href="/rueckmeldung/<?php echo $page->getCollectionHandle(); ?>/">Ergebnisrückmeldung einsehen</a>.</p>
