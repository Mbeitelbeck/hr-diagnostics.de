<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$c = Page::getByID($_REQUEST['cID'], $_REQUEST['cvID']);

$p = Page::getByID($link);
$pagePath = $p->cPath;

$file = File::getByID($fID);

$image = $file->getVersion();
?>

<div style="width:288px;float:right;" class="module module-2 unframed oneHalf">
	<div style="border-left-width:0px;border-right-width:0px;" class="moduleBodyWrap">
		<div class="moduleBody moduleBodyBleed">
			<div class="csc-default" <?php /*?>id="c34"<?php */?>>
				<div class="csc-header csc-header-n1">
					<h2 class="csc-firstHeader"><?php echo $title; ?></h2>
				</div>
                <?php if ($image->getAttribute('width') != "") {print '<img src="' . $image->getRelativePath() . '" width="' . $image->getAttribute('width') . '" height="' . $image->getAttribute('height') . '" alt="" style="float:left;" />';} ?>
				<?php print $content; ?>
				<?php if ($pagePath != "") { ?> <p><a title="<?php print $title; ?>" href="<?php print $pagePath; ?>" class="arrow-link">Jetzt informieren</a></p><?php } ?>
			</div>
			<div class="clearOnly">&nbsp;</div>
		</div>
	</div>
</div>

