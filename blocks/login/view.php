<?php   defined('C5_EXECUTE') or die(_("Access Denied.")); 

$c = Page::getCurrentPage();
$u = new User();
$loginURL= $this->url('/login', 'do_login' );

if ($u->isRegistered() && $hideFormUponLogin) { ?>
<?php    
	if (Config::get("ENABLE_USER_PROFILES")) {
		$userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
	} else {
		$userName = $u->getUserName();
	}
	?>

<div class="diagonal-box" style="background:url(/themes/hr-d/_images/login-green-box-back.jpg) repeat;">
	<div class="content"><span class="sign-in">
		<?php   echo t('Currently logged in as <b>%s</b>.', $userName)?>
		<a href="<?php   echo $this->url('/login', 'logout')?>">
		<?php   echo t('Sign Out')?>
		</a></span></div>
</div>
<?php    } else { ?>
<div class="diagonal-box"  style="background:url(/themes/hr-d/_images/login-green-box-back.jpg) repeat;">
	<div class="content">
	<p>
<img width="143" height="38" alt="Login" src="/themes/hr-d/_images/shop/heading_login_box.jpg">
</p>
		<form class="login_block_form" method="post" action="<?php    echo $loginURL?>">
			<?php     
			if($returnToSamePage ){ 
				echo $form->hidden('rcID',$c->getCollectionID());
			} ?>
		<?php /*?>	<div class="loginTxt">
				<?php    echo t('Login')?>
			</div><?php */?>
			<div class="uNameWrap">
		<?php /*?>		<label for="uName">
					<?php     if (USER_REGISTRATION_WITH_EMAIL_ADDRESS == true) { ?>
					<?php    echo t('Email Address')?>
					<?php     } else { ?>
					<?php    echo t('Username')?>
					<?php     } ?>
				</label>
				<br/><?php */?>
				<?php   echo $form->text('uName',$uName); ?>
			</div>
			<div class="passwordWrap">
			<?php /*?>	<label for="uPassword">
					<?php    echo t('Password')?>
				</label>
				<br/><?php */?>
				<?php   echo $form->password('uPassword'); ?>
			</div>
			<div class="loginButton">
				<?php    echo $form->submit('submit', '')?>
			</div>
			<div class="footer">&nbsp;</div>

			<p class="regtext"><span>Noch nicht akkreditiert?</span>
Dann melden Sie sich einmalig und kostenfrei an, um dauerhaft Zugriff auf unser breit angelegtes Verfahrens-
portfolio zu haben.</p>

<p>
<a class="action" href="/de/registrieren">Jetzt registrieren</a>

</p>
<div class="footer">&nbsp;</div>
			<?php     if($showRegisterLink && ENABLE_REGISTRATION){ ?>
			<div class="login_block_register_link"><a href="<?php    echo View::url('/register')?>">
				<?php    echo $registerText?>
				</a></div>
			<?php     } ?>
		</form>
	</div>
</div>
<?php   } // end if not logged in  ?>
