﻿<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true;
$assetLibraryPassThru = array(
	'type' => 'image'
);
$al = Loader::helper('concrete/asset_library');
?>
<h2><strong>Inhalt</strong></h2>
<div id="ccm-block-field-group">
<textarea id="ccm-content-<?php echo $a->getAreaID()?>" class="ccm-advanced-editor" name="content"><?php echo $content; ?></textarea>
</div>

<h2><strong>Individuell</strong></h2>
<div class="ccm-block-field-group">
<h3><?php echo t('ID')?></h3>
<?php echo  $form->text('id', $id, array('style' => 'width: 100px')); ?>
</div>
<div class="ccm-block-field-group">
<h3><?php echo t('Class')?></h3>
<?php echo  $form->text('class', $class, array('style' => 'width: 100px')); ?>
</div>
<div class="ccm-block-field-group">
<h3><?php echo t('Style')?></h3>
<?php echo  $form->text('style', $style, array('style' => 'width: 250px')); ?>
</div>