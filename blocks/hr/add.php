<p>F&uuml;gt eine horizontale Linie hinzu.<br />
	&Uuml;ber die Auswahl einer Benutzervorlage k&ouml;nnen Ecken um den Inhaltsbereich gelegt werden.
</p>
<p>
	- Standard: horizontale Linie<br />
	- Benutzer-Vorlage: Ecken oben<br />
	- Benutzer-Vorlage: Ecken unten
</p>