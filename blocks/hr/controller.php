<?php 
	defined('C5_EXECUTE') or die(_("Access Denied."));
	require_once(DIR_FILES_BLOCK_TYPES_CORE . '/library_file/controller.php');
	
	class HrBlockController extends BlockController {

		protected $btInterfaceWidth = 350;
		protected $btInterfaceHeight = 300;
		protected $btTable = 'btHr';

		/** 
		 * Used for localization. If we want to localize the name/description we have to include this
		 */
		public function getBlockTypeDescription() {
			return t("F&uuml;gt Linien oder Ecken um den Inhaltsbereich hinzu.");
		}
		
		public function getBlockTypeName() {
			return t("Linien oder Ecken");
		}

		public function getJavaScriptStrings() {
			return array('file-required' => t('You must select a file.'));	
		}		
		
	}