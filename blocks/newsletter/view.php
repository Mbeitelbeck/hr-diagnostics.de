<?php
$c = Page::getCurrentPage();

Loader::model('page_list');
$options = array('Unternehmens Neuigkeiten' => '/themes/hr-d/_images/newsletter/company_news.png',
    'Psychologische Neuigkeiten' => '/themes/hr-d/_images/newsletter/psy_news.png',
    'Jobmatcher Neuigkeiten' => '/themes/hr-d/_images/newsletter/job_news.png',
    'Im Fokus' => '/themes/hr-d/_images/newsletter/im_fokus.png',
    'Für Sie gelesen' => '/themes/hr-d/_images/newsletter/gelesen.png',
    'Rechtliches' => '/themes/hr-d/_images/newsletter/rechtliches.png');

$core = Page::getByID($pageselect_1);
$pl = new PageList();
$pl->filterByParentID($pageselect_1);
$list = array();

foreach ($options as $key => $value) {
    $pl = new PageList();
    $pl->filterByParentID($pageselect_1);
    $pages = $pl->get($pl->getTotal());
    $tmp = array();
    $tmp['pages'] = array();
    foreach ($pages as $page) {
        $tmp['img'] = $value;
        if ($page->getAttribute('newsletter_cat') == $key) {
            ;
            $tmp['pages'][] = $page;
        }
    }
    if (count($tmp['pages']) > 0) {
        $list[$key] = $tmp;
    }
}
?>

<div class="diagonal-box" id="letterNav">
    <div class="content">
        <div class="letterHead">
           <h4 class="letterDate"> <?php
            echo $core->getCollectionName(); // BILD HEADER
            ?></h4>
            <div style="clear:both;"></div>
            <img class="hrLetter" src="/themes/hr-d/_images/newsletter/letter_heading.png">
        </div>
        <?php
        foreach ($list as $obj) {
            $pages = $obj['pages'];
            $img = $obj['img'];
            echo '<img src="' . $img . '" />'; // BILD
            echo '<ul>';
            foreach ($pages as $p) {
                $class = '';
                $name = $p->getCollectionName();
                $path = $p->getCollectionPath();
                if ($p->getCollectionID() == $c->getCollectionID()) $class = 'active';
                echo '<li  class="' . $class . '"><a href="' . $path . '">» ' . $name . '</a><hr></li>';
            }
            echo '</ul>';
        }
        ?>
    </div>
</div>
