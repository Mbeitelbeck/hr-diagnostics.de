<?php
class NewsletterBlockController extends BlockController {
    protected $btTable = 'btNewsletter';
    protected $btDescription = 'Newsletter Block';
    protected $btName = 'Newsletter Rubrik';
    protected $btHandle = 'newsletter';
    protected $displayBuffer = array();
    protected $btInterfaceWidth = "550";
    protected $btInterfaceHeight = "500";
    protected $btWrapperClass = 'ccm-ui';
    protected $btVersion = 1.2;

    public function getBlockTypeDescription() {
        return t($this->btDescription);
    }

    public function getBlockTypeName() {
        return t($this->btName);
    }

    public function on_start() {
        $cacheID = $this->btHandle . 'XML';
        $records = Cache::get($cacheID, false);
        Cache::flush();
        if(!$records) {
            $this->readXML();
            Cache::set($cacheID, false, $this->displayBuffer);
        } else {
            $this->displayBuffer = $records;
        }
    }

    public function save($args) {
        if($args['option_1'] == "") $args['option_1'] = 0;
        if($args['option_2'] == "") $args['option_2'] = 0;
        if($args['option_3'] == "") $args['option_3'] = 0;
        if($args['option_4'] == "") $args['option_4'] = 0;

        parent::save($args);
    }





    private function readXML() {
        $path =  DIR_BASE . '/blocks/' . $this->btHandle . '/display.xml';
        $xml = simplexml_load_file($path);
        $elements = $xml->element;
        foreach($elements as $element) {
            $xmlAttributs = $element->attributes();
            $data = array();
            foreach($xmlAttributs as $key => $value) {
                $data[$key] = (string) $value;
            }
            switch($data['type']) {
                case 'html':
                    $data['display'] = (string) $element->content;
                    break;
                case 'select':
                    $options = array('' => '----');
                    foreach($element->option as $option) {
                        foreach($option->attributes() as $key => $value) {
                            $wellFormated[$key] = (string) $value;
                        }
                        $options[$wellFormated['value']] = $wellFormated['display'];
                    }
                    $data['options'] = $options;
                    break;
                default:
            }

            $this->displayBuffer[] = $data;

        }

        /*
        $json     = json_encode( $xml );
        $displayXML  = json_decode( $json, TRUE );

        foreach($displayXML['element'] as $element) {
            $data = $element['@attributes'];
            if($data['type']) {

            }else if(is_array($element['option'])) {
                $options = $element['option'];
                $selectArray = array('' => '---');
                foreach($options as $option) {
                    $value = $option['@attributes']['value'];
                    $display = $option['@attributes']['display'];
                    $selectArray[$value] = $display;
                }
                $data['options'] = $selectArray;
            }


            $this->displayBuffer[] = $data;

        }
        */
    }

    public function display($data) {
        /**
         * HELPERS
         */
        $form = Loader::helper('form');
        $pageSelector = Loader::helper('form/page_selector');
        $al = Loader::helper('concrete/asset_library');
        $key = $data['key'];
        $value = $this->get($key);
        $type = $data['type'];

        $description = $data['display'];

        switch($type) {
            case 'text':
                $string = '<div class="clearfix"><div style="margin-bottom: 10px;">' . $description . '</div>' . $form->text($key, $value). '</div>';
                break;
            case 'textarea':
                $string = '<div class="clearfix"><div style="margin-bottom: 10px;">' . $description . '</div>' . $form->textarea($key, $value, array('style' => 'width: 60%; height:115px;')). '</div>';
                break;
            case 'file':
                $string = '<div class="clearfix"><div style="margin-bottom: 10px;">' . $description . '</div>' . $al->file($key, $key, t('Choose File'), File::getByID($value)). '</div>';
                break;
            case 'option':
                $string = '<div class="clearfix"><div style="margin-bottom: 10px;">' . $description . '</div>' . $form->checkbox($key, ($value == 1) ? '' : 1, ($value == 1) ? true : false). '</div>';
                break;
            case 'pageselect':
                $string = '<div class="clearfix"><div style="margin-bottom: 10px;">' . $description . '</div>' . $pageSelector->selectPage($key, $value, 'ccm_selectSitemapNode'). '</div>';
                break;
            case 'extern':
                $string = '<div class="clearfix"><div style="margin-bottom: 10px;">' . $description . '</div>' . $form->text($key, $value) . '</div>';
                break;
            case 'hint':
                $string = '<p>' . $description . '</p>';
                break;
            case 'select':
                $string = '<div class="clearfix"><div style="margin-bottom: 10px;">' . $description . '</div>' . $form->select($key, $data['options'], $value) . '</div>';
                break;
            case 'html':
                $string = '<div class="clearfix">' . $description . '</div>';
                break;
            default:
                $string = "";
                break;
        }

        echo $string;

    }

    public function renderXML() {
        foreach($this->displayBuffer as $e) {
            $this->display($e);
        }
    }

    private function getType($string) {
        $frags = explode('_', $string);
        return $frags[0];
    }
}