<?php
	Loader::block('library_file');
	defined('C5_EXECUTE') or die(_("Access Denied."));	
	global $initHRtest;
	$initHRtest=true;
	class HrimagespopupBlockController extends BlockController {

		protected $btInterfaceWidth = "850";
		protected $btInterfaceHeight = "600";
		protected $btTable = 'btHRimagespopup';

		/** 
		 * Used for localization. If we want to localize the name/description we have to include this
		 */
		public function getBlockTypeDescription() {
			return t("HRimagespopup.");
		}
		
		public function getBlockTypeName() {
			return t("HRimagespopup");
		}		
		
		/*public function getJavaScriptStrings() {
			return array(
				'image-required' => t('You must select an image.')
			);
		}*/
		
		function getID() {return $this->bID;}
		/*function getFileID() {return $this->fID;}
		function getKuerzel() {return $this->kuerzel;}
		function getName() {return $this->name;}
		function getDescription() {return $this->description;}
		function getDefinition() {return $this->definition;}
		function getVerfahren() {return $this->verfahren;}
		function getAnwendungsbereich() {return $this->anwendungsbereich;}
		function getLangversion() {return $this->langversion;}
		function getKurzversion() {return $this->kurzversion;}
		function isPersonalauswahl() {return $this->isPersonalauswahl;}
		function isPersonalentwicklung() {return $this->isPersonalentwicklung;}
		function getUngBearb() {return $this->ungBearb;}
		function getBesonderheit() {return $this->besonderheit;}
		function getGuetekriterien() {return $this->guetekriterien;}
		
		public function save($args) {		
			$args['fOnstateID'] = ($args['fOnstateID'] != '') ? $args['fOnstateID'] : 0;
			$args['fID'] = ($args['fID'] != '') ? $args['fID'] : 0;
			$args['kuerzel'] = ($args['kuerzel'] != '') ? $args['kuerzel'] : 0;
			$args['name'] = ($args['name'] != '') ? $args['name'] : "";
			$args['kurzbeschreibung'] = ($args['kurzbeschreibung'] != '') ? $args['kurzbeschreibung'] : "";
			$args['beschreibung'] = ($args['beschreibung'] != '') ? $args['beschreibung'] : "";
			
			$args['langItems'] = ($args['langItems'] != '') ? $args['langItems'] : 0;
			$args['langDimensionen'] = ($args['langDimensionen'] != '') ? $args['langDimensionen'] : 0;
			$args['langDauer'] = ($args['langDauer'] != '') ? $args['langDauer'] : 0;
			$args['langIsUngBearb'] = ($args['langIsUngBearb'] != '') ? $args['langIsUngBearb'] : 0;
			
			$args['kurzItems'] = ($args['kurzItems'] != '') ? $args['kurzItems'] : 0;
			$args['kurzDimensionen'] = ($args['kurzDimensionen'] != '') ? $args['kurzDimensionen'] : 0;
			$args['kurzDauer'] = ($args['kurzDauer'] != '') ? $args['kurzDauer'] : 0;
			$args['kurzIsUngBearb'] = ($args['kurzIsUngBearb'] != '') ? $args['kurzIsUngBearb'] : 0;
			
			$args['isPersonalauswahl'] = ($args['isPersonalauswahl'] != '') ? $args['isPersonalauswahl'] : 0;
			$args['isPersonalentwicklung'] = ($args['isPersonalentwicklung'] != '') ? $args['isPersonalentwicklung'] : 0;
			
			$args['besonderheit'] = ($args['besonderheit'] != '') ? $args['besonderheit'] : "";
			$args['guetekriterien'] = ($args['guetekriterien'] != '') ? $args['guetekriterien'] : "";
			$args['bedEmpfohlen'] = ($args['bedEmpfohlen'] != '') ? $args['bedEmpfohlen'] : "";
			$args['jewEntw'] = ($args['jewEntw'] != '') ? $args['jewEntw'] : "";
			$args['empfohlen'] = ($args['empfohlen'] != '') ? $args['empfohlen'] : "";
			parent::save($args);
		}*/
		
		function getFileObject($id) {
			return File::getByID($id);
		}
		
		function getImages() {
			$iID1 = $this->iID1;
			$iID2 = $this->iID2;
			$iID3 = $this->iID3;
			$iID4 = $this->iID4;
			$iID5 = $this->iID5;
			
			$anz = 0;
			if ($iID1 > 0) $anz++;
			if ($iID2 > 0) $anz++;
			if ($iID3 > 0) $anz++;
			if ($iID4 > 0) $anz++;
			if ($iID5 > 0) $anz++;
			
			$html = "";
			$counter = 0;
			if ($iID1 > 0) {
				$counter++;
				$image = $this->getImagePath($iID1);
				$html = "<a href='".$image."' class='fancy img1' title='[".$counter."/".$anz."]' rel='g".$this->bID."'><img src='".$image."' alt='' /></a>";
			}
			if ($iID2 > 0) {
				$counter++;
				$image = $this->getImagePath($iID2);
				//if($html!=""){ $hidden = " hidden";}else{$hidden = "";};
				$html = $html."<a href='".$image."' class='fancy img2' title='[".$counter."/".$anz."]' rel='g".$this->bID."'><img src='".$image."' alt='' /></a>";
			}
			if ($iID3 > 0) {
				$counter++;
				$image = $this->getImagePath($iID3);
				//if($html!=""){ $hidden = " hidden";}else{$hidden = "";};
				$html = $html."<a href='".$image."' class='fancy img3' title='[".$counter."/".$anz."]' rel='g".$this->bID."'><img src='".$image."' alt='' /></a>";
			}
			if ($iID4 > 0) {
				$counter++;
				$image = $this->getImagePath($iID4);
				if($html!=""){ $hidden = " hidden";}else{$hidden = "";};
				$html = $html."<a href='".$image."' class='fancy img4".$hidden."' title='[".$counter."/".$anz."]' rel='g".$this->bID."'><img src='".$image."' alt='' /></a>";
			}
			if ($iID5 > 0) {
				$counter++;
				$image = $this->getImagePath($iID5);
				if($html!=""){ $hidden = " hidden";}else{$hidden = "";};
				$html = $html."<a href='".$image."' class='fancy img5".$hidden."' title='[".$counter."/".$anz."]' rel='g".$this->bID."'><img src='".$image."' alt='' /></a>";
			}
			
			/*if ($counter > 1) {
				$html = $html."<br /><span>Bild 1 von ".$counter."</span>";
			}*/
			return $html;
		}
		
		function getImagePath($id) {
			$f = $this->getFileObject($id);
			$relPath = $f->getRelativePath();
			return $relPath;
		}		
	}

?>