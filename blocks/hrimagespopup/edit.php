﻿<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$includeAssetLibrary = true;
$assetLibraryPassThru = array(
	'type' => 'image'
);
$al = Loader::helper('concrete/asset_library');
?>
<h2><strong>Bilder</strong></h2>
<div class="ccm-block-field-group">
<?php
	$iO1 = null;
	$iO2 = null;
	$iO3 = null;
	$iO4 = null;
	$iO5 = null;
	if ($iID1 > 0) { $iO1 = $controller->getFileObject($iID1); }
	if ($iID2 > 0) { $iO2 = $controller->getFileObject($iID2); }
	if ($iID3 > 0) { $iO3 = $controller->getFileObject($iID3); }
	if ($iID4 > 0) { $iO4 = $controller->getFileObject($iID4); }
	if ($iID5 > 0) { $iO5 = $controller->getFileObject($iID5); }	
	echo $al->image('image1', 'iID1', t('Choose Image'), $iO1);
	echo $al->image('image2', 'iID2', t('Choose Image'), $iO2);
	echo $al->image('image3', 'iID3', t('Choose Image'), $iO3);
	echo $al->image('image4', 'iID4', t('Choose Image'), $iO4);
	echo $al->image('image5', 'iID5', t('Choose Image'), $iO5);
?>
</div>