ccmValidateBlockForm = function() {
	if ($("#ccm-b-image-value").val() == '' || $("#ccm-b-image-value").val() == 0) { 
		ccm_addError(ccm_t('image-required'));
	}
	return false;
}

$(function() {
	$("#ccm-autonav-tabs a").click(function(){
		$("#ccm-autonav-tabs .ccm-nav-active").removeClass("ccm-nav-active");
		$(this).parent().addClass("ccm-nav-active");
		$(".ccm-autonavPane:visible").hide();
		$("#ccm-autonavPane-"+$(this).attr("rel")).show();
	});
});