<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true;
$assetLibraryPassThru = array(
	'type' => 'image'
);
$pageselector = Loader::helper('form/page_selector');
$al = Loader::helper('concrete/asset_library');

$kurzbeschreibung = 'Diefundierte Grundlage für einen erfolgreichen Einstieg ins Berufsleben';
$title = 'BERUFSPROFILING junior: <br/>Berufsorientierung für Schüler';
$preis = '7,50€';
$sf = '540';
$bestellen = '#';
$samplecontent = "<ul><li>Rückmeldung: 18 Dimensionen</li>";
$samplecontent .= "<li>Aufgabentypen:5</li>";
$samplecontent .= "<li>Durchführungsdauer: 35 Minuten </li></ul>";

$dimensionen['performance'] = $controller->getDimensionen('performance');
$dimensionen['personality'] = $controller->getDimensionen('personality');
$dimensionen['behavior'] = $controller->getDimensionen('behavior');
$dimensionen['interests'] = $controller->getDimensionen('interests');

$features = $controller->getFeatures();
?>

<div style="padding: 10px;">   
	<!-- &uuml;berschirft/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('&Uuml;berschrift')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
                <?php echo $form->text('title', $title, array('style' => 'width: 100%;
																		  border: 1px solid #CCCCCC;
																		  border-radius: 3px 3px 3px 3px;
																		  color: #808080;')); ?>
            </div>
        </div>  
    </div>
    <!-- &uuml;berschirft/Ende -->
    <!-- Test-Typ/Start -->   
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Testtyp')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
                <?php
						echo '<div style="float: left;">';
						echo $form->radio('testtyp', '1');
						echo 'Einzel Test';
						echo '</div>';
						
						echo '<div style="float: left; margin-left: 40px;">';
						echo $form->radio('testtyp', '1', array('style' => 'margin-left: 20px;'));
						echo 'Testbatterie';
						echo '</div>';
				?>
                <br style="clear: both";/>
            </div>
        </div>  
    </div> 
    <!-- Test-Typ/Ende -->       
    <!-- Kurzbeschreibung/Start -->    
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Kurzbeschreibung')?></h2>   
    	<div class="ccm-block-field-group">
    		<div style="margin-top: 10px;">
               <textarea style="border: 1px solid #CCCCCC;border-radius: 3px 3px 3px 3px;color: #808080; font-size:12px; height:60px; width:100%;" name="kurzbeschreibung"><?php echo $kurzbeschreibung ?></textarea>
        	</div>  
        </div> 
    </div>
    <!-- Kurzbeschreibung/Ende --> 
    <!-- Test-Icon/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Test-Icon')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
                <?php echo $al->image('fID', 'fID', t('Choose File'), $sf);?>
            </div>
        </div>  
    </div>
    <!-- Test-Icon/Ende -->
    <!-- Listepunkte/Start -->
  	<div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Listenpunkte')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<textarea id="ccm-content-<?php echo $a->getAreaID()?>1" class="ccm-advanced-editor" name="content"><?php echo $samplecontent ?></textarea>
        	</div>
        </div>  
    </div>
    <!-- Listepunkte/Ende -->
    <!-- Preis/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Preis')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<?php echo $form->text('preis', $preis, array('style' => 'width: 100%;
																		  border: 1px solid #CCCCCC;
																		  border-radius: 3px 3px 3px 3px;
																		  color: #808080;')); ?>
        	</div>
        </div>  
    </div>
	<!-- Preis/Ende -->
    <!-- Bestell-Link/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Bestell-Link')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<?php echo $form->text('bestellen', $bestellen, array('style' => 'width: 100%;
																	  			  border: 1px solid #CCCCCC;
																				  border-radius: 3px 3px 3px 3px;
																				  color: #808080;')); ?>
        	</div>
        </div>  
    </div>
    <!-- Bestell-Link/Ende -->
    <!-- Seiten-Link/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Link zur Detailseite')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<?php echo $pageselector->selectPage('link', $link, 'ccm_selectSitemapNode', array('style' => 'width: 100%;
																	  			  border: 1px solid #CCCCCC;
																				  border-radius: 3px 3px 3px 3px;
																				  color: #808080;')); ?>
        	</div>
        </div>  
    </div>
    <!-- Seiten-Link/Ende -->
    <!-- Dimensionen/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
        <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Dimensionen')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<div style="float: left; min-width: 330px;">
                	<ul>
                    	<li><h3>Performance</h3></li>
                        <?php
						foreach($dimensionen['performance'] as $key => $value)
						{
							echo '<li>'. $form->checkbox($value, 1, 0).$value.'</li>';
						}
						?>
                    </ul>    
                </div>
                <div style="float: left; margin-left: 20px;">
               	 	<ul>
                    	<li><h3>Personality</h3></li>
                    	<?php
						foreach($dimensionen['personality'] as $key => $value)
						{
							echo '<li>'. $form->checkbox($value, 1, 0).$value.'</li>';
						}
						?>
                    </ul>
                </div>
                <br style="clear: both" />
       		</div>
            <div style="margin-top: 10px;">
				<div style="float: left; min-width: 330px;">
                	<ul>
                    	<li><h3>Behavior</h3></li>
                    	<?php
						foreach($dimensionen['behavior'] as $key => $value)
						{
							echo '<li>'. $form->checkbox($value, 1, 0).$value.'</li>';
						}
						?>
                    </ul>
                </div>
              	<div style="float: left; margin-left: 20px;">
               	 	<ul>
                    	<li><h3>Interests</h3></li>
                    	<?php
						foreach($dimensionen['interests'] as $key => $value)
						{
							echo '<li>'. $form->checkbox($value, 1, 0).$value.'</li>';
						}
						?>
                    </ul>
                </div>
                <br style="clear: both" />
       		</div>   
   		</div>
    </div>
    <!-- Dimensionen/Ende -->
    <!-- Features/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
        <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Features')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<div style="float: left; min-width: 330px;">
                <ul>
                	<?php
					foreach($features as $key => $value)
					{
						echo '<li>'. $form->checkbox($value, 1, 0).$value.'</li>';
					}
					?> 
                </ul>       
                </div>
             </div>
             <br style="clear: both" />  
   		</div>
    </div>
    <!-- Features/Ende --> 
      
</div>