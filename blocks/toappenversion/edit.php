<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true;
$assetLibraryPassThru = array(
	'type' => 'image'
);
$pageselector = Loader::helper('form/page_selector');
$al = Loader::helper('concrete/asset_library');
if ($fID > 0) { $sf = $controller->getFileObject($fID); }

$dimensionenSet['performance'] = $controller->getDimensionen('performance');
$dimensionenSet['personality'] = $controller->getDimensionen('personality');
$dimensionenSet['behavior'] = $controller->getDimensionen('behavior');
$dimensionenSet['interests'] = $controller->getDimensionen('interests');

$controller->setArray($dimensionenSet);
$cleanDimensionen = $controller->orderArray();
$savedDimensionen = explode(',', $dimensionen);



$featureSet = $controller->getFeatures();

$controller->setFeatures($featureSet);
$savedFeatures = explode(',', $features);

?>
<div style="padding: 10px;">   
	<!-- &uuml;berschirft/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('&Uuml;berschrift')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
                <?php echo $form->text('title', $title, array('style' => 'width: 100%;
																		  border: 1px solid #CCCCCC;
																		  border-radius: 3px 3px 3px 3px;
																		  color: #808080;')); ?>
            </div>
        </div>  
    </div>
    <!-- &uuml;berschirft/Ende -->
    <!-- Test-Typ/Start -->   
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Testtyp')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
                <?php
                	if($testtyp == 0)
					{
						echo '<div style="float: left;">';
						echo $form->radio('testtyp', '0', array('checked' => 'checked'));
						echo 'Einzel Test';
						echo '</div>';
						
						echo '<div style="float: left; margin-left: 40px;">';
						echo $form->radio('testtyp', '1', array('style' => 'margin-left: 20px;'));
						echo 'Testbatterie';
						echo '</div>';
					}
					elseif($testtyp == 1)
					{
						echo '<div style="float: left;">';
						echo $form->radio('testtyp', '0');
						echo 'Einzel Test';
						echo '</div>';
						
						echo '<div style="float: left; margin-left: 40px;">';
						echo $form->radio('testtyp', '1', array('style' => 'margin-left: 20px;', 'checked' => 'checked'));
						echo 'Testbatterie';
						echo '</div>';
					}
				?>
                <br style="clear: both";/>
            </div>
        </div>  
    </div> 
    <!-- Test-Typ/Ende -->       
    <!-- Kurzbeschreibung/Start -->    
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Kurzbeschreibung')?></h2>   
    	<div class="ccm-block-field-group">
    		<div style="margin-top: 10px;">
               <textarea style="border: 1px solid #CCCCCC;border-radius: 3px 3px 3px 3px;color: #808080; font-size:12px; height:60px; width:100%;" name="kurzbeschreibung"><?php echo $kurzbeschreibung ?></textarea>
        	</div>  
        </div> 
    </div>
    <!-- Kurzbeschreibung/Ende --> 
    <!-- Test-Icon/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Test-Icon')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
                <?php echo $al->image('fID', 'fID', t('Choose File'), $sf);?>
            </div>
        </div>  
    </div>
    <!-- Test-Icon/Ende -->
    <!-- Listepunkte/Start -->
  	<div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Listenpunkte')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<textarea id="ccm-content-<?php echo $a->getAreaID()?>1" class="ccm-advanced-editor" name="content"><?php echo $content ?></textarea>
        	</div>
        </div>  
    </div>
    <!-- Listepunkte/Ende -->
    <!-- Preis/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Preis')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<?php echo $form->text('preis', $preis, array('style' => 'width: 100%;
																		  border: 1px solid #CCCCCC;
																		  border-radius: 3px 3px 3px 3px;
																		  color: #808080;')); ?>
        	</div>
        </div>  
    </div>
	<!-- Preis/Ende -->
    <!-- Bestell-Link/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Bestell-Link')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<?php echo $form->text('bestellen', $bestellen, array('style' => 'width: 100%;
																	  			  border: 1px solid #CCCCCC;
																				  border-radius: 3px 3px 3px 3px;
																				  color: #808080;')); ?>
        	</div>
        </div>  
    </div>
    <!-- Bestell-Link/Ende -->
    <!-- Seiten-Link/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
    <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Link zur Detailseite')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<?php echo $pageselector->selectPage('link', $link, 'ccm_selectSitemapNode', array('style' => 'width: 100%;
																	  			  border: 1px solid #CCCCCC;
																				  border-radius: 3px 3px 3px 3px;
																				  color: #808080;')); ?>
        	</div>
        </div>  
    </div>
    <!-- Seiten-Link/Ende -->
    <!-- Dimensionen/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
        <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Dimensionen')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<div style="float: left; min-width: 330px;">
                	<ul>
                    	<li><h3>Performance</h3></li>
                        <?php
						$i = 0;
						foreach($dimensionenSet['performance'] as $key => $value)
						{
							if(in_array($value, $savedDimensionen))
							{
								echo '<li>'. $form->checkbox('dimensionen['.$i.']', $value, 1).$value.'</li>';
							}
							else
							{
								echo '<li>'. $form->checkbox('dimensionen['.$i.']', $value, 0).$value.'</li>';
							}
							$i++;
						}
						?>
                    </ul>    
                </div>
                <div style="float: left; margin-left: 20px;">
               	 	<ul>
                    	<li><h3>Personality</h3></li>
                    	<?php
						foreach($dimensionenSet['personality'] as $key => $value)
						{
							if(in_array($value, $savedDimensionen))
							{
								echo '<li>'. $form->checkbox('dimensionen['.$i.']', $value, 1).$value.'</li>';
							}
							else
							{
								echo '<li>'. $form->checkbox('dimensionen['.$i.']', $value, 0).$value.'</li>';
							}
							$i++;
						}
						?>
                    </ul>
                </div>
                <br style="clear: both" />
       		</div>
            <div style="margin-top: 10px;">
				<div style="float: left; min-width: 330px;">
                	<ul>
                    	<li><h3>Behavior</h3></li>
                    	<?php
						foreach($dimensionenSet['behavior'] as $key => $value)
						{
							if(in_array($value, $savedDimensionen))
							{
								echo '<li>'. $form->checkbox('dimensionen['.$i.']', $value, 1).$value.'</li>';
							}
							else
							{
								echo '<li>'. $form->checkbox('dimensionen['.$i.']', $value, 0).$value.'</li>';
							}
							$i++;
						}
						?>
                    </ul>
                </div>
              	<div style="float: left; margin-left: 20px;">
               	 	<ul>
                    	<li><h3>Interests</h3></li>
                    	<?php
						foreach($dimensionenSet['interests'] as $key => $value)
						{
							if(in_array($value, $savedDimensionen))
							{
								echo '<li>'. $form->checkbox('dimensionen['.$i.']', $value, 1).$value.'</li>';
							}
							else
							{
								echo '<li>'. $form->checkbox('dimensionen['.$i.']', $value, 0).$value.'</li>';
							}
							$i++;
						}
						?>
                    </ul>
                </div>
                <br style="clear: both" />
       		</div>   
   		</div>
    </div>
    <!-- Dimensionen/Ende -->
    <!-- Features/Start -->
    <div style="width: 100%; margin-top: 10px; padding-bottom: 5px;">
        <h2 style="font-weight: bold; font-size: 18px;"><?php echo t('Features')?></h2>   
        <div class="ccm-block-field-group">
        	<div style="margin-top: 10px;">
				<div style="float: left; min-width: 330px;">
                	<ul>
                    	<?php
						foreach($featureSet as $key => $value)
						{
							if(in_array($value, $savedFeatures))
							{
								echo '<li>'. $form->checkbox('features['.$i.']', $value, 1).$value.'</li>';
							}
							else
							{
								echo '<li>'. $form->checkbox('features['.$i.']', $value, 0).$value.'</li>';
							}
							$i++;
						}
						?>
                    </ul>    
                </div>
             </div>
             <br style="clear: both" />  
   		</div>
    </div>
    <!-- Features/Ende --> 
</div>










