<?php
$page = Page::getCurrentPage();

if(strpos($dimensionen,",")!==false)
{
	$dimensionen = explode(',', $dimensionen);
}
else
{
	$dimensionen = array($dimensionen);
}
$arrayLen = count($dimensionen);
$d = array('','one','two','three', 'four', 'five', 'six','seven', 'eight', 'nine');
?>
<h2>Results report</h2>
<div style="float:left; margin:10px 0 10px 12px;">
	<img width="154" height="107" border="0" src="/files/7213/2466/1384/flip-pdf.jpg" alt="">
</div>
<div style="float:right; width:480px;">
	<p>The results report includes 
	<?php 
		if($arrayLen < 2)
		{
			echo ' following dimensions:';
		}
		else
		{
			echo ' following ' . $d[$arrayLen] . ' dimensions:';
		}
	?>
    </p>
    <ul>
    <?php
	$i=1;
	foreach($dimensionen as $array => $key)
	{
		if($i%2 != 0)
		{
			$left[] = '<li>'.$key.'</li>';
		}
		else
		{
			$right[] = '<li>'.$key.'</li>';
		}
		$i++;
	}
	if(is_array($left))
	{
		echo '<ul style="float:left;">';
		foreach($left as $link)
		{
			echo $link;
		}
		echo '</ul>';
	}
	if(is_array($right))
	{
		echo '<ul style="float:left; margin-left: 50px;">';
		foreach($right as $link)
		{
			echo $link;
		}
		echo '</ul>';
	}
	
	?>
    </ul>

</div>
<div class="footer"></div>
<p>
The report will be a PDF document that can either be downloaded or emailed.</p>
<p><a class="bericht action" href="/rueckmeldung_en/<?php echo $page->getCollectionHandle(); ?>/">View a sample report</a></p>