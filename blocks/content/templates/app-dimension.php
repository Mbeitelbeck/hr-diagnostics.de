<?php
$c = Page::getCurrentPage();

$app = BlockType::getByHandle('toapp');

$originalContent = $controller->getContent();
$app->controller->set('originalContent', $originalContent);

foreach($c->getBlocks('Inhalt') as $contentBlock)
{
	if ($contentBlock->getBlockTypeID() == 30)
	{
		$blockController = $contentBlock->getInstance();
		
		$app->controller->set('title', $blockController->title);
		$app->controller->set('preis', $blockController->preis);
		$app->controller->set('kurzbeschreibung', $blockController->kurzbeschreibung);
		$app->controller->set('content', $blockController->content);
		$app->controller->set('fID', $blockController->fID);
		$app->controller->set('bestellen', $blockController->bestellen);
		$app->controller->set('dimensionen', $blockController->dimensionen);

		$app->render('templates/app-dimensionen');
	}
}
