<?php
$c = Page::getCurrentPage();

foreach($c->getBlocks('Inhalt') as $b)
{
	if ($b->getBlockTypeID() == 44)
	{
		$controller = $b->getInstance();
		
		$app = BlockType::getByHandle('toappenversion');
		$app->controller->set('title', $controller->title);
		$app->controller->set('preis', $controller->preis);
		$app->controller->set('kurzbeschreibung', $controller->kurzbeschreibung);
		$app->controller->set('content', $controller->content);
		$app->controller->set('fID', $controller->fID);
		$app->controller->set('bestellen', $controller->bestellen);
		$app->controller->set('dimensionen', $controller->dimensionen);
		$app->render('templates/app-dimensionen-en');
	}
}
?>
