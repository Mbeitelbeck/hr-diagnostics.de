<style type="text/css">
#ccm-iframe-block-table acronym { border-bottom-width:1px; border-bottom-style:dashed; border-bottom-color:#00A000; cursor: help; }
</style>
<table id="ccm-iframe-block-table">
	<tr>
		<th><acronym title="<?php echo t('The website  the iframe should display')?>"><?php echo t('URL')?></acronym></th>
		<td><input type="text" name="url" value="<?php echo $formController->url; ?>"></td>
	</tr>
	<tr>
		<th><?php echo t('Name')?></th>
		<td><input type="text" name="name" value="<?php echo $formController->name; ?>"></td>
	</tr>
	<tr>
		<th><?php echo t('Width')?></th>
		<td><input type="text" name="width" value="<?php echo $formController->width; ?>"></td>
	</tr>
	<tr>
		<th><?php echo t('Height')?></th>
		<td><input type="text" name="height" value="<?php echo $formController->height; ?>"></td>
	</tr>
	<tr>
		<th><?php echo t('Frame Border')?></th>
		<td>
			<select name="frameborder">
				<option value="1"<?php echo $formController->frameborder=='1'?' selected="selected"':''; ?>><?php echo t('Yes')?></option>
				<option value="0"<?php echo $formController->frameborder=='0'?' selected="selected"':''; ?>><?php echo t('No')?></option>
			</select>
		</td>
	</tr>
	<tr>
		<th><?php echo t('Align')?></th>
		<td>
			<select name="align">
				<option value="left"<?php echo $formController->align=='left'?' selected="selected"':''; ?>><?php echo t('Left')?></option>
				<option value="center"<?php echo $formController->align=='center'?' selected="selected"':''; ?>><?php echo t('Center')?></option>
				<option value="right"<?php echo $formController->align=='right'?' selected="selected"':''; ?>><?php echo t('Right')?></option>
			</select>
		</td>
	</tr>
	<tr>
		<th><acronym title="Set to auto to let the browser automacially show a scrollbar if nessecary, or to no to never show a scrollbar."><?php echo t('Scrolling')?></acronym></th>
		<td>
			<select name="scrolling">
				<option value="auto"<?php echo $formController->scrolling=='auto'?' selected="selected"':''; ?>><?php echo t('Auto')?></option>
				<option value="yes"<?php echo $formController->scrolling=='yes'?' selected="selected"':''; ?>><?php echo t('Yes')?></option>
				<option value="no"<?php echo $formController->scrolling=='no'?' selected="selected"':''; ?>><?php echo t('No')?></option>
			</select>
		</td>
	</tr>
	<tr>
		<th><?php echo t('Margin Width')?></th>
		<td><input type="text" name="marginwidth" value="<?php echo $formController->marginwidth; ?>"></td>
	</tr>
	<tr>
		<th><?php echo t('Margin Height')?></th>
		<td><input type="text" name="marginheight" value="<?php echo $formController->marginheight; ?>"></td>
	</tr>
</table>
