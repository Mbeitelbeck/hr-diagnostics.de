<?php 
	defined('C5_EXECUTE') or die(_("Access Denied."));
	
	class IframeBlockController extends BlockController {

		protected $btDescription = "Insert an IFRAME";
		protected $btName = "iFrame";
		protected $btInterfaceWidth = 300;
		protected $btInterfaceHeight = 250;
		protected $btTable = 'btIframe';		
		
	}
?>
