<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$c = Page::getByID($_REQUEST['cID'], $_REQUEST['cvID']);

$p = Page::getByID($link);
$pagePath = $p->cPath;

$i = $controller->getFileObject($fID);
$imagePath = $i->getRelativePath();
?>
<div class="subitem">
	<div class="top"><h2 class="flashheadline"><a href="<?php echo $pagePath ?>"><?php echo $title; ?></a></h2></div>
	<div class="content">
		<div class="leftcol">
			<?php echo $content; ?>
		</div>
		<div class="rightcol">
			<?php if($fID != 0) { ?><a href="<?php echo $pagePath ?>"><img src="<?php echo $imagePath ?>" alt="<?php echo $fID; ?>" /></a><?php } ?>
		</div>
		<div class="footer"></div>
	</div>
	<a class="btn" href="<?php echo $pagePath ?>"><img src="/themes/hr-d/_images/subitem/mehr-erfahren-<?php echo $_SESSION["lang"]; ?>.jpg" alt="mehr erfahren"></a>
</div>