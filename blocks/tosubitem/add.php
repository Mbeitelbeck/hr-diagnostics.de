
<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true;
$assetLibraryPassThru = array(
	'type' => 'image'
);
$al = Loader::helper('concrete/asset_library');
?>
<h2><strong>Allgemein</strong></h2>
<div class="ccm-block-field-group">
<h2><?php echo t('Überschirft')?></h2>
<?php echo  $form->text('title', array('style' => 'width: 250px')); ?>
</div>

<h2><strong>Inhalt</strong></h2>
<div id="ccm-block-field-group">
<h2><?php echo t('Text')?></h2>
<textarea id="ccm-content-<?php echo $a->getAreaID()?>" class="ccm-advanced-editor" name="content"></textarea>
</div>

<h2 style="padding-top:20px; margin-top:10px; border-top:1px dotted #aaaaaa;"><strong>Bild</strong></h2>
<div class="ccm-block-field-group">
<h2><?php echo t('Bild')?></h2>
<?php echo $al->image('fID', 'fID', t('Choose Image'), $fID);?>
</div>

<h2 style="padding-top:20px; margin-top:10px; border-top:1px dotted #aaaaaa;"><strong>Link</strong></h2>
<div class="ccm-block-field-group">
<?php  $form = Loader::helper('form/page_selector');
print $form->selectPage('link', $link);?>
</div>

