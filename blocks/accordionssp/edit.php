﻿<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$replaceOnUnload = 1;
$bt->inc('editor_init.php');
$includeAssetLibrary = true; 
$assetLibraryPassThru = array(
	'type' => 'image'
);
$al = Loader::helper('concrete/asset_library');
$choiceSSP = $controller->getChoice("SSP");

?>
<h2><strong>Allgemein</strong></h2>
<div class="ccm-block-field-group">
<h2><?php echo t('Titel')?></h2>
<?php echo  $form->text('title', $title, array('style' => 'width: 250px')); ?>
</div>

<h2><strong>Inhalt</strong></h2>
<div id="ccm-block-field-group">
<h2><?php echo t('Text')?></h2>
<textarea id="ccm-content-<?php echo $b->getBlockID()?>-<?php echo $a->getAreaID()?>" class="ccm-advanced-editor" name="content"><?php echo $controller->getContentEditMode()?></textarea>
</div>

<h2 style="padding-top:20px; margin-top:10px; border-top:1px dotted #aaaaaa;"><strong><?php echo t('SSP')?></strong></h2>
<div class="ccm-block-field-group">
<input<?php if ($choiceSSP=="verfuegbar") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceSSP" id="choiceSSP4" value="verfuegbar" type="radio">verf&uuml;gbar/inklusive<br />
<input<?php if ($choiceSSP=="optional") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceSSP" id="choiceSSP5" value="optional" type="radio">optional<?php /*?><br>
<input<?php if ($choiceS=="nicht-verfuegbar") { echo ' checked="checked"'; } ?> class="ccm-input-radio" name="choiceS" id="choiceSSP6" value="nicht-verfuegbar" type="radio">nicht verf&uuml;gbar<?php */?>
</div>
