<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
global $initAccordion;
$c = Page::getByID($_REQUEST['cID'], $_REQUEST['cvID']);

if($initAccordion && !$c->isEditMode()) { ?><script type="text/javascript" src="/blocks/accordion/accordion.js"></script><?php }
if($initAccordion && $c->isEditMode()) { ?><style type="text/css">.accordion .content {display:none;}</style><?php }

$initAccordion = false;
?>
<div class="accordion">
	<div class="top">
		<h4><?php echo $title; ?></h4>
		<div class="choise">
			<?php echo($controller->getContentAndGenerate()); ?>
		</div>
		<div class="footer"></div>
	</div>
	<div class="content">
		<?php echo $content; ?>
	</div>
	<div class="footer"></div>
</div>