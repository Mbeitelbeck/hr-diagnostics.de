<?php
/**
 * User: l.pommerenke
 * Date: 24.01.13
 * Time: 14:30
 */

class ShopCategoryPageTypeController
    extends Controller
{
    protected $c = NULL;
    protected $oberDimensionen = array(
        'Interessen'     => '/themes/hr-d/_images/test/messbereich_i.png',
        'Leistung'       => '/themes/hr-d/_images/test/messbereich_l.png',
        'Verhalten'      => '/themes/hr-d/_images/test/messbereich_v.png',
        'Persönlichkeit' => '/themes/hr-d/_images/test/messbereich_p.png',
    );

    const BLOCK_TYPE_ID_EINZELTEST   = 0;
    const BLOCK_TYPE_ID_TESTBATTERIE = 1;
    const CHILD_PAGE_TEASERS         = 'teasers';
    const TEST_TYPE_EINZEL_TESTS     = 'einzelTests';
    const TEST_TYPE_TEST_BATTERIEN   = 'testBatterien';

    public function view()
    {
        $this->c = Page::getCurrentPage();
        /** @var $imageHelper ImageHelper */
        $imageHelper = Loader::helper('image');

        $testsAndTeasers = $this->loadTestsAndTeasers($this->c);
        $this->set(self::CHILD_PAGE_TEASERS, $testsAndTeasers[self::CHILD_PAGE_TEASERS]);
        $this->set(self::TEST_TYPE_EINZEL_TESTS, $testsAndTeasers[self::TEST_TYPE_EINZEL_TESTS]);
        $this->set(self::TEST_TYPE_TEST_BATTERIEN, $testsAndTeasers[self::TEST_TYPE_TEST_BATTERIEN]);

        $this->set('categoryLinks', $this->getCategoryLinks($this->c, $imageHelper));
        $this->set('oberDimensionen', $this->oberDimensionen);
        $this->set('dimensionenSet', $this->createDimensionSet());
    }

    /**
     * @return array
     */
    private function createDimensionSet()
    {
        $cacheId        = 'dimensionSet';
        $type           = 'dimensionSetType';
        $dimensionenSet = Cache::get($type, $cacheId);
        if (!empty($dimensionenSet)) {
            return $dimensionenSet;
        }

        $dimensionenSet = array();

        $dimensionenSet['Leistung']       = $this->getDimensionen('leistung');
        $dimensionenSet['Persönlichkeit'] = $this->getDimensionen('persoenlichkeit');
        $dimensionenSet['Verhalten']      = $this->getDimensionen('verhalten');
        $dimensionenSet['Interessen']     = $this->getDimensionen('interessen');

        Cache::set($type, $cacheId, $dimensionenSet);

        return $dimensionenSet;
    }

    /**
     * @param Page $c
     *
     * @return array
     */
    private function loadTestsAndTeasers(Page $c)
    {
        $cacheId = $c->getCollectionID();

        $testsAndTeaser = Cache::get('testdata', $cacheId);
        if (!empty($testsAndTeaser)) {
            return $testsAndTeaser;
        }

        Loader::model('page_list');
        $pl = new PageList();
        $pl->filterByParentID($c->getCollectionID());
        $pl->sortByDisplayOrder();

        $pages          = $pl->get($pl->getTotal());
        $testsAndTeaser = array(
            self::CHILD_PAGE_TEASERS       => array(),
            self::TEST_TYPE_EINZEL_TESTS   => array(),
            self::TEST_TYPE_TEST_BATTERIEN => array(),
        );

        /** @var $testPage Page */
        foreach ($pages as $testPage) {
            $testCollectionId = $testPage->getCollectionID();

            $testsAndTeaser[self::CHILD_PAGE_TEASERS][] = $this->extractTeaser($testPage);

            /** @var $block Block */
            foreach ($testPage->getBlocks('Inhalt') as $block) {
                if ($block->getBlockTypeID() == 30) {
                    $blockInstance = $block->getInstance();
                    // hoffentlich klappt
                    if ($blockInstance->testtyp == self::BLOCK_TYPE_ID_EINZELTEST) {
                        $testsAndTeaser[self::TEST_TYPE_EINZEL_TESTS][$testCollectionId] =
                            $this->extractEinzeltest($testPage, $blockInstance);
                    }
                    elseif ($blockInstance->testtyp == self::BLOCK_TYPE_ID_TESTBATTERIE) {
                        $testsAndTeaser[self::TEST_TYPE_TEST_BATTERIEN][$testCollectionId] =
                            $this->extractTestbatterie($testPage, $blockInstance);
                    }
                }
            }
        }

        Cache::set('testdata', $cacheId, $testsAndTeaser);

        return $testsAndTeaser;
    }

    /**
     * @param $typ
     *
     * @return array
     */
    private function getDimensionen($typ)
    {
        /** @var $db ADOConnection */
        $db = Loader::db();

        $sql = "
        SELECT `aso`.`akID`, `aso`.`VALUE`
        FROM `atSelectOptions` `aso`
          JOIN `AttributeKeys` `ak`
            ON `ak`.`aKID` = `aso`.`akID`
        WHERE `ak`.`akHandle` = ?
        ";

        $result = $db->getAll($sql, array($typ));

        $dimensions = array();

        foreach ($result as $begriff) {
            foreach ($begriff as $name => $typ) {
                if ($name != 'akID') {
                    $dimensions[] = $typ;
                }
            }
        }

        return $dimensions;
    }

    /**
     * @param Page        $c
     * @param ImageHelper $imageHelper
     *
     * @return string
     */
    private function getCategoryLinks(Page $c, $imageHelper)
    {
        $cacheId = 'categoryLinks' . $c->getCollectionID();

        $categoryLinks = Cache::get('shopCategory', $cacheId);
        if (!empty($categoryLinks)) {
            return $categoryLinks;
        }

        $data = array(
            149 => 541,
            169 => 547,
            170 => 542,
            171 => 544,
            172 => 545,
            173 => 543,
            174 => 546,
        );

        $linkFormat = '<p><a class="categorylink %s" href="%s" style="background-image:url(\'%s\');" >%s (%d)</a></p>';

        $categoryLinks = array();
        foreach ($data as $pageId => $imageId) {
            /** @var $categoryPage Page */
            $categoryPage = Page::getByID($pageId);
            $active       = ($c->getCollectionID() == $pageId) ? 'active' : '';

            $categoryLinks[] = sprintf(
                $linkFormat,
                $active,
                $categoryPage->getCollectionPath(),
                $imageHelper->getThumbnail(File::getByID($imageId), 30, 30, TRUE)->src,
                $categoryPage->getCollectionName(),
                $categoryPage->getNumChildren()
            );
        }

        $categoryLinks = implode('', $categoryLinks);
        Cache::set('shopCategory', $cacheId, $categoryLinks);

        return $categoryLinks;
    }

    /**
     * @param $blockInstance
     *
     * @return array
     */
    private function makeDimension($blockInstance)
    {
        $dimensionen = $blockInstance->dimensionen;
        if (strpos($dimensionen, ",") !== FALSE) {
            return explode(',', $dimensionen);
        }
        else {
            return array($dimensionen);
        }
    }

    /**
     * @param Page $testPage
     *
     * @return array
     */
    private function extractTeaser(Page $testPage)
    {
        foreach ($testPage->getBlocks('Test-Header') as $block) {
            /** @var $block Block */
            if ($block->getBlockTypeID() == 8) {
                $blockController = $block->getInstance();

                return array(
                    'testUrl'  => $this->getOriginalTeaserPath($testPage->getPagePaths()),
                    'imageUrl' => File::getByID($blockController->fID)->getRelativePath()
                );
            }
        }
    }

    /**
     *
     */
    private function extractEinzeltest(Page $testPage, $blockInstance)
    {
        $testData = new stdClass();

        $testData->dimensionen      = $this->makeDimension($blockInstance);
        $testData->title            = $blockInstance->title;
        $testData->kurzbeschreibung = $blockInstance->kurzbeschreibung;
        $testData->content          = $blockInstance->content;
        $testData->fID              = $blockInstance->fID;
        $testData->preis            = $blockInstance->preis;
        $testData->features         = $blockInstance->features;
        $testData->imagePath        = File::getRelativePathFromID($blockInstance->fID);

        $testData->pagePath = $this->getOriginalPagePath($testPage->getPagePaths(), 'einzeltests');

        return $testData;
    }

    private function getOriginalTeaserPath(array $pagePaths)
    {
        foreach ($pagePaths as $pp) {
            $cPath = $pp['cPath'];

            if ((stripos($cPath, 'einzeltests')  !== FALSE) OR
                (stripos($cPath, 'testbatterie') !== FALSE)
            ) {
                return $cPath;
            }
        }
    }

    /**
     * @param array $pagePaths
     * @param       $pattern
     *
     * @return
     */
    private function getOriginalPagePath(array $pagePaths, $pattern)
    {
        foreach ($pagePaths as $pp) {
            $cPath = $pp['cPath'];

            if (stripos($cPath, $pattern) !== FALSE) {
                return $cPath;
            }
        }
    }

    /**
     * @param Page $testPage
     * @param      $blockInstance
     *
     * @return object
     */
    private function extractTestbatterie(Page $testPage, $blockInstance)
    {
        $testBatterie = new stdClass();

        $testBatterie->title            = $blockInstance->title;
        $testBatterie->kurzbeschreibung = $blockInstance->kurzbeschreibung;
        $testBatterie->content          = $blockInstance->content;
        $testBatterie->fID              = $blockInstance->fID;
        $testBatterie->preis            = $blockInstance->preis;
        $testBatterie->link             = $blockInstance->link;
        $testBatterie->dimensionen      = $this->makeDimension($blockInstance);
        $testBatterie->features         = $blockInstance->features;
        $testBatterie->imagePath        = File::getRelativePathFromID($blockInstance->fID);

        $testBatterie->pagePath = $this->getOriginalPagePath($testPage->getPagePaths(), 'testbatterie');

        return $testBatterie;
    }
}
