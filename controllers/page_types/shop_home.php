<?php
/**
 * User: l.pommerenke
 * Date: 07.02.13
 * Time: 14:21
 */

class ShopHomePageTypeController
    extends Controller
{
    protected $c = NULL;

    public function on_before_render()
    {
        $this->c = Page::getCurrentPage();
        $this->set('teasers', $this->loadTeasers($this->c->getCollectionID()));
    }

    /**
     * Lädt die relevanten Teaserbilderinformationen (für die Startseite von testverlag.de)
     *
     * Die Ergebnisse werden gecacht.
     *
     * @param $collectionId
     *
     * @return array
     */
    public function loadTeasers($collectionId)
    {
        $teasers = Cache::get('teaser', $collectionId);
        if (!empty($teasers)) {
            return $teasers;
        }

        $teasers = array();

        Loader::model('page_list');
        $grandparentList = new PageList();
        $grandparentList->filterByParentID($collectionId);
        $grandparentList->sortByDisplayOrder();
        $grandparentList->ignorePermissions();

        $grandparentPages = $grandparentList->get($grandparentList->getTotal());

        /** @var $grandParent Page */
        foreach($grandparentPages as $grandParent) {
            $parentList = new PageList();
            $parentList->filterByParentID($grandParent->getCollectionID());
            $parentList->filterByIsAlias(FALSE);
            $parentList->filterByAttribute('exclude_from_teaser', 1, '!=');
            $parentList->sortByDisplayOrder();
            $parentList->ignorePermissions();

            $parentPages = $parentList->get($parentList->getTotal());

            /** @var $page Page */
            foreach ($parentPages as $page) {
                foreach ($page->getBlocks('Test-Header') as $b) {
                    if ($b->getBlockTypeID() == 8) {
                        $controller = $b->getInstance();

                        $teasers[$page->getCollectionPath()] = File::getByID($controller->fID)->getRelativePath();
                    }
                }
            }
        }

        Cache::set('teaser', $collectionId, $teasers);

        return $teasers;
    }

}
