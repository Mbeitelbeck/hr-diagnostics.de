<?php 

class SecurimageSystemCaptchaTypeController extends SystemCaptchaTypeController {

   	protected $securimage;
   
	public function __construct() {
		Loader::library("3rdparty/securimage/securimage");
		
		$this->securimage = new Securimage();
		$this->securimage->image_width   = 190;
		$this->securimage->image_height  = 60;
		$this->securimage->image_bg_color = new Securimage_Color(250, 250, 250);
		$this->securimage->line_color = new Securimage_Color(51, 51, 51);
		$this->securimage->num_lines = 5;
		$this->securimage->line_color = new Securimage_Color(204, 204, 204);
		$this->securimage->use_wordlist  = true;
		
		$this->securimage->use_multi_text   = false;
		$this->securimage->text_color = new Securimage_Color(132, 188, 17);		
	}
	
	/** 
	 * Display the captcha
	 */ 
	public function display() {
	  echo '<script type="application/javascript">';
	  echo 'var locale = "'.LOCALE.'";';
	  echo '</script>';
	  if(LOCALE == 'de_DE')
	  { 
	  	$text = 'Bild anklicken um ein anderes captcha zu zeigen.';
	  }
	  else
	  {
		  $text = 'Click the image to see another captcha.';
	  }
	  $ci = Loader::helper('concrete/urls');
	  echo '<div><img src="' . $ci->getToolsURL('captcha') . '?nocache=' .time(). '" alt="' .t('Captcha Code'). '" onclick="this.src = \'' . $ci->getToolsURL('captcha') . '?nocache=' .$time. '\'" class="ccm-captcha-image" /></div>';
      echo '<br/><div>'.$text.'</div>';
	}
	
	public function label() {
		$form = Loader::helper('form');
		print $form->label('captcha', t('Please type the letters and numbers shown in the image.'));
	}
	
	
	/** 
	 * Print the captcha image. You usually don't have to call this method directly.
	 * It gets called by captcha.php from the tools 
	 */	 	
	public function displayCaptchaPicture() {
	   $this->securimage->show();
	}
	
	/**
	 * Displays the text input field that must be entered when used with a corresponding image.
	 */
	public function showInput($args = false)
	{
	$attribs = '';
		if (is_array($args)) {
			foreach($args as $key => $value) {
				$attribs .= $key . '="' . $value . '" ';
			}
		}
	  echo '<div><input type="text" name="ccmCaptchaCode" class="ccm-input-captcha" ' . $attribs . ' /></div><br/>';
	}
	
	/** 
	 * Checks the captcha code the user has entered.
	 *    	 
	 * @param string $fieldName Optional name of the field that contains the captcha code
	 * @return boolean true if the code was correct, false if not
	 */   	
	public function check($fieldName='ccmCaptchaCode') {
	   return $this->securimage->check($_REQUEST[$fieldName]);
	}


}