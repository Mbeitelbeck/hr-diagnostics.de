<?php
class BenchmarkPageRender {
  public function startTimer() {
    Cache::set('benchmark_page_render', false, microtime(true));
  }
  public function endTimer() {
    $te = microtime(true);
    $ts = Cache::get('benchmark_page_render', false);
    $time = $te - $ts;
	echo $time;
  }
}
?>