<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$array = explode('/', $c->getCollectionPath());
$chapter = $array[2] == "" ? "home" : $array[2];
$lang = $array[1];
$_SESSION["lang"] = $lang;

if (!empty($_GET['cID'])) {
    $pageURL = Page::getCollectionPathFromID($_GET['cID']);
    $path = explode('/', $pageURL);
} else {
    $pageURL = parse_url($_SERVER['REQUEST_URI']);
    $path = explode('/', $pageURL['path']);
}

$navLang = $path[1];
$knowSubNum1 = '';
$knowSubNum2 = '';
$knowSubNum3 = '';
$knowSubNum4 = '';
$knowSubNum5 = '';
$knowSubNum6 = '';
$knowSubNum7 = '';
$knowSubNum8 = '';
$knowSubNum9 = '';

$knowActive = false;
$testsActive = false;
$homeAnimation = false;
$softwareActive = false;
$kontaktActive = false;
$contactActive = false;


if ($path[1] == 'de' || $path[1] == 'en' || $path[1] == 'bewerbung') {
    if ($path[2] == '') {
        $homeAnimation = true;
    } else
        if ($path[2] == 'knowhow' || $path[2] == 'expertise') {
            $knowActive = 'YES';

            if ($path[3]) {
                $knowSub = ' sub-page ';

                if ($path[3] == 'erecruiting') {
                    $knowNum = 'one';
                    $knowSubNum1 = ' active';
                }
                if ($path[3] == 'personalauswahl' || $path[3] == 'staff_selection') {
                    $knowNum = 'two';
                    $knowSubNum2 = ' active';
                }
                if ($path[3] == 'potenzialanalyse' || $path[3] == 'potential_analysis') {
                    $knowNum = 'three';
                    $knowSubNum3 = 'active';
                }
                if ($path[3] == 'leistungsbeurteilung' || $path[3] == 'performance_appraisal') {
                    $knowNum = 'four';
                    $knowSubNum4 = ' active';
                }
                if ($path[3] == 'personalentwicklung' || $path[3] == 'human_resource_development') {
                    $knowNum = 'five';
                    $knowSubNum5 = ' active';
                }
                if ($path[3] == 'jobmatching' || $path[3] == 'job_matching') {
                    $knowNum = 'six';
                    $knowSubNum6 = 'active';
                }
                if ($path[3] == 'berufsprofiling' || $path[3] == 'job_profiling') {
                    $knowNum = 'seven';
                    $knowSubNum7 = 'active';
                }
                if ($path[3] == 'background_screening') {
                    $knowNum = 'eight';
                    $knowSubNum8 = 'active';
                }
            }
        } else
            if ($path[2] == 'tests') {
                $testsActive = 'YES';

                if ($path[3]) {
                    $testsSub = ' sub-page ';

                    if ($path[3] == 'testnavigator' || $path[3] == 'test_navigator') {
                        $testsNum = 'one';
                        $testsSubNum1 = ' active';
                    }
                    if ($path[3] == 'testmanufaktur' || $path[3] == 'test_factory') {
                        $testsNum = 'two';
                        $testsSubNum2 = ' active';
                    }
                }
            } else
                if ($path[2] == 'software') {
                    $softwareActive = 'YES';

                    if ($path[3]) {
                        $softwareSub = ' sub-page ';

                        if ($path[3] == 'jobmatcher') {
                            if ($path[4] == 'ssp_testplattform' || $path[4] == 'ssp') {
                                $softwareNum = 'one';
                                $softwareSubNum1 = ' active';
                            }
                            if ($path[4] == 'erecruiting' || $path[4] == 'e_recruiting') {
                                $softwareNum = 'two';
                                $softwareSubNum2 = ' active';
                            }
                        }
                        if ($path[3] == 'produktivitaetswerkzeuge' || $path[3] == 'productivity_tools') {
                            $softwareNum = 'three';
                            $softwareSubNum3 = ' active';
                        }
                        if ($path[3] == 'integration') {
                            $softwareNum = 'four';
                            $softwareSubNum4 = ' active';
                        }
                        if ($path[3] == 'sicherheit' || $path[3] == 'security') {
                            $softwareNum = 'five';
                            $softwareSubNum5 = ' active';
                        }
                        if ($path[3] == 'service') {
                            $softwareNum = 'six';
                            $softwareSubNum6 = ' active';
                        }
                    }
                } else
                    if ($path[2] == 'kontakt' || $path[2] == 'contact') {
                        $kontaktActive = 'YES';




                    } else
                        if (($path[1] == 'de' || $path[1] == 'en') && $path[2] == '') {
                            $animation = 'YES';
                        } else
                            if (($path[1] == 'de' || $path[1] == 'en') && $path[2] != '') {
                                $general = 'general';
                            }
} else if ($path[1] == 'login') {
    $general = 'general';
}
?>
    <!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="content-language" content="de"/>
        <?php Loader::element('header_required'); ?>
        <meta http-equiv="imagetoolbar" content="no"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <meta name="copyright" content="team:orange GmbH - http://www.teamorange.de"/>
        <meta name="description" content="HR-Diagnostics global"/>
        <meta name="keywords" content="HR-Diagnostics global"/>
        <meta name="publisher" content="HR-Diagnostics global"/>
        <meta name="robots" content="index,follow"/>

        <link rel="shortcut icon" href="/themes/hr-d/favicon.ico"/>

        <link rel="stylesheet" href="/themes/hr-d/_styles/_reset.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="/themes/hr-d/_styles/screen.min.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="/themes/hr-d/_scripts/jquery/jquery.fancybox/jquery.fancybox.css" type="text/css"
              media="screen"/>
        <link rel="stylesheet" href="/themes/hr-d/_styles/explorer.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="/themes/hr-d/_styles/explorer-print.css" type="text/css" media="print"/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans' type='text/css'/>
        <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <?php if ($path[3] == 'testnavigator' || $path[3] == 'test_navigator') { ?>
            <link rel="stylesheet" href="/themes/hr-d/_styles/flora/flora.all.css" type="text/css" media="all"/>
        <?php } ?>

    </head>
<body id="<?php echo $c->getCollectionHandle() ?>" class="<?php echo $chapter;
if (isset($_GET["popup"])) {
    echo " popup";
} ?>">
    <script type="application/javascript">var locale = "<?php echo LOCALE; ?>"</script>

<div id="stage">
<div id="page">
    <div class="page-top"></div>
<div class="page-middle">
    <div id="header">
        <?php
        if ($lang == 'de') {
            $pagesTopID = 61;
        } else {
            $pagesTopID = 98;
        }
        $bt_main = BlockType::getByHandle('autonav');
        $bt_main->controller->displayPages = 'custom';
        $bt_main->controller->displayPagesCID = $pagesTopID;
        $bt_main->controller->orderBy = 'display_asc';
        $bt_main->controller->displaySubPages = 'all';
        $bt_main->controller->displaySubPageLevels = 'all';
        $bt_main->render('templates/top_menu');
        ?>
        <a href="/<?php echo $path[1] ?>/?hr" id="logo">
            <img src="/themes/hr-d/_images/hr-diagnostics-logo.png" width="247" height="82" alt="HR-Diagnostics"/>
        </a>

        <div id="langswitcher">
            <ul>
                <li>
                    <a class="<?php if ($_SESSION["lang"] == "de") {
                        echo('active');
                    } ?>" href="/de/">De</a>

                </li>
                <li>
                    <a class="<?php if ($_SESSION["lang"] == "en") {
                        echo('active');
                    }?>" href="/en/">En</a>
                </li>
            </ul>
        </div>

    </div>

    <script>
        function selectMenuHoverClass(ID) {
            /* Clear all menuHover classes */
            $('#know-how-btn a, #tests-btn a, #software-btn a, #kontakt-btn a').removeClass("menuHover");

            /* Set new menuHover class if not 'none' */
            if (ID != 'none') {
                $(ID).addClass("menuHover");
            }
        }

        $(window).load(function () {
            $('.nav-hover').css({
                '-webkit-transition-duration': '300ms',
                '-moz-transition-duration': '300ms',
                '-ms-transition-duration': '300ms',
                '-o-transition-duration': '300ms',
                'transition-duration': '300ms'
            });
            $('li[id$="-btn"] a').css({
                '-webkit-transition-duration': '200ms',
                '-moz-transition-duration': '200ms',
                '-ms-transition-duration': '200ms',
                '-o-transition-duration': '200ms',
                'transition-duration': '200ms'
            });
        });
    </script>

<?php if ($homeAnimation) { ?>
    <style type="text/css" scoped="scoped">
        header.nav-container {
            background: url("/themes/hr-d/_images/content-back.png") repeat-y 0 0;
            height: 303px;
        }

        .nav-hover {
            width: 0;
            left: 0;
        }
    </style>
    <script>
        $(window).load(function () {
            var knowhow = $('#know-how-btn').outerWidth();
            var knowhow2 = $('#know-how-btn').outerWidth(true);
            var tests = $('#tests-btn').outerWidth();
            var tests2 = $('#tests-btn').outerWidth(true);
            var software = $('#software-btn').outerWidth();
            var kontakt = $('#kontakt-btn').outerWidth();


            $('#know-how-btn').mouseenter(function () {
                $('.nav-hover').css({'width': knowhow, 'left': '26px'});
                selectMenuHoverClass('#know-how-btn a');
            });
            $('#tests-btn').mouseenter(function () {
                $('.nav-hover').css({'width': tests, 'left': knowhow2 + 26});
                selectMenuHoverClass('#tests-btn a');
            });
            $('#software-btn').mouseenter(function () {
                $('.nav-hover').css({'width': software, 'left': knowhow2 + tests2 + 26});
                selectMenuHoverClass('#software-btn a');
            });
            $('#kontakt-btn').mouseenter(function () {
                $('.nav-hover').css({'width': kontakt, 'left': knowhow2 + tests2 + software + 350});
                selectMenuHoverClass('#kontakt-btn a');
            });

            $('.nav-container').mouseleave(function () {
                $('.nav-hover').css({'width': '0', 'left': '0'});
                selectMenuHoverClass('none');
            });

            $('.nav-detail-animation').animate({
                opacity: 1
            }, 500);
            $('.nav-detail-animation .bg-animation').delay(800).animate({
                opacity: 1
            }, 500);
            $('.nav-detail-animation .bg-animation .bg-ani-overlay').delay(1300).animate({
                opacity: 1
            }, 3000);

            $('.nav-detail-animation .bg-animation .bg-ani-0').delay(2000).animate({
                opacity: 1
            }, 800);

            $('.nav-detail-animation .bg-animation .bg-ani-1').delay(1300).animate({
                opacity: 1
            }, 800);
            $('.nav-detail-animation .bg-animation .bg-ani-2').delay(900).animate({
                opacity: 1
            }, 800);
            $('.nav-detail-animation .bg-animation .bg-ani-3').delay(1600).animate({
                opacity: 1
            }, 800);

            $('.nav-detail-animation .bg-animation .bg-ani-4').delay(3700).animate({
                bottom: '0'
            }, 400);
            $('.nav-detail-animation .bg-animation .bg-ani-5').delay(3400).animate({
                bottom: '0'
            }, 400);
            $('.nav-detail-animation .bg-animation .bg-ani-6').delay(2700).animate({
                bottom: '0'
            }, 400);
            $('.nav-detail-animation .bg-animation .bg-ani-7').delay(2900).animate({
                bottom: '0'
            }, 400);
            $('.nav-detail-animation .bg-animation .bg-ani-8').delay(2200).animate({
                bottom: '0'
            }, 400);
            $('.nav-detail-animation .bg-animation .bg-ani-9').delay(2000).animate({
                bottom: '0'
            }, 400);

            $('.nav-detail-animation .logo-animation').delay(300).animate({
                opacity: 1
            }, 500);
            $('.nav-detail-animation .logo-animation').delay(1000).animate({
                opacity: 0
            }, 1000);

        });
    </script>
<?php } else { ?>

    <script>
    $(document).ready(function () {

        if ($('.nav-over').hasClass('knowhow') || $('.nav-over').hasClass('expertise')) {
            selectMenuHoverClass('#know-how-btn a');
        } else if ($('.nav-over').hasClass('tests')) {
            selectMenuHoverClass('#tests-btn a');
        } else if ($('.nav-over').hasClass('software')) {
            selectMenuHoverClass('#software-btn a');
        } else if ($('.nav-over').hasClass('kontakt')) {
            selectMenuHoverClass('#kontakt-btn a');
        }

        var knowHowClass = '.know-how-slide.sub-page .animation';
        if ($('.know-how-slide').hasClass('one') ||
            $('.know-how-slide').hasClass('two') ||
            $('.know-how-slide').hasClass('seven')) {
            var rnd = Math.floor(Math.random() * 25) + 1;
            if (rnd == 1) {
                $(knowHowClass).addClass('one');
            }
            if (rnd == 2) {
                $(knowHowClass).addClass('two');
            }
            if (rnd == 3) {
                $(knowHowClass).addClass('three');
            }
            if (rnd == 4) {
                $(knowHowClass).addClass('four');
            }
            if (rnd == 5) {
                $(knowHowClass).addClass('five');
            }
            if (rnd == 6) {
                $(knowHowClass).addClass('six');
            }
            if (rnd == 7) {
                $(knowHowClass).addClass('seven');
            }
            if (rnd == 8) {
                $(knowHowClass).addClass('eight');
            }
            if (rnd == 9) {
                $(knowHowClass).addClass('nine');
            }
            if (rnd == 10) {
                $(knowHowClass).addClass('ten');
            }
            if (rnd == 11) {
                $(knowHowClass).addClass('eleven');
            }
            if (rnd == 12) {
                $(knowHowClass).addClass('twelve');
            }
            if (rnd == 13) {
                $(knowHowClass).addClass('thirteen');
            }
            if (rnd == 14) {
                $(knowHowClass).addClass('fourteen');
            }
            if (rnd == 15) {
                $(knowHowClass).addClass('fifteen');
            }
            if (rnd == 16) {
                $(knowHowClass).addClass('sixteen');
            }
            if (rnd == 17) {
                $(knowHowClass).addClass('seventeen');
            }
            if (rnd == 18) {
                $(knowHowClass).addClass('eighteen');
            }
            if (rnd == 19) {
                $(knowHowClass).addClass('nineteen');
            }
            if (rnd == 20) {
                $(knowHowClass).addClass('twenty');
            }
            if (rnd == 21) {
                $(knowHowClass).addClass('twenty-one');
            }
            if (rnd == 22) {
                $(knowHowClass).addClass('twenty-two');
            }
            if (rnd == 23) {
                $(knowHowClass).addClass('twenty-three');
            }
            if (rnd == 24) {
                $(knowHowClass).addClass('twenty-four');
            }
            if (rnd == 25) {
                $(knowHowClass).addClass('twenty-five');
            }
        }
    });

    $(window).load(function () {
        <?php if(!$knowSub) { ?>
        $(".know-how-slide .bg-animation").animate({
            opacity: 1
        }, 500);
        <?php } ?>
        
        if ($('.know-how-slide').hasClass('one')) {
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-animation").delay(300).animate({
                top: "-8px",
                opacity: 1
            }, 500);
        }

        if ($('.know-how-slide').hasClass('two')) {
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-animation").css({
                'opacity': '1',
                'background': 'none',
                'left': '570px'
            });
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-1").delay(600).animate({
                opacity: 1
            }, 600);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-2").delay(1100).animate({
                opacity: 1
            }, 600);
        }

        if ($('.know-how-slide').hasClass('one') ||
            $('.know-how-slide').hasClass('two') ||
            $('.know-how-slide').hasClass('three') ||
            $('.know-how-slide').hasClass('four') ||
            $('.know-how-slide').hasClass('five') ||
            $('.know-how-slide').hasClass('seven')) {
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .slide-inner .animation").animate({
                top: "0px",
                opacity: 1
            }, 500);
        }

        if ($('.know-how-slide').hasClass('three')) {
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-animation").delay(650).animate({
                opacity: 1,
                width: "153px"
            }, 500);
        }

        if ($('.know-how-slide').hasClass('four')) {
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-1, .know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-2, .know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-3").delay(500).animate({
                opacity: 1
            }, 900);
        }

        if ($('.know-how-slide').hasClass('five')) {
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-1").delay(500).animate({
                opacity: 1
            }, 500);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-2").delay(800).animate({
                opacity: 1
            }, 500);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-3").delay(1000).animate({
                opacity: 1
            }, 500);
        }

        if ($('.know-how-slide').hasClass('six')) {
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-1").delay(500).animate({
                opacity: 1
            }, 300);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-2").delay(700).animate({
                opacity: 1
            }, 300);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-3").delay(900).animate({
                opacity: 1
            }, 300);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-4").delay(1100).animate({
                opacity: 1
            }, 300);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-5").delay(1300).animate({
                opacity: 1
            }, 300);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-6").delay(1700).animate({
                opacity: 1
            }, 400);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-7").animate({
                opacity: 1,
                top: '54px'
            }, 400);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-8, .know-how-slide.sub-page.<?php echo $knowNum ?> .animation").delay(1900).animate({
                opacity: 1,
                height: '50px'
            }, 300).animate({
                    width: '160px'
                }, 300);
        }

        if ($('.know-how-slide').hasClass('seven')) {
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-animation").delay(600).animate({
                opacity: 1,
                top: '22px'
            }, 500);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-1").delay(800).animate({
                opacity: 1,
                height: '40px'
            }, 300).animate({
                    width: '155px'
                }, 300);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-2").delay(800).animate({
                opacity: 1,
                height: '40px'
            }, 300).animate({
                    width: '55px'
                }, 300);
        }

        if ($('.know-how-slide').hasClass('eight')) {
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-1").animate({
                opacity: 1,
                top: '18px'
            }, 500).delay(400).animate({
                    opacity: 0
                }, 400);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-2").delay(500).animate({
                opacity: 1
            }, 0);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-3").delay(500).animate({
                opacity: 1,
                left: '82px',
                top: '40px'
            }, 400);
            $(".know-how-slide.sub-page.<?php echo $knowNum ?> .bg-ani-4").delay(900).animate({
                opacity: 1
            }, 600);
        }

        $('#detail-slider').css({
            '-webkit-transition-duration': '1s',
            '-moz-transition-duration': '1s',
            '-ms-transition-duration': '1s',
            '-o-transition-duration': '1s',
            'transition-duration': '1s'
        });

        var knowhow = $('#know-how-btn').outerWidth();
        var knowhow2 = $('#know-how-btn').outerWidth(true);
        var tests = $('#tests-btn').outerWidth();
        var tests2 = $('#tests-btn').outerWidth(true);
        var software = $('#software-btn').outerWidth();
        var kontakt = $('#kontakt-btn').outerWidth();
        var contact = $('.en #kontakt-btn').outerWidth();

        $('#know-how-btn').mouseenter(function () {
            $('#detail-slider').css('left', '0');
            $('.nav-hover').css({'width': knowhow, 'left': '26px'});
            selectMenuHoverClass('#know-how-btn a');
        });
        $('#tests-btn').mouseenter(function () {
            $('#detail-slider').css('left', '-842px');
            $('.nav-hover').css({'width': tests, 'left': knowhow2 + 26});
            selectMenuHoverClass('#tests-btn a');
        });
        $('#software-btn').mouseenter(function () {
            $('#detail-slider').css('left', '-1684px');
            $('.nav-hover').css({'width': software, 'left': knowhow2 + tests2 + 26});
            selectMenuHoverClass('#software-btn a');
        });

        $('#kontakt-btn').mouseenter(function () {
            $('#detail-slider').css('left', '-2526px');
            $('.nav-hover').css({'width': kontakt, 'left': knowhow2 + tests2 + software + 342});
            selectMenuHoverClass('#kontakt-btn a');
        });

        $('.nav-container.en #kontakt-btn').mouseenter(function () {
            $('#detail-slider').css('left', '-2526px');
            $('.nav-hover').css({'width': contact, 'left': knowhow2 + tests2 + software + 352});
            selectMenuHoverClass('.nav-container.en #kontakt-btn a');
        });



        <?php if($knowActive) { ?>
        $('.nav-container').mouseleave(function () {
            $('#detail-slider').css('left', '0');
            $('.nav-hover').css({'width': knowhow, 'left': '26px'});
            selectMenuHoverClass('#know-how-btn a');
        });
        <?php } ?>
        <?php if($testsActive) { ?>
        $('.nav-container').mouseleave(function () {
            $('#detail-slider').css('left', '-842px');
            $('.nav-hover').css({'width': tests, 'left': knowhow2 + 26});
            selectMenuHoverClass('#tests-btn a');
        });
        <?php } ?>
        <?php if($softwareActive) { ?>
        $('.nav-container').mouseleave(function () {
            $('#detail-slider').css('left', '-1684px');
            $('.nav-hover').css({'width': software, 'left': knowhow2 + tests2 + 26});
            selectMenuHoverClass('#software-btn a');
        });
        <?php } ?>
        <?php if($kontaktActive) { ?>
        $('.nav-container').mouseleave(function () {
            $('#detail-slider').css('left', '-2526px');
            $('.nav-hover').css({'width': kontakt, 'left': knowhow2 + tests2 + software + 342});
            selectMenuHoverClass('#kontakt-btn a');
        });

        <?php } ?>
        <?php if($kontaktActive) { ?>
        $('.nav-container.en #kontakt-btn').mouseenter(function () {
            $('#detail-slider').css('left', '-2526px');
            $('.nav-hover').css({'width': kontak, 'left': knowhow2 + tests2 + software + 352});
            selectMenuHoverClass('.nav-container.en #kontakt-btn a');
        });

        <?php } ?>

        <?php if($general) { ?>
        $('.nav-container').mouseleave(function () {
            $('#detail-slider').css('left', '-2526px');
            $('.nav-hover').css({'width': '0', 'left': '0'});
            selectMenuHoverClass('none');
        });
        <?php } ?>
    });
    </script>

<?php } ?>


<?php if ($navLang == 'de' || $navLang == 'en' || $navLang == 'bewerbung') {
    if ($homeAnimation) {
        ?>
        <header class="nav-container">
            <nav class="nav-over <?php if ($general) {
                echo $general;
            } else {
                echo $path[2];
            } ?>">
                <ul>
                    <?php if ($navLang == 'de' || $navLang == 'bewerbung') { ?>
                        <li id="know-how-btn"><a href="/de/knowhow/"><strong>HR</strong> Know How</a></li>
                        <li id="tests-btn"><a href="/de/tests/"><strong>HR</strong> Tests</a></li>
                        <li id="software-btn"><a href="/de/software/"><strong>HR</strong> Software</a></li>
                        <li id="kontakt-btn"><a href="/de/kontakt/">Kontakt</a></li>
                    <?php } elseif ($navLang == 'en') { ?>
                        <li id="know-how-btn"><a href="/en/expertise/"><strong>HR</strong> Expertise</a></li>
                        <li id="tests-btn"><a href="/en/tests/"><strong>HR</strong> Tests</a></li>
                        <li id="software-btn"><a href="/en/software/"><strong>HR</strong> Software</a></li>
                        <li id="kontakt-btn"><a href="/en/contact/">Contact</a></li>

                    <?php } ?>
                </ul>
                <div class="nav-hover"></div>
            </nav>
            <div class="nav-detail-animation">
                <div class="bg-animation">
                    <div class="bg-ani-overlay"></div>
                    <div class="bg-ani-0"></div>
                    <div class="bg-ani-1"></div>
                    <div class="bg-ani-2"></div>
                    <div class="bg-ani-3"></div>
                    <div class="bg-ani-4"></div>
                    <div class="bg-ani-5"></div>
                    <div class="bg-ani-6"></div>
                    <div class="bg-ani-7"></div>
                    <div class="bg-ani-8"></div>
                    <div class="bg-ani-9"></div>
                </div>
                <div class="logo-animation"></div>
            </div>
        </header>
    <?php } elseif ($navLang == 'de') { ?>
        <header class="nav-container">
            <nav class="nav-over <?php if ($general) {
                echo $general;
            } else {
                echo $path[2];
            } ?>">
                <ul>
                    <li id="know-how-btn"><a href="/de/knowhow/"><strong>HR</strong> Know How</a></li>
                    <li id="tests-btn"><a href="/de/tests/"><strong>HR</strong> Tests</a></li>
                    <li id="software-btn"><a href="/de/software/"><strong>HR</strong> Software</a></li>
                    <li id="kontakt-btn"><a href="/de/kontakt/">Kontakt</a></li>

                </ul>
                <div class="nav-hover"></div>
            </nav>
            <div class="nav-detail">
                <div id="detail-slider" class="nav-detail-slider">
                    <div class="know-how-slide<?php echo $knowSub . $knowNum ?>">
                        <div class="bg-animation">
                            <div class="bg-ani-1"></div>
                            <div class="bg-ani-2"></div>
                            <div class="bg-ani-3"></div>
                            <div class="bg-ani-4"></div>
                            <div class="bg-ani-5"></div>
                            <div class="bg-ani-6"></div>
                            <div class="bg-ani-7"></div>
                            <div class="bg-ani-8"></div>
                        </div>
                        <div class="slide-inner text-center">
                            <div class="animation"></div>
                            <div class="nav-block-one">
                                <a class="<?php echo $knowSubNum1 ?>" href="/de/knowhow/erecruiting/">E-Recruiting</a>
                                <a class="<?php echo $knowSubNum2 ?>" href="/de/knowhow/personalauswahl/">Personalauswahl</a>
                                <a class="no-margin <?php echo $knowSubNum3 ?>" href="/de/knowhow/potenzialanalyse/">Potenzialanalyse</a>
                            </div>
                            <div class="nav-block-two">
                                <a class="<?php echo $knowSubNum4 ?>" href="/de/knowhow/leistungsbeurteilung/">Leistungsbeurteilung</a>
                                <a class="<?php echo $knowSubNum5 ?>" href="/de/knowhow/personalentwicklung/">Personalentwicklung</a>
                                <a class="no-margin <?php echo $knowSubNum6 ?>" href="/de/knowhow/jobmatching/">Jobmatching</a>
                            </div>
                            <div class="nav-block-three">
                                <a class="<?php echo $knowSubNum7 ?>" href="/de/knowhow/berufsprofiling/">Berufsprofiling</a>
                            </div>
                        </div>
                    </div>
                    <div class="tests-slide<?php echo $testsSub . $testsNum ?>">
                        <div class="slide-inner">
                            <div class="nav-block-one">
                                <a class="btn-nav<?php echo $testsSubNum1 ?>" href="/de/tests/testnavigator/">
                                    <span>Test-</span>
                                    <span class="second">Navigator</span>
                                    Testverfahren nach <br>Ihrem Bedarf kombinieren
                                </a>
                            </div>
                            <div class="nav-block-two">
                                <a class="btn-lg<?php echo $testsSubNum2 ?>" href="/de/tests/testmanufaktur/">
                                    <span>Test-</span>
                                    <span class="second">Manufaktur</span>
                                    Entwicklung spezifischer Testkonzepte
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="software-slide<?php echo $softwareSub . $softwareNum ?>">
                        <div class="slide-inner">
                            <div class="nav-block-one">
                                <div class="job-matcher"></div>
                                <div class="wrap">
                                    <div class="green">
                                        <a class="<?php echo $softwareSubNum1 ?>"
                                           href="/de/software/jobmatcher/ssp_testplattform/">
                                            <span>SSP</span>
                                            Testplattform
                                        </a>
                                    </div>
                                    <div class="green no-margin">
                                        <a class="<?php echo $softwareSubNum2 ?>"
                                           href="/de/software/jobmatcher/erecruiting/">
                                            <span>S / M / XL</span>
                                            integriertes<br>
                                            E-Recruiting
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="nav-block-two">
                                <div class="white">
                                    <a class="<?php echo $softwareSubNum3 ?>"
                                       href="/de/software/produktivitaetswerkzeuge/">
                                        <em>Produktivitäts-</em>
                                        <em class="fix">werkzeuge</em>
                                        <small>z.B. AC-Planung,</small>
                                        <small>Jobmatching,</small>
                                        <small>Talentpools</small>
                                    </a>
                                </div>
                                <div class="wrap">
                                    <a class="<?php echo $softwareSubNum4 ?>" href="/de/software/integration/">Integration</a>
                                    <a class="<?php echo $softwareSubNum5 ?>"
                                       href="/de/software/sicherheit/">Sicherheit</a>
                                    <a class="<?php echo $softwareSubNum6 ?>" href="/de/software/service/">Service</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="general-slide"></div>
                </div>
            </div>
        </header>
    <?php } elseif ($navLang == 'en') { ?>
        <header class="nav-container en">
            <nav class="nav-over <?php if ($general) {
                echo $general;
            } else {
                echo $path[2];
            } ?>">
                <ul>
                    <li id="know-how-btn"><a href="/en/expertise/"><strong>HR</strong> Expertise</a></li>
                    <li id="tests-btn"><a href="/en/tests/"><strong>HR</strong> Tests</a></li>
                    <li id="software-btn"><a href="/en/software/"><strong>HR</strong> Software</a></li>
                    <li id="kontakt-btn"><a href="/en/contact/">Contact</a></li>

                </ul>
                <div class="nav-hover"></div>
            </nav>
            <div class="nav-detail">
                <div id="detail-slider" class="nav-detail-slider">
                    <div class="know-how-slide<?php echo $knowSub . $knowNum ?>">
                        <div class="bg-animation">
                            <div class="bg-ani-1"></div>
                            <div class="bg-ani-2"></div>
                            <div class="bg-ani-3"></div>
                            <div class="bg-ani-4"></div>
                            <div class="bg-ani-5"></div>
                            <div class="bg-ani-6"></div>
                            <div class="bg-ani-7"></div>
                            <div class="bg-ani-8"></div>
                        </div>
                        <div class="slide-inner text-center">
                            <div class="animation"></div>
                            <div class="nav-block-one">
                                <a class="<?php echo $knowSubNum1 ?>" href="/en/expertise/erecruiting/">E-Recruiting</a>
                                <a class="<?php echo $knowSubNum2 ?>" href="/en/expertise/staff_selection/">Staff
                                    Selection</a>
                                <a class="no-margin <?php echo $knowSubNum3 ?>"
                                   href="/en/expertise/potential_analysis/">Potential Analysis</a>
                            </div>
                            <div class="nav-block-two">
                                <a class="<?php echo $knowSubNum4 ?>" href="/en/expertise/performance_appraisal/">Performance
                                    Appraisal</a>
                                <a class="<?php echo $knowSubNum5 ?>" href="/en/expertise/human_resource_development/">HR
                                    Development</a>
                                <a class="no-margin <?php echo $knowSubNum6 ?>" href="/en/expertise/job_matching/">Job
                                    Matching</a>
                            </div>
                            <div class="nav-block-three eng">
                                <a class="<?php echo $knowSubNum7 ?>" href="/en/expertise/job_profiling/">Job
                                    Profiling</a>
                                <a class="<?php echo $knowSubNum8 ?>" href="/en/expertise/background_screening/">Background
                                    Screening</a>
                            </div>
                        </div>
                    </div>
                    <div class="tests-slide<?php echo $testsSub . $testsNum ?>">
                        <div class="slide-inner">
                            <div class="nav-block-one">
                                <a class="btn-nav<?php echo $testsSubNum1 ?>" href="/en/tests/test_navigator/">
                                    <span>Test-</span>
                                    <span class="second">Navigator</span>
                                    Explore and combine <br>Online-Assessments to fit your need
                                </a>
                            </div>
                            <div class="nav-block-two">
                                <a class="btn-lg<?php echo $testsSubNum2 ?>" href="/en/tests/test_factory/">
                                    <span>Test-</span>
                                    <span class="second">Factory</span>
                                    We design specific assessments
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="software-slide<?php echo $softwareSub . $softwareNum ?>">
                        <div class="slide-inner">
                            <div class="nav-block-one">
                                <div class="job-matcher"></div>
                                <div class="wrap">
                                    <div class="green">
                                        <a class="<?php echo $softwareSubNum1 ?>" href="/en/software/jobmatcher/ssp/">
                                            <span>SSP</span>
                                            test platform
                                        </a>
                                    </div>
                                    <div class="green no-margin">
                                        <a class="<?php echo $softwareSubNum2 ?>"
                                           href="/en/software/jobmatcher/e_recruiting/">
                                            <span>S / M / XL</span>
                                            integrated <br>
                                            E-Recruiting
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="nav-block-two">
                                <div class="white">
                                    <a class="<?php echo $softwareSubNum3 ?>" href="/en/software/productivity_tools/">
                                        <em>Productivity</em>
                                        <em class="fix">tools</em>
                                        <small>Learn how our</small>
                                        <small>tools make your</small>
                                        <small>work easier</small>
                                    </a>
                                </div>
                                <div class="wrap">
                                    <a class="<?php echo $softwareSubNum4 ?>" href="/en/software/integration/">Integration</a>
                                    <a class="<?php echo $softwareSubNum5 ?>" href="/en/software/security/">Security</a>
                                    <a class="<?php echo $softwareSubNum6 ?>" href="/en/software/service/">Service</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="general-slide"></div>
                </div>
            </div>
        </header>
    <?php
    }
} ?>