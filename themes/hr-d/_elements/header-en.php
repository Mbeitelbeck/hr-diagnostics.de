<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="content-language" content="en" />
<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="pragm" content="no-cache" />
<meta name="copyright" content="team:orange GmbH - http://www.teamorange.de" />
<meta name="description" content="HR-Diagnostics global" />
<meta name="keywords" content="HR-Diagnostics global" />
<meta name="publisher" content="HR-Diagnostics global" />
<meta name="robots" content="index,follow" />

<link rel="shortcut icon" href="/themes/hr-d/favicon.ico" />

<link rel="stylesheet" href="/themes/hr-d/_styles/screen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/themes/hr-d/_styles/screen-en.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/themes/hr-d/_scripts/jquery/jquery.fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<?php 
	$array = explode ( '/', $c->getCollectionPath() );
	$chapter = $array[2]==""?"home":$array[2]; 
	$lang = $array[1];
	Loader::element('header_required');
	
	$_SESSION["lang"] = $lang;
?>

<script type="text/javascript" src="/themes/hr-d/_scripts/jquery/jquery.flash.js"></script>
<script type="text/javascript" src="/themes/hr-d/_scripts/jquery/jquery.fancybox/jquery.fancybox-1.2.1.pack.js"></script>
<script type="text/javascript" src="/themes/hr-d/_scripts/useCases.min.js"></script>
<?php if ($c->getCollectionHandle()=='en') { ?>
<script type="text/javascript" src="/themes/hr-d/_scripts/referenzen.js"></script>

<?php } ?>

<?php 
	if ($c->getCollectionHandle()=='en') { 
		$swf = 'home.swf';
	} else {
		$swf = 'main-navi.swf';
	}
?>
<script type="text/javascript">
//<![CDATA[
	$(document).ready(function(){
		$("a[href*='/tests/explorer-en/']").click( function() {
			popupheight = screen.availHeight-200;			
			fenster = window.open($(this).attr("href"), "Navigator", "width=870,height="+popupheight+",status=no,scrollbars=yes,resizable=yes");
			fenster.focus();
			return false;
		});
		
		$("a.thickbox").fancybox();
		
		if ($.fn.flash.hasFlash.playerVersion().split(',')[0] >= 6) {
			$('#mainnavi').flash(
				{ 
					src: '/themes/hr-d/_flash-en/<?php echo $swf ?>',
					width: 920,
					height: <?php if ($c->getCollectionHandle()=='en') echo "335"; else echo "255"; ?>,
					wmode: 'transparent',
					flashvars: { chapter: '<?php echo $chapter ?>', page: '<?php echo $c->getCollectionHandle() ?>', stopvideo: <?php if (isset($_GET['hr'])) echo "true"; else echo "false"  ?>  }
				},
				{ version: <?php if ($c->getCollectionHandle()=='en') echo "9"; else echo "6"; ?> }
			);
		} else {
			switch ("<?php echo $chapter ?>") {
				case "home":
					navigation = 			'<img src="/themes/hr-d/_images/flashersatz/home-en.jpg" alt="HR Diagnostics" border="0" usemap="#home" />'
					navigation = navigation+'<map name="home" id="home">';
					navigation = navigation+'	<area shape="rect" coords="90,34,231,64" href="/en/expertise/" alt="Expertise" />';
					navigation = navigation+'	<area shape="rect" coords="232,34,362,63" href="/en/tests/" alt="Tests" />';
					navigation = navigation+'	<area shape="rect" coords="364,34,498,63" href="/en/software/" alt="Software" />';
					navigation = navigation+'</map>';
					break;
				case "expertise":
					navigation = 			'<img src="/themes/hr-d/_images/flashersatz/hr-know-how-en.jpg" alt="HR Diagnostics" border="0" usemap="#knowhow" />';
					navigation = navigation+'<map name="knowhow" id="knowhow">';
					navigation = navigation+'	<area shape="rect" coords="283,170,443,203" href="/en/expertise/job_profiling/" alt="Job Profiling" />';
					navigation = navigation+'	<area shape="rect" coords="465,98,620,127" href="/en/expertise/job_matching/" alt="Jobmatching" />';
					navigation = navigation+'	<area shape="rect" coords="283,98,443,131" href="/en/expertise/performance_appraisal/" alt="Performance Appraisal" />';
					navigation = navigation+'	<area shape="rect" coords="283,134,443,167" href="/en/expertise/human_resource_development/" alt="HR Development" />';
					navigation = navigation+'	<area shape="rect" coords="104,171,264,204" href="/en/expertise/potential_analysis/" alt="Potential Analysis" />';
					navigation = navigation+'	<area shape="rect" coords="104,135,264,168" href="/en/expertise/staff_selection/" alt="Staff selection" />';
					navigation = navigation+'	<area shape="rect" coords="465,137,621,166" href="/en/expertise/background_screening/" alt="Background Screening" />';
					navigation = navigation+'	<area shape="rect" coords="459,171,622,201" href="/en/expertise/sports_psychology/" alt="Sports Psychology" />';
					
					navigation = navigation+'	<area shape="rect" coords="90,34,231,64" href="/en/expertise/" alt="Know How" />';
					navigation = navigation+'	<area shape="rect" coords="232,34,362,63" href="/en/tests/" alt="Tests" />';
					navigation = navigation+'	<area shape="rect" coords="364,34,498,63" href="/en/software/" alt="Software" />';
					navigation = navigation+'	<area shape="rect" coords="105,98,265,131" href="/en/expertise/erecruiting/" alt="E-Recruiting" />';
					navigation = navigation+'</map>';
					break;
				case "tests":
					navigation = 			'<img src="/themes/hr-d/_images/flashersatz/hr-tests-en.jpg" alt="HR Diagnostics" border="0" usemap="#tests" />';
					navigation = navigation+'<map name="tests" id="tests">';
					navigation = navigation+'	<area shape="rect" coords="328,79,542,214" href="/en/tests/testmanufaktur/" />';
					navigation = navigation+'	<area shape="rect" coords="90,34,231,64" href="/en/expertise/" alt="Know How" />';
					navigation = navigation+'	<area shape="rect" coords="232,34,362,63" href="/en/tests/" alt="Tests" />';
					navigation = navigation+'	<area shape="rect" coords="364,34,498,63" href="/en/software/" alt="Software" />';
					navigation = navigation+'	<area shape="rect" coords="105,78,319,213" href="/en/tests/testnavigator/" alt="Testnavigator" />';
					navigation = navigation+'</map>';
					break;
				case "software":
					navigation = '<img src="/themes/hr-d/_images/flashersatz/hr-software-en.jpg" alt="HR Diagnostics" border="0" usemap="#home" />';
					navigation = navigation+'<map name="home" id="home">';
					navigation = navigation+'	<area shape="rect" coords="489,183,604,208" href="/en/software/service/" alt="Service" />';
					navigation = navigation+'	<area shape="rect" coords="489,155,604,180" href="/en/software/sicherheit/" alt="Sicherheit" />';
					navigation = navigation+'	<area shape="rect" coords="489,127,604,152" href="/en/software/integration/" alt="Integration" />';
					navigation = navigation+'	<area shape="rect" coords="365,127,480,209" href="/en/software/produktivitaetswerkzeuge/" alt="Produktivitaetswerkzeuge" />';
					navigation = navigation+'	<area shape="rect" coords="229,125,344,207" href="/en/software/jobmatcher/erecruiting" alt="S-M-XL" />';
					navigation = navigation+'	<area shape="rect" coords="90,34,231,64" href="/en/expertise/" alt="Know How" />';
					navigation = navigation+'	<area shape="rect" coords="232,34,362,63" href="/en/tests/" alt="Tests" />';
					navigation = navigation+'	<area shape="rect" coords="364,34,498,63" href="/en/software/" target="/en/software/" alt="Software" />';
					navigation = navigation+'	<area shape="rect" coords="105,125,220,207" href="/en/software/jobmatcher/ssp_testplattform" alt="SSP" />';
					navigation = navigation+'</map>';
					break;
				default:
					navigation = '<img src="/themes/hr-d/_images/flashersatz/standard.jpg" alt="HR Diagnostics" border="0" usemap="#standard" />';
					navigation = navigation+'<map name="standard" id="standard">';
					navigation = navigation+'	<area shape="rect" coords="90,34,231,64" href="/en/expertise/" alt="Know How" />';
					navigation = navigation+'	<area shape="rect" coords="232,34,362,63" href="/en/tests/" alt="Tests" />';
					navigation = navigation+'	<area shape="rect" coords="364,34,498,63" href="/en/software/" alt="Software" />';
					navigation = navigation+'</map>'; 
			}
			$('#mainnavi').html(navigation);
		}
		if ($.fn.flash.hasFlash.playerVersion().split(',')[0] >= 7) {
			$('h1').flash(
				{ 
					src: '/themes/hr-d/_flash/hrd-headline.swf',
					wmode: 'transparent',
					flashvars: { 
						css: [
							'* { color: #85bb16; }',
							'em { color: #676767; display:inline; }'
						].join(' ')
					}
				},
				{ version: 7 },
				function(htmlOptions) {
					htmlOptions.flashvars.txt = this.innerHTML;
					this.innerHTML = "<"+"div>"+this.innerHTML+"</"+"div>";
					var $alt = $(this.firstChild);
					htmlOptions.height = $alt.height();
					htmlOptions.width = $alt.width();
					$alt.addClass('alt');
					$(this)
						.addClass('flash-replaced')
						.prepend($.fn.flash.transform(htmlOptions));						
				}
			);
			$('h2.flashheadline').flash(
				{ 
					src: '/themes/hr-d/_flash/hrd-headline.swf',
					wmode: 'transparent',
					flashvars: { 
						css: [
							'* { color: #85bb16; }',
							'em { color: #676767; display:inline; }'
						].join(' ')
					}
				},
				{ version: 7 },
				function(htmlOptions) {
					htmlOptions.flashvars.txt = this.innerHTML;
					this.innerHTML = "<"+"div>"+this.innerHTML+"</"+"div>";
					var $alt = $(this.firstChild);
					htmlOptions.height = $alt.height()+5;
					htmlOptions.width = $alt.width();
					$alt.addClass('alt');
					$(this)
						.addClass('flash-replaced')
						.prepend($.fn.flash.transform(htmlOptions));						
				}
			);
		} else {
			$("#noscriptfallback").show();
		}	
	});


</script>


<?php
$DoNotTrackHeader = "DNT";
$DoNotTrackValue = "1";

$phpHeader = "HTTP_" . strtoupper(str_replace("-", "_", $DoNotTrackHeader));

if((!array_key_exists($phpHeader, $_SERVER)) OR ($_SERVER[$phpHeader] !== $DoNotTrackValue)) :
?>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try
{var pageTracker = _gat._getTracker("UA-5444313-1");
_gat._anonymizeIp();
pageTracker._trackPageview();
} catch(err) {}
</script>

<?php endif ?>

</head>
<body id="<?php echo $c->getCollectionHandle() ?>" class="<?php echo $chapter ?>">
<?php
echo '<script type="application/javascript">';
echo 'var locale = "'.LOCALE.'";';
echo 'console.log(locale);';
echo '</script>';
?>
	<div id="stage">
		<div id="page">
			<div class="page-top"></div>
			<div class="page-middle">
				<div id="header">
					<?php 
						$bt_main = BlockType::getByHandle('autonav');
						$bt_main->controller->displayPages = 'custom'; 
						$bt_main->controller->displayPagesCID = 98;
						$bt_main->controller->orderBy = 'display_asc';                    
						$bt_main->controller->displaySubPages = 'all';                    
						$bt_main->controller->displaySubPageLevels = 'all';                    
						$bt_main->render('templates/top_menu');
					?> 
					<a href="/en/?hr" id="logo"><img src="/themes/hr-d/_images/hr-diagnostics-logo.jpg" width="247px" height="82px" alt="HR-Diagnostics" /></a>
				</div>
				<div id="mainnavi">
					<div id="noscriptfallback">
					<?php 
						$bt_main = BlockType::getByHandle('autonav');
						$bt_main->controller->displayPages = 'custom';
						$bt_main->controller->displayPagesCID = 98;
						$bt_main->controller->orderBy = 'display_asc';                    
						$bt_main->controller->displaySubPages = 'relevant';                    
						$bt_main->controller->displaySubPageLevels = 'enough_plus1';                    
						$bt_main->render('templates/main_menu');
					?>
					<div class="footer"></div>
					</div>
					<script type="text/javascript">document.getElementById('noscriptfallback').style.display = "none";</script>				
</div>
			
		