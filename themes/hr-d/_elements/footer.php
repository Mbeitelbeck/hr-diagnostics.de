<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
				<div id="footer">
					<?php
                        $array = explode ( '/', $c->getCollectionPath() );
                        $chapter = $array[2]==""?"home":$array[2];
                        $lang = $array[1];
                        $_SESSION["lang"] = $lang;

                        $request = parse_url($_SERVER['REQUEST_URI']);
                        $path = explode('/', $request["path"]);

                        if ($lang == 'de' || $path[1] == 'de') {
                            $pagesID = 61;
                        } else {
                            $pagesID = 98;
                        }
						$bt_main = BlockType::getByHandle('autonav');
						$bt_main->controller->displayPages = 'custom';
						$bt_main->controller->displayPagesCID = $pagesID;
						$bt_main->controller->orderBy = 'display_asc';                    
						$bt_main->controller->displaySubPages = 'all';                    
						$bt_main->controller->displaySubPageLevels = 'all';                    
						$bt_main->render('templates/footer_menu');
					?> 
					<div id="footerbar">
						<!--<a href="http://www.teamorange.de">&copy; teamorange 2009</a>-->
					</div>
				</div>
			</div>
			<div class="page-bottom"></div>
		</div>
	</div>
    <?php if ($path[1] == 'de') { ?>
    <script type="text/javascript">
        $(function() {
            $(".useCasesWrapper").useCases();
        });
    </script>
    <?php } ?>

    <script type="text/javascript" src="/themes/hr-d/_scripts/jquery/jquery.fancybox/jquery.fancybox-1.2.1.pack.js"></script>
    <script type="text/javascript" src="/themes/hr-d/_scripts/useCases.min.js"></script>
    <?php if ($c->getCollectionHandle()=='de') { ?>
    <script type="text/javascript" src="/themes/hr-d/_scripts/referenzen.js"></script>
    <?php } ?>


    <script type="text/javascript">
        $(document).ready(function(){
            $("a[href*='/tests/explorer/']").click( function() {
                popupheight = screen.availHeight-200;
                fenster = window.open($(this).attr("href"), "Navigator", "width=870,height="+popupheight+",status=no,scrollbars=yes,resizable=yes");
                fenster.focus();
                return false;
            });

            $("a.thickbox").fancybox();
        });
    </script>


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-5444313-1', 'auto');
    ga('require', 'displayfeatures');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');

</script>



<?php  Loader::element('footer_required'); ?>
</body>
</html>