<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
				<div id="footer">
					<?php 
						$bt_main = BlockType::getByHandle('autonav');
						$bt_main->controller->displayPages = 'custom';
						$bt_main->controller->displayPagesCID = 98;
						$bt_main->controller->orderBy = 'display_asc';                    
						$bt_main->controller->displaySubPages = 'all';                    
						$bt_main->controller->displaySubPageLevels = 'all';                    
						$bt_main->render('templates/footer_menu');
					?> 
					<div id="footerbar">
						<!--<a href="http://www.teamorange.de">&copy; teamorange 2009</a>-->
					</div>
				</div>
			</div>
			<div class="page-bottom">
			</div>
		</div>
	</div>
<?php  Loader::element('footer_required'); ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-5444313-1', 'auto');
    ga('require', 'displayfeatures');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');

</script>
</body>
</html>