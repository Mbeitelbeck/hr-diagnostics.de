<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
	<div id="stage">
		<div id="page">
			<div class="page-top"></div>
			<div class="page-middle">
				<div id="header">
					<?php 
						$bt_main = BlockType::getByHandle('autonav');
						$bt_main->controller->displayPages = 'custom';
						$bt_main->controller->displayPagesCID = 61;
						$bt_main->controller->orderBy = 'display_asc';                    
						$bt_main->controller->displaySubPages = 'all';                    
						$bt_main->controller->displaySubPageLevels = 'all';                    
						$bt_main->render('templates/top_menu');
					?> 
					<a href="/de/?hr" id="logo"><img src="/themes/hr-d/_images/hr-diagnostics-logo.jpg" alt="HR-Diagnostics" /></a>
				</div>
				<div id="mainnavi">
					<div id="noscriptfallback">
					<?php 
						$bt_main = BlockType::getByHandle('autonav');
						$bt_main->controller->displayPages = 'custom';
						$bt_main->controller->displayPagesCID = 61;
						$bt_main->controller->orderBy = 'display_asc';                    
						$bt_main->controller->displaySubPages = 'relevant';                    
						$bt_main->controller->displaySubPageLevels = 'enough_plus1';                    
						$bt_main->render('templates/main_menu');
					?>
					<div class="footer"></div>
					</div>
					<script type="text/javascript">document.getElementById('noscriptfallback').style.display = "none";</script>				
				</div>
			
		