<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('_elements/shop/header.php');

class Suche {

    public function getDimensionen($typ) {
        $db = Loader::db();
        $rs = $db->GetAll("
            SELECT `akID`
            FROM `AttributeKeys`
            WHERE `akHandle` = ?
            ", array($typ)
        );
        $result = $db->GetAll("
            SELECT `akID`, `value`
            FROM `atSelectOptions`
            WHERE `akID` = ?
            ORDER BY `displayOrder` ASC
            ", array($rs[0]['akID'])
        );

        foreach ($result as $begriff) {
            foreach ($begriff as $name => $typ) {
                if ($name == 'akID') {
                    continue;
                }

                $typs[] = $typ;
            }
        }
        return $typs;
    }

    public function getCurrentUrl() {
        $currentPage = Page::getCurrentPage();
        $nh = Loader::helper('navigation');
        return $nh->getLinkToCollection($currentPage, true);
    }


}

$obj = new Suche();
$dimensionenSet['Leistung'] = $obj->getDimensionen('leistung');
$dimensionenSet['Persönlichkeit'] = $obj->getDimensionen('persoenlichkeit');
$dimensionenSet['Verhalten'] = $obj->getDimensionen('verhalten');
$dimensionenSet['Interessen'] = $obj->getDimensionen('interessen');


$oberDimensionen = array(
    'Interessen'     => '/themes/hr-d/_images/test/messbereich_i.png',
    'Leistung'       => '/themes/hr-d/_images/test/messbereich_l.png',
    'Verhalten'      => '/themes/hr-d/_images/test/messbereich_v.png',
    'Persönlichkeit' => '/themes/hr-d/_images/test/messbereich_p.png',
    );

?>

<div id="content">
    <div id="testheader"></div>
    <div class="footer"></div>

    <div id="aside">
        <div class="hr-top">
            <hr>
        </div>
        <div id="produktbox-mini" class="diagonal-box">
            <div id="filtertool" class="content" style="float:left;">
                <p>
                    <img width="212" height="58" alt="heading_shop_box2.jpg"
                         src="/files/1213/4459/3140/filtertool_headline.jpg">
                </p>

                <?php foreach($dimensionenSet as $header => $set) : ?>

                <h2>

                    <div class="messbereich-background">
                        <img
                            class="dimension-<?php echo $header[0] ?>"
                            src="<?php echo $oberDimensionen[$header] ?>"
                            title="<?php echo $header ?>"
                            alt="<?php echo $header ?>"
                        />
                    </div><!-- /messbereich-background -->
                    <span class="dimension-header">
                        <?php echo $header ?>
                    </span>
                </h2>
                <ul>
                <?php foreach ($set as $key => $value) : ?>
                   <li>
                       <a
                            href="<?php echo $obj->getCurrentUrl() ?>?search=<?php echo $value ?>"
                            class="ajax-search <?php if (isset($_GET['search']) AND $value == $_GET['search']) : ?>
                                active
                            <?php endif ?>"
                       >
                            <?php echo $value ?>
                       </a>
                    </li>
                <?php endforeach // set ?>
                </ul>
                <?php endforeach //dimensionenSet ?>

            </div><!-- /filtertool -->

        </div><!-- /produktbox-mini -->
        <div class="footer"></div>

        <?php
        $a = new Area('RechteSpalte');
        $a->display($c);
        ?>
    </div><!-- /aside -->

    <div id="section">
        <?php
        if (isset($_GET['search'])) :
            $get = $_GET['search'];
        ?>
        <h2>
            Folgende Tests enthalten die Dimension:
            <span style="display: inline; color: #000; font-size: 13px;">
                <?php echo $get ?>
            </span>
        </h2>

        <?php
        $c = Page::getCurrentPage();
        $db = Loader::db();
        $sql = "
            SELECT mb.bID
            FROM btTOApp mb
                INNER JOIN Blocks b
                    ON mb.bID = b.bID
                INNER JOIN CollectionVersionBlocks cvb
                    ON cvb.bID = b.bID
                INNER JOIN CollectionVersions cv
                    ON cv.cvID = cvb.cvID
                    AND cv.cID = cvb.cID
            WHERE cv.cvIsApproved = 1
                AND b.bIsActive = 1
                AND mb.dimensionen like ?
                AND mb.testtyp = ?
            ORDER BY mb.preis ASC
        ";

        $param = array("%${get}%", 1);
        $r = $db->query($sql, $param);

        $batterie = array();
        $einzel = array();

        while ($row = $r->fetchRow()) {
            $batterie[] = $row['bID'];
        }

        $param = array("%${get}%", 0);
        $r = $db->query($sql, $param);

        while ($row = $r->fetchRow()) {
            $einzel[] = $row['bID'];
        }

        // Ausgabe der Batterien
        foreach ($batterie as $bID) :
            $b = Block::getByID($bID);
            $controller = $b->getInstance();
            //Set Vars

            $title = $controller->title;
            $kurzbeschreibung = $controller->kurzbeschreibung;
            $content = $controller->content;
            $detailpage = $controller->detailpage;
            $fID = $controller->fID;
            $preis = $controller->preis;
            $link = $controller->link;
            $bestellen = $controller->bestellen;
            $dimensionen = $controller->dimensionen;
            $features = $controller->features;

            $p = Page::getByID($link);
            $pagePath = $p->getCollectionPath();

            $file = File::getByID($fID);
            $path = $file->getPath();
            $size = getimagesize($path);
            $imagePath = File::getRelativePathFromID($fID);

            if (strpos($dimensionen, ",") !== false) {
                $dimensionen = explode(',', $dimensionen);
            }
            else {
                $dimensionen = array($dimensionen);
            }

            ?>

            <div class="app-box-batterie">
                <div class="device-icons">
                <!-- hier mehrteiligen feature-icons -->
                    <div class="messbereich-background">
                    <?php foreach($dimensionenSet as $group => $set) : ?>
                            <?php foreach($dimensionen as $dim) : ?>
                                <?php if (in_array($dim, $set)) : ?>
                                    <img
                                        class="dimension-<?php echo $group[0] ?>"
                                        src="<?php echo $oberDimensionen[$group] ?>"
                                        title="<?php echo $group ?>"
                                        alt="<?php echo $group ?>"
                                    />
                                <?php continue 2;/* hier wollen wir ins naechste Set springen */ endif ?>
                            <?php endforeach // dimensionen ?>
                    <?php endforeach // dimensionenSet ?>
                    </div><!-- /messbereich-background -->
                <?php if (preg_match('/Tablets/', $features) === 1) : ?>
                    <img
                        src="/themes/hr-d/_images/shop/icon_mobile.png"
                        width="27" height="27"
                        alt="Bearbeitung auf iPads und Tablets"
                        title="Bearbeitung auf iPads und Tablets"
                    />
                <?php endif ?>
                <?php if (preg_match('/ungesch/', $features) === 1) : ?>
                    <img
                        src="/themes/hr-d/_images/shop/icon_office.png"
                        width="27" height="27"
                        alt="Test zur ungesch&uuml;tzten Bearbeitung"
                        title="Test zur ungesch&uuml;tzten Bearbeitung"
                    />
                <?php endif ?>
                </div><!-- /device-icons -->
                <h3>
                    <a href="<?php echo $pagePath ?>">
                        <?php echo $title ?>
                    </a>
                </h3>
                <p class="desc">
                    <a href="<?php echo $pagePath ?>">
                        <?php echo $kurzbeschreibung; ?>
                    </a>
                </p>
                <div class="info">
                    <div class="icon">
                    <?php if ($fID != 0 && $fID != "") : ?>
                        <a href="<?php echo $pagePath ?>">
                            <img <?php echo $class ?>
                                src="<?php echo $imagePath ?>"
                                alt="<?php echo $title ?>"
                            />
                        </a>
                    <?php endif ?>
                    </div><!-- /icon -->

                    <div class="list">
                        <?php echo $content ?>
                        <div class="footer"></div>
                        <?php if ($pagePath != "") : ?>
                        <a
                            title="<?php echo $title ?>"
                            href="<?php echo $pagePath ?>"
                            class="info-link"
                        >
                            Mehr Information
                        </a>
                        <?php endif ?>
                        <div class="footer"></div>
                        <div class="action">
                            <p class="price">
                                <?php echo $preis ?> &euro;
                                <a class="buy" href="/de/authentifizierung">
                                    <span>bestellen</span>
                                </a>
                            </p>
                        </div><!-- /action -->
                        <div class="footer"></div>
                    </div><!-- /list -->

                    <div class="dimension">
                        <p style="font-size:11px; color:#363636;">
                            <strong>Dimensionen:</strong>
                        </p>
                        <ul>
                        <?php foreach ($dimensionen as $array => $key) : ?>
                            <li style="list-style: none;">
                                <a
                                    title="<?php echo $key ?>"
                                    href="/de/test-nach-dimensionen-finden/?search=<?php echo $key ?>"
                                >
                                    <?php echo $key ?>
                                </a>
                            </li>
                        <?php endforeach ?>
                        </ul>
                    </div><!-- /dimension -->
                    <div class="footer"></div>
                </div><!-- /info -->
            </div><!-- /app-box-batterie -->
            <?php endforeach  // batterie ?>

            <?  // Ausgabe der Einzelteste
            $i = 0;
            foreach ($einzel as $bID) :
                $i++; //even or odd

                $b = Block::getByID($bID);
                $controller = $b->getInstance();
                //Set Vars

                $title = $controller->title;
                $kurzbeschreibung = $controller->kurzbeschreibung;
                $content = $controller->content;
                $detailpage = $controller->detailpage;
                $fID = $controller->fID;
                $preis = $controller->preis;
                $link = $controller->link;
                $bestellen = $controller->bestellen;
                $dimensionen = $controller->dimensionen;
                $features = $controller->features;

                $p = Page::getByID($link);
                $pagePath = $p->getCollectionPath();

                $file = File::getByID($fID);
                $path = $file->getPath();
                $size = getimagesize($path);
                $imagePath = File::getRelativePathFromID($fID);

                if (strpos($dimensionen, ",") !== false) {
                    $dimensionen = explode(',', $dimensionen);
                }
                else {
                    $dimensionen = array($dimensionen);
                }
                ?>

                <div class="app-box"
                <?php if ($i % 2 == 0) : ?>
                    style="border-left:1px dotted #999999;"
                <?php endif ?>
                >
                    <div class="device-icons">
                    <!-- hier die mehrteiligen feature-icons -->
                    <div class="messbereich-background">
                    <?php foreach($dimensionenSet as $group => $set) : ?>
                            <?php foreach($dimensionen as $dim) : ?>
                                <?php if (in_array($dim, $set)) : ?>
                                    <img
                                        class="dimension-<?php echo $group[0] ?>"
                                        src="<?php echo $oberDimensionen[$group] ?>"
                                        title="<?php echo $group ?>"
                                    />
                                <?php continue 2; endif ?>
                            <?php endforeach // dimensionen ?>
                    <?php endforeach // dimensionenSet ?>
                    </div>
                    <?php if (preg_match('/Tablets/', $features) === 1) : ?>
                        <img
                            src="/themes/hr-d/_images/shop/icon_mobile.png"
                            width="27" height="27"
                            alt="Bearbeitung auf iPads und Tablets"
                            title="Bearbeitung auf iPads und Tablets"
                        />
                    <?php endif ?>
                    <?php if (preg_match('/ungesch/', $features) === 1) : ?>
                        <img
                            src="/themes/hr-d/_images/shop/icon_office.png"
                            width="27" height="27"
                            alt="Test zur ungesch&uuml;tzten Bearbeitung"
                            title="Test zur ungesch&uuml;tzten Bearbeitung"
                        />
                    <?php endif ?>
                    </div><!-- /device-icons -->

                    <h3>
                        <a href="<?php echo $pagePath ?>">
                            <?php echo $title ?>
                        </a>
                    </h3>

                    <p class="desc">
                        <a href="<?php echo $pagePath ?>">
                            <?php echo $kurzbeschreibung ?>
                        </a>
                    </p>

                    <div class="info">
                        <div class="icon">
                        <?php if ($fID != 0 && $fID != "") : ?>
                            <a href="<?php echo $pagePath ?>">
                                <img <?php echo $class ?>
                                    src="<?php echo $imagePath ?>"
                                    alt="<?php echo $title ?>"
                                />
                            </a>
                        <?php endif ?>
                        </div><!-- /icon -->

                        <div class="list">
                            <?php echo $content ?>
                            <div class="dimension">
                                <ul>
                                    <strong style="color:#363636; float:left; padding-right:8px;">Dimensionen:</strong>

                                    <?php foreach ($dimensionen as $array => $key) : ?>
                                    <?php if ($array == 0) : ?>
                                    <li style="list-style: none;">
                                        <a
                                            title="<?php echo $key ?>"
                                            href="/de/test-nach-dimensionen-finden/?search=<?php echo $key ?>"
                                        >
                                        <?php echo $key ?>
                                        </a>
                                    </li>
                                    <?php elseif ($array == (count($dimensionen) - 1)) : ?>
                                    <li style="list-style: none;">
                                        <a
                                            title="<?php echo $key ?>"
                                            href="/de/test-nach-dimensionen-finden/?search=<?php echo $key ?>"
                                        >
                                        <?php echo $key ?>
                                        </a>
                                    </li>
                                    <?php else : ?>
                                    <li style="list-style: none;">
                                        <a
                                            title="<?php echo $key ?>"
                                            href="/de/test-nach-dimensionen-finden/?search=<?php echo $key ?>"
                                        >
                                        <?php echo $key ?>
                                        </a>,
                                    </li>
                                    <?php endif ?>
                                    <?php endforeach ?>
                                </ul>
                            </div><!-- /dimension -->

                            <div class="action">
                                <?php if ($pagePath != "") : ?>
                                <a
                                    title="<?php echo $title ?>"
                                    href="<?php echo $pagePath ?>"
                                    class="info-link"
                                >
                                    Mehr Information
                                </a>
                                <?php endif ?>

                                <p class="price">
                                    <?php echo $preis ?> &euro;
                                    <a class="buy" href="/de/authentifizierung">
                                        <span>bestellen</span>
                                    </a>
                                </p>
                            </div><!-- /action -->

                            <div class="footer"></div>

                        </div><!-- /list -->
                    </div><!-- /info -->
                </div><!-- /app-box -->

            <?php endforeach //einzel ?>

        <?php endif // $_GET['search'] ?>
        <div class="footer"></div>

        <?php
        $a = new Area('Endzeile');
        $a->display($c);
        ?>

    </div><!-- /section -->
    <div class="footer"></div>

</div><!-- /content -->
<?php $this->inc('_elements/shop/footer.php'); ?>
