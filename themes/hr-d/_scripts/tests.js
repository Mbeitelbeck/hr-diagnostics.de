function saveMinHeight() {
	$('body').height(Math.max($('body').height(),minHeight));
}

function toggleUnused() {
	$("ul#testliste li > h2").each(function(){
		$("ul > li.test:visible",this.parentNode).length == 0 ? $(this.parentNode).slideUp(300) : $(this.parentNode).slideDown(300);
	});
	$("ul#testliste > li > h1").each(function(){
		$(this.parentNode).show();
		$("li.test:visible",this.parentNode).length == 0 ? $(this.parentNode).hide() : '';
	});
	//saveMinHeight();
}

function ajupdate() {
	var tmp1 = $("#persAuswahlOnly")[0].checked ? '1' : '0';
	var tmp2 = $("#persEntwOnly")[0].checked ? '1' : '0';
	var tmp3 = $("#ungBearbOnly")[0].checked ? '1' : '0';
	var checkbox = $("#catfilter :checkbox:checked").val();

	($("#search").val()!="" || tmp1==1 || tmp2==1 || tmp3==1 || checkbox > 0) ? $("h5#filterstatus").show() :  $("h5#filterstatus").hide();
		
	$.getJSON(
		"/single_pages/tests.ajax.php",
		{ q: $("#search").val(), a:tmp1, e:tmp2, u:tmp3, c:checkbox },
		function(data){
			//console.log(data);
			d = data.RES.DATA;
			cols = data.RES.COLUMNS;
			var referrals = {"e": "empfohlen", "b": "bedempfohlen", "j": "jeweiligeentwicklung", "0": ""};
			var countAll = 0;
			var countVisible = 0;
//			console.log(d);
			for (var i = 0; i<d.length; i++) {
				
				$("#test"+d[i][0]+" h3 span.referral").attr("class","").addClass("referral");
				$("#test"+d[i][0]+" h3 span.referral").addClass(referrals[d[i][2]]);

				countAll++
				if (d[i][1]==1) countVisible++;
				d[i][1]==1 ? $("#test"+d[i][0]).slideDown(300) : $("#test"+d[i][0]).slideUp(300);
				var u = 2;
				for (var c = 2; c<cols.length; c++) {
					o = "#test"+d[i][0]+" ."+cols[c].toLowerCase();
					d[i][u]!=3 ? $(o).html('&diams;') : $(o).html('&nbsp;');
					u++;
				}
			}
			$("li.test").removeClass('open');
			
			if ($("#explorer-en").hasClass('tests')){ lang = " to "; } else{ lang = " von "; }
			$("h5#filterstatus span").html(countVisible+lang+countAll);
			if (checkbox) $(".legende-block-bottom").fadeIn();
			setTimeout(toggleUnused,310);
		}
	);
}

function ajsearch() {
	if (typeof(update) != 'undefined') clearTimeout(update);
	update = setTimeout(ajupdate,250);
}

function resetFilter() {
	tgs = new Object();
	tgs.tg1 = ({id:0,label:'',color:'#8ad0a8',inx:0});
	
	$("#search").val("");
	$("input:checkbox").each(function(){
		this.checked = false;
		$(this).removeAttr("disabled");
	});
	$("h3 span.icons span").hide();
	$("h5#filterstatus").hide();
	$(".legende-block-bottom").hide();
	ajupdate();
}

function selectTG(checkbox) {
	freeI = 'tg1';
	if(checkbox.checked) {
		tgs[freeI].id = checkbox.value;
		tgs[freeI].label = $('span',checkbox.parentNode).html();
		tgs[freeI].inx = freeI;
	} else {
		tgs[freeI].id=0;
		tgs[freeI].label='';
		tgs[freeI].inx = 0;
	}
}

$(document).ready(function(){	
	$("#search").keyup( function() {
		ajsearch();
	} );
	$("input:checkbox").click( function() {
		saveMinHeight();
		ajsearch();
	} );
	$("input.check:checkbox").click( function() {
		if($(this).attr("checked")){var wasChecked=true;}
		$("input.check:checked").removeAttr("checked");
		if(wasChecked==true){$(this).attr("checked","checked");}
		
		selectTG(this);
	} );
	$("li.test h3").click( function() {
		if ($(this.parentNode).attr('class').search(/open/) == -1) {
			$("li.test").removeClass('open');
			$(this.parentNode).toggleClass('open');
	
			$.browser.msie ? $("li.test .details").hide(0,saveMinHeight) : $("li.test .details").slideUp(200,saveMinHeight);
			$.browser.msie ? $('.details',this.parentNode).toggle(0,saveMinHeight) : $('.details',this.parentNode).slideToggle(200,saveMinHeight);
		} else {
			$(this.parentNode).toggleClass('open');
			$.browser.msie ? $('.details',this.parentNode).toggle(0,saveMinHeight) : $('.details',this.parentNode).slideToggle(200,saveMinHeight);
		}
	} );
	$("input#search").attr('autocomplete','off');
	minHeight = $('body').height();

	resetFilter();
});