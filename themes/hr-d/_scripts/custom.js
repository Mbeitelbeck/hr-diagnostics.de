//<![CDATA[
$(document).ready(function () {

// -------------------------------------------------------------------------
// - Kuemmert sich um die Hoehe der AppBlocks
// -------------------------------------------------------------------------
    setInfoHeight();

    function setInfoHeight() {
        //Kleine Darstellung
        var maxHeight = 0;
        $(".app-box:not(.big)").each(function (i) {
            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        });
        $(".app-box:not(.big)").height(maxHeight);
    }

    $("#showcase").awShowcase({
        content_width: 655,
        content_height: 250,
        fit_to_parent: false,
        auto: true,
        interval: 4000,
        continuous: true,
        loading: true,
        tooltip_width: 200,
        tooltip_icon_width: 32,
        tooltip_icon_height: 32,
        tooltip_offsetx: 18,
        tooltip_offsety: 0,
        arrows: true,
        buttons: false,
        btn_numbers: false,
        keybord_keys: false,
        mousetrace: false,
        /* Trace x and y coordinates for the mouse */
        pauseonover: false,
        stoponclick: false,
        transition: 'hslide',
        /* hslide / vslide / fade */
        transition_delay: 300,
        transition_speed: 500,
        show_caption: 'onload',
        /* onload / onhover / show */
        thumbnails: false,
        thumbnails_position: 'outside-last',
        /* outside-last / outside-first / inside-last / inside-first */
        thumbnails_direction: 'vertical',
        /* vertical / horizontal */
        thumbnails_slidex: 3,
        /* 0 = auto / 1 = slide one thumbnail / 2 = slide two thumbnails / etc. */
        dynamic_height: false,
        /* For dynamic height to work in webkit you need to set the width and height of images in the source. Usually works to only set the dimension of the first slide in the showcase. */
        speed_change: true,
        /* This prevents user from swithing more then one slide at once */
        viewline: true,
        /* If set to true content_width, thumbnails, transition and dynamic_height will be disabled. As for dynamic height you need to set the width and height of images in the source. */
        fullscreen_width_x: 15,
        custom_function: null
    });

    $('#carousel').infiniteCarousel({
        transitionSpeed: 1000,
        displayTime: 15000,
        displayProgressRing: false,
        thumbnailType: 'none',
        // buttons, images, numbers, count, or none
        easeLeft: 'easeInOutQuad',
        easeRight: 'easeInOutQuad',
        imagePath: '/themes/hr-d/_scripts/jquery/infinitecarousel/images/',
        inView: 1,
        margin: 0,
        advance: 1,
        showControls: false,
        autoHideCaptions: false,
        autoPilot: true,
        prevNextInternal: true,
        internalThumbnails: false,
        enableKeyboardNav: true
    });

    $("a.fancy").fancybox({
        'transitionIn': 'elastic',
        'transitionOut': 'elastic'
    });

    $(".bericht").fancybox({
        'width': '90%',
        'height': '90%',
        'autoScale': true,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'type': 'iframe'
    });

});


//TOOLTIP STEPS
$(document).ready(function () {
							
	$('span.agb a').live("click",function(){_gaq.push(['_trackEvent', 'HRD-Testverlag', 'Registrierung', 'AGB-PDF']);});						
							
							

    function t(lang, german, english) {
        if (lang === 'en_US') {
            return english;
        }
        else {
            return german;
        }
    }


    $("#uName,#uPassword").each(function (i) {
        if ($(this).val() !== "") {
            $(this).addClass("focus");
        }
    });

    $("#uName,#uPassword").focus(function () {
        if ($(this).val() === "") {
            $(this).addClass("focus");
        }
    });
    $("#uName,#uPassword").blur(function () {
        if ($(this).val() === "") {
            $(this).removeClass("focus");
        }
    });

    $("#anmelden").tooltip({
        // tweak the position
        offset: [177, 238],
        effect: 'fade'

        // add dynamic plugin with optional configuration for bottom edge
    }).dynamic({
            bottom: {
                direction: 'down',
                bounce: true
            }
        });

    $("#einloggen").tooltip({
        // tweak the position
        offset: [177, 118],
        effect: 'fade'

        // add dynamic plugin with optional configuration for bottom edge
    }).dynamic({
            bottom: {
                direction: 'down',
                bounce: true
            }
        });

    $("#bestellen").tooltip({
        // tweak the position
        offset: [177, -2],
        effect: 'fade'

        // add dynamic plugin with optional configuration for bottom edge
    }).dynamic({
            bottom: {
                direction: 'down',
                bounce: true
            }
        });

    $("#durchfuhren").tooltip({
        // tweak the position
        offset: [177, -122],
        effect: 'fade'

        // add dynamic plugin with optional configuration for bottom edge
    }).dynamic({
            bottom: {
                direction: 'down',
                bounce: true
            }
        });

    $("#entscheiden").tooltip({
        // tweak the position
        offset: [177, -242],
        effect: 'fade'

        // add dynamic plugin with optional configuration for bottom edge
    }).dynamic({
            bottom: {
                direction: 'down',
                bounce: true
            }
        });

    $("#steps .item a").mousemove(function () {
        $("#steps").css('background-position', '0px -' + (($("#steps a").get().indexOf(this) + 1) * 48) + 'px');
    });

    $("#steps .item").mouseleave(function () {
        $("#steps").css('background-position', '0px 0px');
    });

    $(".ic_right_nav img, .ic_left_nav img").hover(function () {
        $(this).animate({
            'opacity': 0.5
        });
    }, function () {
        $(this).animate({
            'opacity': 1
        });
    });


    $(".device-icons img[title]").tooltip({
            // tweak the position
            effect: 'fade',
            offset: [50, 0]
        }

    );


    //Registrierungsformular
    if ($('body#registrieren').length) {

        $('input[name="Question8"]').attr('placeholder', 'Name@Firma.de');

        var defaults = {
            show: {
                event: 'click focus'
            },
            hide: {
                event: 'blur unfocus'
            },
            overwrite: false,
            position: {
                my: 'left center',
                at: 'right center',
                viewport: $(window)
            },
            style: {
                classes: 'ui-tooltip-info'
            }
        };

        $('input[name="Question8"]').qtip($.extend(defaults, {
            content: {
                text: t(locale, 'Bitte haben Sie Verst&auml;ndnis daf&uuml;r, dass wir Sie nur dann akkreditieren ' +
                    'k&ouml;nnen, wenn die gewerbliche Nutzung unseres Angebots anhand Ihrer E-Mail-Adresse ' +
                    'nachvollzogen werden kann. <br />Sollten Sie keine E-Mail-Adresse mit einer Firmen-Domain ' +
                    'haben nehmen Sie bitte <a href="mailto:testverlag@hr-diagnostics.de?subject=E-Mail-Domain">' +
                    'Kontakt</a> zu uns auf.',
                    'Please understand that we can accredit you only if we can comprehend the commercial use ' +
                        'of our choice through your email address.<br />If you do not have an email address ' +
                        'with a company domain please <a ' +
                        'href="mailto:testverlag@hr-diagnostics.de?subject=E-Mail-Domain">contact us.</a>')
            }
        }));

        $('input[name="Question4"]').qtip($.extend(defaults, {
            content: {
                text: t(locale, 'Bitte geben Sie Ihre Firmendaten so ein, wie sie sp&auml;ter f&uuml;r Kommunikation ' +
                    'und Rechnungsstellung genutzt werden sollen, z.B. Firmenname GmbH & Co.KG.',
                    'Please enter your company data in a way we can use for communication and invoicing purposes ' +
                        '(for example, Company name, Inc.)')
            }
        }));


        $('input[name="Question34"]').qtip($.extend(defaults, {
            content: {
                text: t(locale, 'Wenn Sie einen Gutscheincode erhalten haben, geben Sie ihn bitte hier ein.' +
                    ' Die Gutschrift erfolgt automatisch im Zuge der Anmeldung.',
                    'If you have received a coupon code, please enter it here.' +
                        'The credit will be automatically at registration.')
            }
        }));

        $('input[name="Question35"]').qtip($.extend(defaults, {
            content: {
                text: t(locale, 'Wenn Sie einen Gutscheincode erhalten haben, geben Sie ihn bitte hier ein.' +
                    ' Die Gutschrift erfolgt automatisch im Zuge der Anmeldung.',
                    'If you have received a coupon code, please enter it here.' +
                        'The credit will be automatically at registration.')
            }
        }));

        $.validator.addMethod("corporateMail", function (value, element, arg) {
            var domains = ['web.de', 'hotmail', 'live', 't-online', 'gmx', 'yahoo', 'googlemail', 'gmail', 'arcor',
                'aol', '1und1', 'me.com', 'mail.ru', 'freenet', 'msn.com', 'bluewin.ch'];
            return this.optional(element) || !RegExp("@(" + domains.join('|') + ")").test(value);
        }, t(locale, 'Bitte geben Sie eine geschäftliche E-Mail-Adresse ein.', 'Please enter a commercial email address'));

        var myForm = $('form.miniSurveyView');

        myForm.validate({
            /*debug: true,*/
            meta: "validate",
            errorClass: "errormessage",
            onkeyup: false,
            errorClass: 'error',
            validClass: 'valid',
            rules: {
                Question1: { //Anrede
                    required: true
                },
                Question3: { //Vorname
                    required: true
                },
                Question32: { //Nachname
                    required: true
                },
                Question8: { //E-Mail
                    required: true,
                    email: true,
                    corporateMail: true
                },
                Question27: { //E-Mail2
                    equalTo: $('input[name="Question8"]')
                },
                Question4: { //Unternehmen
                    required: true
                },
                Question5: { //Adresse
                    required: true
                },
                Question6: { //Adresse2
                    required: true
                },
                Question7: { //Telefon
                    required: true
                },
                Question28_0: { //AGB
                    required: true
                },
                Question29_0: { //Datenschutz
                    required: true
                },

                ccmCaptchaCode: { //Captcha
                    required: true
                }
            },
            messages: {
                Question1: { //Anrede
                    required: t(locale, 'Bitte w&auml;hlen Sie eine Anrede aus.', 'Please select a title')
                },
                Question3: { //Vorname
                    required: t(locale, 'Bitte geben Sie Ihren Vornamen ein.', 'Please enter your first name')
                },
                Question32: { //Nachname
                    required: t(locale, 'Bitte geben Sie Ihren Nachnamen ein.', 'Please enter your last name.')
                },
                Question8: { //E-Mail
                    required: t(locale, 'Bitte geben Sie Ihre E-Mail-Adresse ein.',
                        'Please enter your email address.'),
                    email: t(locale, 'Bitte geben Sie Ihre E-Mail-Adresse ein.', 'Please enter your email address.'),
                    corporateMail: t(locale, 'Bitte haben Sie Verst&auml;ndnis daf&uuml;r, dass wir Sie nur dann ' +
                        'akkreditieren k&ouml;nnen, wenn die gewerbliche Nutzung unseres Angebots anhand ' +
                        'Ihrer E-Mail-Adresse nachvollzogen werden kann. <br />Sollten Sie keine ' +
                        'E-Mail-Adresse mit einer Firmen-Domain haben nehmen Sie bitte ' +
                        '<a href="mailto:testverlag@hr-diagnostics.de?subject=E-Mail-Domain">Kontakt</a> zu ' +
                        'uns auf.',
                        'Please understand that we can accredit you only if we can comprehend the commercial use ' +
                            'of our choice through your email address.<br />If you do not have an email address ' +
                            'with a company domain please <a ' +
                            'href="mailto:testverlag@hr-diagnostics.de?subject=E-Mail-Domain">contact us.</a>')
                },
                Question27: { //E-Mail2
                    equalTo: t(locale, 'Bitte best&auml;tigen Sie Ihre E-Mail-Adresse.',
                        'Please confirm your email address.')
                },
                Question4: { //Unternehmen
                    required: t(locale, 'Bitte geben Sie Ihre Firmendaten so ein, wie sie sp&auml;ter f&uuml;r ' +
                        'Kommunikation und Rechnungsstellung genutzt werden sollen, z.B. Firmenname GmbH & ' +
                        'Co.KG.',
                        'Please enter your company data in a way we can use for communication and invoicing ' +
                            'purposes (for example, Company name, Inc.)')
                },
                Question5: { //Adresse
                    required: t(locale, 'Bitte geben Sie Ihre Stra&szlig;e und Hausnummer ein.',
                        'Please enter your street address and number.')
                },
                Question6: { //Adresse2
                    required: t(locale, 'Bitte geben Sie Ihre Postleitzahl und Ort ein.',
                        'Please enter your ZIP code and City.')
                },
                Question7: { //Telefon
                    required: t(locale, 'Bitte geben Sie Ihre Telefonnummer ein.', 'Please enter your phone number.')
                },
                Question28_0: { //AGB
                    required: t(locale, 'Bitte stimmen Sie den AGB zu.', 'Please consent to the terms and conditions')
                },
                Question29_0: { //Datenschutz
                    required: t(locale, 'Bitte stimmen Sie der Datenschutzerkl&auml;rung zu.',
                        'Please consent to the data privacy statement.')
                },
                ccmCaptchaCode: { //Captcha
                    required: t(locale, 'Bitte geben Sie den Captcha ein.', 'Please enter the captcha.')
                }
            },
            errorPlacement: function (error, element) {
                // Check we have a valid error message
                if (!error.is(':empty')) {
                    // Apply the tooltip only if it isn't valid
                    $(element).filter(':not(.valid)').qtip({
                        overwrite: true,
                        content: error,
                        position: {
                            my: 'left center',
                            at: 'right center',
                            viewport: $(window)
                        },
                        show: {
                            event: false,
                            ready: true
                        },
                        hide: false,
                        style: {
                            classes: 'ui-tooltip-red' // Make it red... the classic error colour!
                        }
                    });

                    // If we have a tooltip on this element already, just update its content
                    /*.qtip('option', 'content.text', error);*/
                }

                // If the error is empty, remove the qTip
                else {
                    $(element).qtip('destroy');
                }
            },
            success: $.noop
            // Odd workaround for errorPlacement not firing!
        });
    }
});