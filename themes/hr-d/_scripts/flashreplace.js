$(document).ready(function(){
		$("a[href*='/tests/explorer/']").click( function() {
			popupheight = screen.availHeight-200;			
			fenster = window.open($(this).attr("href"), "Navigator", "width=870,height="+popupheight+",status=no,scrollbars=yes,resizable=yes");
			fenster.focus();
			return false;
		});
		
		$("a.thickbox").fancybox();
		
		if ($.fn.flash.hasFlash.playerVersion().split(',')[0] >= 6) {
			$('#mainnavi').flash(
				{ 
					src: '/themes/hr-d/_flash/<?php echo $swf ?>',
					width: 920,
					height: <?php if ($c->getCollectionHandle()=='de' or $c->getCollectionHandle()=='tunnel_test') echo "335"; else echo "255"; ?>,
					wmode: 'transparent',
					flashvars: { chapter: '<?php echo $chapter ?>', page: '<?php echo $c->getCollectionHandle() ?>' }
				},
				{ version: 6 }
			);
		} else {
			switch ("<?php echo $chapter ?>") {
				case "home":
					navigation = 			'<img src="/themes/hr-d/_images/flashersatz/home.jpg" alt="HR Diagnostics" border="0" usemap="#home" />'
					navigation = navigation+'<map name="home" id="home">';
					navigation = navigation+'	<area shape="rect" coords="90,34,231,64" href="/de/knowhow/" alt="Know How" />';
					navigation = navigation+'	<area shape="rect" coords="232,34,362,63" href="/de/tests/" alt="Tests" />';
					navigation = navigation+'	<area shape="rect" coords="364,34,498,63" href="/de/tools/" alt="Tools" />';
					navigation = navigation+'</map>';
					break;
				case "knowhow":
					navigation = 			'<img src="/themes/hr-d/_images/flashersatz/hr-know-how.jpg" alt="HR Diagnostics" border="0" usemap="#knowhow" />';
					navigation = navigation+'<map name="knowhow" id="knowhow">';
					navigation = navigation+'	<area shape="rect" coords="462,134,622,167" href="/de/knowhow/berufsprofiling/" alt="Berufsprofiling" />';
					navigation = navigation+'	<area shape="rect" coords="462,170,622,203" href="/de/knowhow/jobmatching/" alt="Jobmatching" />';
					navigation = navigation+'	<area shape="rect" coords="283,134,443,167" href="/de/knowhow/leistungsbeurteilung/" alt="Leistungsbeurteilung" />';
					navigation = navigation+'	<area shape="rect" coords="283,170,443,203" href="/de/knowhow/personalentwicklung/" alt="Personalentwicklung" />';
					navigation = navigation+'	<area shape="rect" coords="104,171,264,204" href="/de/knowhow/potenzialanalyse/" alt="Potenzialanalyse" />';
					navigation = navigation+'	<area shape="rect" coords="104,135,264,168" href="/de/knowhow/personalauswahl/" alt="Personalauswahl" />';
					navigation = navigation+'	<area shape="rect" coords="90,34,231,64" href="/de/knowhow/" alt="Know How" />';
					navigation = navigation+'	<area shape="rect" coords="232,34,362,63" href="/de/tests/" alt="Tests" />';
					navigation = navigation+'	<area shape="rect" coords="364,34,498,63" href="/de/tools/" alt="Tools" />';
					navigation = navigation+'	<area shape="rect" coords="105,98,265,131" href="/de/knowhow/erecruiting/" alt="E-Recruiting" />';
					navigation = navigation+'</map>';
					break;
				case "tests":
					navigation = 			'<img src="/themes/hr-d/_images/flashersatz/hr-tests.jpg" alt="HR Diagnostics" border="0" usemap="#tests" />';
					navigation = navigation+'<map name="tests" id="tests">';
					navigation = navigation+'	<area shape="rect" coords="328,79,542,214" href="/de/tests/testmanufaktur/" />';
					navigation = navigation+'	<area shape="rect" coords="90,34,231,64" href="/de/knowhow/" alt="Know How" />';
					navigation = navigation+'	<area shape="rect" coords="232,34,362,63" href="/de/tests/" alt="Tests" />';
					navigation = navigation+'	<area shape="rect" coords="364,34,498,63" href="/de/tools/" alt="Tools" />';
					navigation = navigation+'	<area shape="rect" coords="105,78,319,213" href="/de/tests/testnavigator/" alt="Testnavigator" />';
					navigation = navigation+'</map>';
					break;
				case "tools":
					navigation = '<img src="/themes/hr-d/_images/flashersatz/hr-tools.jpg" alt="HR Diagnostics" border="0" usemap="#home" />';
					navigation = navigation+'<map name="home" id="home">';
					navigation = navigation+'	<area shape="rect" coords="489,183,604,208" href="/de/tools/service/" alt="Service" />';
					navigation = navigation+'	<area shape="rect" coords="489,155,604,180" href="/de/tools/sicherheit/" alt="Sicherheit" />';
					navigation = navigation+'	<area shape="rect" coords="489,127,604,152" href="/de/tools/integration/" alt="Integration" />';
					navigation = navigation+'	<area shape="rect" coords="365,127,480,209" href="/de/tools/produktivitaetswerkzeuge/" alt="Produktivitaetswerkzeuge" />';
					navigation = navigation+'	<area shape="rect" coords="229,125,344,207" href="/de/tools/jobmatcher/erecruiting" alt="S-M-XL" />';
					navigation = navigation+'	<area shape="rect" coords="90,34,231,64" href="/de/knowhow/" alt="Know How" />';
					navigation = navigation+'	<area shape="rect" coords="232,34,362,63" href="/de/tests/" alt="Tests" />';
					navigation = navigation+'	<area shape="rect" coords="364,34,498,63" href="/de/tools/" target="/de/tools/" alt="Tools" />';
					navigation = navigation+'	<area shape="rect" coords="105,125,220,207" href="/de/tools/jobmatcher/ssp_testplattform" alt="SSP" />';
					navigation = navigation+'</map>';
					break;
				default:
					navigation = '<img src="/themes/hr-d/_images/flashersatz/standard.jpg" alt="HR Diagnostics" border="0" usemap="#standard" />';
					navigation = navigation+'<map name="standard" id="standard">';
					navigation = navigation+'	<area shape="rect" coords="90,34,231,64" href="/de/knowhow/" alt="Know How" />';
					navigation = navigation+'	<area shape="rect" coords="232,34,362,63" href="/de/tests/" alt="Tests" />';
					navigation = navigation+'	<area shape="rect" coords="364,34,498,63" href="/de/tools/" alt="Tools" />';
					navigation = navigation+'</map>'; 
			}
			$('#mainnavi').html(navigation);
		}
	});