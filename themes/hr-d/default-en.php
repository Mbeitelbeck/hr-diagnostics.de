﻿<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('_elements/header.php'); ?>
    <div id="content">
        <div id="section">
            <?php
            $a = new Area('Inhalt');
            $a->display($c);
            ?>
        </div>
        <div id="aside">
            <?php
            $a = new Area('RechteSpalte');
            $a->display($c);
            ?>
        </div>
        <div class="footer"></div>
    </div>
<?php $this->inc('_elements/footer.php'); ?>