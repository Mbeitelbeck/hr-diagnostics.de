﻿<?php  defined('C5_EXECUTE') or die(_("Access Denied."));

$array = explode ( '/', $c->getCollectionPath() );
$chapter = $array[2]==""?"home":$array[2];
$lang = $array[1];
$_SESSION["lang"] = $lang;

if(!isset($_GET["popup"])) {
    $this->inc('_elements/header.php');
} else { ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="de" />
    <?php Loader::element('header_required'); ?>
    <meta http-equiv="imagetoolbar" content="no" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="copyright" content="team:orange GmbH - http://www.teamorange.de" />
    <meta name="description" content="HR-Diagnostics global" />
    <meta name="keywords" content="HR-Diagnostics global" />
    <meta name="publisher" content="HR-Diagnostics global" />
    <meta name="robots" content="index,follow" />

    <link rel="shortcut icon" href="/themes/hr-d/favicon.ico" />

    <link rel="stylesheet" href="/themes/hr-d/_styles/_reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/themes/hr-d/_styles/screen.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/themes/hr-d/_scripts/jquery/jquery.fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/themes/hr-d/_styles/flora/flora.all.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="/themes/hr-d/_styles/explorer.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/themes/hr-d/_styles/explorer-print.css" type="text/css" media="print"/>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans' type='text/css'/>
    <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body id="<?php echo $c->getCollectionHandle() ?>" class="<?php echo $chapter; if(isset($_GET["popup"])) { echo " popup"; }?>">

<?php } ?>

	<div id="content" class="htmlReplace">
		<div class="teaser">
			<div class="teaser">
			<h1><em>Testnavigator</em></h1><br />
			<p>	Web based testing methods allow the efficient and reliable diagnosis of occupational potentials.<br />
				Our portfolio of proven methods and potential applications are numerous.</p>
		</div>
		</div>
		<div id="filter">
			<h1 style="margin-bottom:15px;">Test-independent <br />search criteria</h1>
			<label style="font-size:10px;">Full text search <input name="search" type="text" id="search" style="margin-bottom:10px;" /></label><br />
			<label><input name="persAuswahlOnly" type="checkbox" id="persAuswahlOnly" value="persAuswahlOnly" />Suitable for personnel selection</label><br />
			<label><input name="persEntwOnly" type="checkbox" id="persEntwOnly" value="persEntwOnly" />Suitable for personnel development</label><br />
			<label><input name="ungBearbOnly" type="checkbox" id="ungBearbOnly" value="ungBearbOnly" />Unproctored processing is available</label>
			<h1 style="margin-top:20px;">Target group oriented <br />search criteria</h1>
            <div id="catfilter">
                <?php
                $db = Loader::db();
                $ciatresult = $db->query("SELECT category FROM targetgroupsEN GROUP BY category ORDER BY category");
                while ($ciatrow = $ciatresult->fetchRow()) {
                    echo "<h3>".str_replace("&", "&amp;", $ciatrow['category'])."</h3>";
                    $result = $db->query("SELECT * FROM targetgroupsEN WHERE category='".$ciatrow['category']."' ORDER BY orderID, category, title");
                    while ($row = $result->fetchRow()) {
                        echo "<label>";
                        echo "<input id=\"c".$row['id']."\" name=\"c".$row['id']."\" type=\"checkbox\" value=\"".$row['id']."\" class=\"check\" />";
                        echo "<span>";
                        echo str_replace("&", "&amp;", $row['title']);
                        echo "</span>";
                        echo "</label>";
                        echo "<br/>";
                    }
                }
                ?>
            </div>
		</div>
		<h5 id="filterstatus">Filtered display: <span></span> <a href="javascript:void(0);" onclick="resetFilter();">reset filter</a></h5>
		<ul id="testliste">
                <?php
                $a = new Area('Tests');
                $a->display($c);
                ?>
                </ul>
            </li>
		</ul>			
        <div class="footer"></div>
	</div>

    <script type="text/javascript" src="/themes/hr-d/_scripts/useCases.min.js"></script>
<?php
if (!$c->isEditMode()) { ?>
    <script type="text/javascript" src="/themes/hr-d/_scripts/jquery/jquery.flash.js"></script>
    <script type="text/javascript" src="/themes/hr-d/_scripts/jquery/ui/ui.core.js"></script>
    <script type="text/javascript" src="/themes/hr-d/_scripts/jquery/ui/ui.tabs.js"></script>
    <script type="text/javascript" src="/themes/hr-d/_scripts/tests.js"></script>
    <script type="text/javascript" src="/themes/hr-d/_scripts/jquery/jquery.fancybox/jquery.fancybox-1.2.1.pack.js"></script>
    <link rel="stylesheet" href="/themes/hr-d/_scripts/jquery/jquery.fancybox/jquery.fancybox.css" type="text/css" media="screen"/>
    <script type="text/javascript">
        $(document).ready(function(){
            $("a.fancy").fancybox();
            $(".details .flora > ul").tabs();
        });
        $(window).load(function() {
            $("img",$(".fancy")).unveil(10,function(){
                $(this).load(function() {
                    this.style.opacity = 1;
                });
            });
        });
    </script>
<?php } ?>

<?php
if(isset($_GET["popup"])) {
    echo "</body></html>";
} else {
    $this->inc('_elements/footer.php');
}
?>