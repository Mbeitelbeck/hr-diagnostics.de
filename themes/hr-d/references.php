<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * User: l.pommerenke
 * Date: 06.06.13
 * Time: 10:15
 */

$u = new User();

if ($u->isLoggedIn()) {
    $this->inc('_elements/header.php');
}
$references = new Area('Inhalt');
$references->display($c);

if ($u->isLoggedIn()) {
    $this->inc('_elements/footer.php');
}

