﻿<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('_elements/header.php'); ?>
<div id="content">
    <div id="section">
        <div class="hr-top"></div>
        <div id="standardPage">
            <?php print $innerContent;    ?>
        </div>
        <div class="hr-bottom"></div>
    </div>
    <div id="aside">
    </div>
    <div class="footer"></div>
</div>
<?php $this->inc('_elements/footer.php'); ?>