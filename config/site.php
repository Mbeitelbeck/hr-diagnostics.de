<?php
//PASSWORT STIMMT NICHT MIT ONLINE ÜBEREIN
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'db475365418');
define('PASSWORD_SALT', 'tYQFEZehWg6xfWJohO7Yt03qRjTy5sFIVi9k75ZrPHZ6BcR26tJ2QTbtO7h1OFDM');
define('ENABLE_NEWSFLOW_OVERLAY', false);

$locales = array("en" => "en_US", "de" => "de_DE");

$regexp = '/^' . preg_quote(DIR_REL, '/') . '\/(index.php\/)?([^\/$]+)/';
preg_match($regexp, $_SERVER['REQUEST_URI'], $match);

if (isset($locales[$match[2]])) {
    define('LOCALE', $locales[$match[2]]);
} else {
    define('LOCALE', 'de_DE');
}?>