<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

$v = View::getInstance();

$v->setThemeByPath('/login', "hr-d");
$v->setThemeByPath('/403', "hr-d");
$v->setThemeByPath('/page_not_found', "hr-d");
