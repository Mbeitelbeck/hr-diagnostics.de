<?php  
defined('C5_EXECUTE') or die("Access Denied.");

$body = "Das Formular Akkreditierung wurde ueber Ihre Webseite gesendet.\r\n\r\n".
	$questionAnswerPairs[1]['question']."\r\n".$questionAnswerPairs[1]['answer']."\r\n\r\n".
	$questionAnswerPairs[30]['question']."\r\n".$questionAnswerPairs[30]['answer']."\r\n\r\n".
	$questionAnswerPairs[3]['question']."\r\n".$questionAnswerPairs[3]['answer']."\r\n\r\n".
	$questionAnswerPairs[32]['question']."\r\n".$questionAnswerPairs[32]['answer']."\r\n\r\n".
	$questionAnswerPairs[8]['question']."\r\n".$questionAnswerPairs[8]['answer']."\r\n\r\n".
	$questionAnswerPairs[4]['question']."\r\n".$questionAnswerPairs[4]['answer']."\r\n\r\n".
	$questionAnswerPairs[31]['question']."\r\n".$questionAnswerPairs[31]['answer']."\r\n\r\n".
	$questionAnswerPairs[5]['question']."\r\n".$questionAnswerPairs[5]['answer']."\r\n\r\n".
	$questionAnswerPairs[6]['question']."\r\n".$questionAnswerPairs[6]['answer']."\r\n\r\n".
	$questionAnswerPairs[7]['question']."\r\n".$questionAnswerPairs[7]['answer']."\r\n\r\n".
	$questionAnswerPairs[34]['question']."\r\n".$questionAnswerPairs[34]['answer']."\r\n\r\n".
	$questionAnswerPairs[35]['question']."\r\n".$questionAnswerPairs[35]['answer']."\r\n\r\n".
    "Um alle Antworten von diesem Formular zu sehen, besuchen Sie ".BASE_URL.DIR_REL.'/' . DISPATCHER_FILENAME . '/dashboard/reports/forms/?qsid='.$questionSetId;